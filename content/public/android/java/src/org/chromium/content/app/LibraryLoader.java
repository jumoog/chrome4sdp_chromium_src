// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.chromium.content.app;

import android.os.AsyncTask;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import org.chromium.base.JNINamespace;
import org.chromium.content.common.CommandLine;
import org.chromium.content.common.TraceEvent;

/**
 * This class provides functionality to load and register the native library.
 * In most cases, users will call ensureInitialized() from their main thread
 * (only) which ensures a post condition that the library is loaded,
 * initialized, and ready to use.
 * Optionally, an application may optimize startup be calling loadNow early on,
 * from a background thread, and then on completion of that method it must call
 * ensureInitialized() on the main thread before it tries to access any native
 * code.
 */
@JNINamespace("content")
public class LibraryLoader {
    private static final String TAG = "LibraryLoader";

    private static String[] sLibraries = null;

    // This object's lock guards sLoaded assignment and also the library load.
    private static Object sLoadedLock = new Object();
    private static Boolean sLoaded = false;

    private static boolean sInitialized = false;


    /**
     * Sets the name of the libraries that are to be loaded.  This must be called prior
     * to them being loaded the first time.
     *
     * @param libraries The name of the libraries to be loaded (without the lib prefix).
     */
    public static void setLibrariesToLoad(String[] libraries) {
        if (equals(libraries, sLibraries))
            return;

        assert !sLoaded : "Setting the libraries must happen before load is called.";
        sLibraries = libraries;
    }

    /**
     * @return The name of the native library set to be loaded.
     */
    public static String[] getLibrariesToLoad() {
        return sLibraries;
    }

    @Deprecated
    public static void loadAndInitSync() {
        // TODO(joth): remove in next patch.
        ensureInitialized();
    }

    /**
     *  This method blocks until the library is fully loaded and initialized;
     *  must be called on the thread that the native will call its "main" thread.
     */
    public static void ensureInitialized() {
        checkThreadUsage();
        if (sInitialized) {
            // Already initialized, nothing to do.
            return;
        }
        loadNow();
        initializeOnMainThread();
    }

    /**
     * @throws UnsatisfiedLinkError if the library is not yet initialized.
     */
    public static void checkIsReady() {
        if (!sInitialized) {
            throw new UnsatisfiedLinkError(sLibraries + " is not initialized");
        }
    }

    /**
     * Loads the library and blocks until the load completes. The caller is responsible
     * for subsequently calling ensureInitialized().
     * May be called on any thread, but should only be called once. Note the thread
     * this is called on will be the thread that runs the native code's static initializers.
     * See the comment in doInBackground() for more considerations on this.
     *
     * @return Whether the native library was successfully loaded.
     */
    public static void loadNow() {
        if (sLibraries == null) {
            assert false : "No library specified to load.  Call setLibrariesToLoad before first.";
        }
        synchronized (sLoadedLock) {
            if (!sLoaded) {
                assert !sInitialized;
                for (int i = 0; i < sLibraries.length; ++i) {
                    final String library = sLibraries[i];
                    Log.i(TAG, "loading: " + library);
                    try {
                        System.loadLibrary(library);
                        Log.i(TAG, "loaded: " + library);
                    } catch (Error e) {
                        Log.i(TAG, "error loading: " + library + "; " + e.getMessage());
                    } catch (Exception e) {
                        Log.i(TAG, "could not load: " + library + "; " + e.getMessage());
                    }
                }
                sLoaded = true;
            }
        }
    }

    /**
     * initializes the library here and now: must be called on the thread that the
     * native will call its "main" thread. The library must have previously been
     * loaded with loadNow.
     * @param initCommandLine The command line arguments that native command line will
     * be initialized with.
     */
    static void initializeOnMainThread(String[] initCommandLine) {
        checkThreadUsage();
        if (sInitialized) {
            return;
        }
        if (!nativeLibraryLoadedOnMainThread(initCommandLine)) {
            Log.e(TAG, "error calling nativeLibraryLoadedOnMainThread");
            throw new UnsatisfiedLinkError();
        }
        // From this point on, native code is ready to use and checkIsReady()
        // shouldn't complain from now on (and in fact, it's used by the
        // following calls).
        sInitialized = true;
        CommandLine.enableNativeProxy();
        TraceEvent.setEnabledToMatchNative();
    }

    static private void initializeOnMainThread() {
        checkThreadUsage();
        if (!sInitialized) {
            initializeOnMainThread(CommandLine.getJavaSwitchesOrNull());
        }
    }

    static private boolean equals(String[] a, String[] b) {
        if (a == null || b == null || a.length != b.length)
            return false;
        for (int i = 0; i < a.length; ++i)
            if (!TextUtils.equals(a[i], b[i]))
                return false;
        return true;
    }

    private LibraryLoader() {
    }

    // This asserts that calls to ensureInitialized() will happen from the
    // same thread.
    private static Object sCheckThreadLock = new Object();
    private static Thread sMyThread;
    private static void checkThreadUsage() {
        Thread currentThread = Thread.currentThread();
        synchronized (sCheckThreadLock) {
            if (sMyThread == null) {
                sMyThread = currentThread;
            } else {
                if (sMyThread != currentThread) {
                    Log.e(TAG, "Threading violation detected. My thread=" + sMyThread + " id=" +
                            sMyThread.getId() + " but I'm being accessed from thread=" +
                          currentThread + " id=" + currentThread.getId());
                    assert false;
                }
            }
        }
    }

    // This is the only method that is registered during System.loadLibrary, as it
    // may happen on a different thread. We then call it on the main thread to register
    // everything else.
    private static native boolean nativeLibraryLoadedOnMainThread(String[] initCommandLine);
}
