/*
Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.chromium.content.browser;

import android.content.Context;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.SurfaceHolder;
import android.view.TextureView;
import android.graphics.SurfaceTexture;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.view.TextureView.SurfaceTextureListener;
import android.widget.FrameLayout;

import org.chromium.base.JNINamespace;
import android.util.Log;

@JNINamespace("content")
public class ContentViewRenderViewFlex extends FrameLayout {

    public class RendererDetails {
        public int rendererId;        //RenderProcessHost ID
        public int renderWidgetId;    //RenderWidgetId
        public Boolean attached = false; //AttachmentStatus
    }

    //Private members
    private SurfaceView mSurfaceView;
    private TextureView mTextureView;
    private Boolean mPrimaryView = false;      //Information if this view is the primary (most optimal) view to be used
    private Boolean mReadyToRender = false;
    private RendererDetails mAttachedRenderer = new RendererDetails();      //This has all the information regarding an AttachedRenderer;
    private Surface mCurrentSurface = null;
    private int mViewType = -1;
    private int mViewId = -1;
    private int mWidth = -1;
    private int mHeight = -1;
    private int mCachedWidth = -1;
    private int mCachedHeight = -1;
    private Bitmap mCachedBitmap;
    private boolean mRecreateBitmap = true;
    private boolean mSurfaceExists = false;
    private static final int INTERNAL_SNAPSHOT_WIDTH = 120;
    private static final int INTERNAL_SNAPSHOT_HEIGHT = 120;

    public static final int SURFACE_VIEW = 0;

    //Need a way to track view ids
    private static int sViewId = 0;

    /**
     * Constructs a new ContentViewRenderViewFlex that can be added to a view hierarchy.
     * @param context The context used to create this.
     * @param activeShell The shell that will use this view for rendering
     * @param viewType default SurfaceView for now
     */
    public ContentViewRenderViewFlex(Context context, Boolean primaryView, int viewType) {
        super(context);

        nativeInit();
        mPrimaryView = primaryView;

        if (viewType == SURFACE_VIEW) {
            createSurfaceView();
        } else {
            Log.e("ContentViewRenderViewFlex", "ViewType not supported");
        }
        mViewType = viewType;

        mViewId = ++sViewId;
    }

    public static boolean checkBypassAvailability() {
        return nativeCheckBypassAvailability();
    }

    public static void conditionalBypassAvailability() {
        nativeConditionalBypassAvailability();
    }

    public void createSurfaceView() {
        Log.d("ContentViewRenderViewFlex","creating a new SurfaceView");
        mSurfaceView = new SurfaceView(getContext());
        mSurfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                nativeSurfaceSetSize(width, height);
                mWidth = width;
                mHeight = height;
            }

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                if(mCurrentSurface != null && mAttachedRenderer != null && mAttachedRenderer.attached == true) {
                    nativeSurfaceDestroyed(mCurrentSurface, mViewId);
                    mCurrentSurface = null;
                }
                mCurrentSurface = holder.getSurface();
                onReadyToRender();
                mReadyToRender = true;
                rendererSetNativeWidget();
                mSurfaceExists = true;
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                if (mCurrentSurface != null) {
                    nativeSurfaceDestroyed(mCurrentSurface, mViewId);
                    mCurrentSurface = null;
                    mSurfaceExists = false;
                }
            }
        });

        addView(mSurfaceView,
                new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT,
                        FrameLayout.LayoutParams.MATCH_PARENT));

    }

    public Boolean isPrimaryView()      {   return mPrimaryView;    }
    public Boolean isReadyToRender()    {   return mReadyToRender;  }


    public int getAttachedRendererId()  {   return mAttachedRenderer.rendererId;    }
    public int getAttachedRoutingId()   {   return mAttachedRenderer.renderWidgetId;    }
    public Boolean getAttachedStatus()  {   return mAttachedRenderer.attached;  }
    public int getViewId()              {   return mViewId; }

    private void rendererSetNativeWidget() {
        if (mAttachedRenderer != null && mAttachedRenderer.attached == true && mCurrentSurface != null && mReadyToRender) {
            nativeRendererSetNativeWidget(mAttachedRenderer.rendererId, mAttachedRenderer.renderWidgetId, mCurrentSurface, mViewType, mViewId);
            onRendererAttached();
        }
    }

    public void invalidateSnapshot() {
        mRecreateBitmap = true;
    }

    public Bitmap captureSnapshot(int width, int height) {
        if (mWidth > 0 && mHeight > 0 && mRecreateBitmap) {
            Bitmap b = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);

            nativeCaptureSnapshot(b, mViewId);
            //mirror Matrix
            float[] mirrorMatrix =
                {   1, 0, 0,
                    0, -1, 0,
                    0, 0, 1 };
            Matrix matrix = new Matrix();
            matrix.setValues(mirrorMatrix);

            Bitmap resizedBitmap = Bitmap.createScaledBitmap(b, width, height, false);

            b.recycle();
            Bitmap mirrorBitmap = Bitmap.createBitmap(resizedBitmap, 0, 0, width, height, matrix, true);
            resizedBitmap.recycle();

            mCachedBitmap = mirrorBitmap;
            mCachedWidth = width;
            mCachedHeight = height;
            mRecreateBitmap = false;
            return mirrorBitmap;
        } else if (!mRecreateBitmap) {
            if (width != mWidth || height != mHeight) {
                Bitmap resizedBitmap = Bitmap.createScaledBitmap(mCachedBitmap, width, height, false);
                return resizedBitmap;
            }

            return mCachedBitmap;
        }

        return null;
    }

    public void detachRenderer() {
        if (mSurfaceExists) {
            invalidateSnapshot();
            //Create snapshot for clients that might need it
            captureSnapshot(INTERNAL_SNAPSHOT_WIDTH, INTERNAL_SNAPSHOT_HEIGHT);
            mSurfaceExists = false;
        }
        nativeSurfaceDestroyed(mCurrentSurface, mViewId);
    }

    /**
     * This should only be called from the UI thread as it uses the current surface
     */
    public void attachRendererToView(int rendererId, int renderWidgetId) {
        mAttachedRenderer.rendererId = rendererId;
        mAttachedRenderer.renderWidgetId = renderWidgetId;
        mAttachedRenderer.attached = true;
        rendererSetNativeWidget();
    }

    /**
     * This method should be subclassed to provide actions to be performed once the view is ready to
     * render.
     */
    protected void onReadyToRender() {
    }

    /**
     * This method should be subclassed to provide actions to be performed once the view is ready to
     * be shown again.
     */
    protected void onRendererAttached() {
    }

    /**
     * @return whether the surface view is initialized and ready to render.
     */
    public boolean isInitialized() {
        return mSurfaceView.getHolder().getSurface() != null;
    }

    /**
     * Makes the passed ContentView the one displayed by this ContentViewRenderView.
     */
    public void setCurrentContentView(ContentView contentView) {
        //NOT IMPLEMENTED
    }

    private native void nativeInit();
    private static native void nativeSurfaceDestroyed(Surface surface, int viewId);
    private static native void nativeDetachRenderer(Surface surface, int viewId);
    private static native void nativeSurfaceSetSize(int width, int height);
    private static native void nativeRendererSetNativeWidget(int rendererId, int rendererWidgetId, Surface surface, int stype, int viewId);
    private static native boolean nativeCaptureSnapshot(Bitmap b, int viewId);
    private static native boolean nativeCheckBypassAvailability();
    private static native void nativeConditionalBypassAvailability();
}

