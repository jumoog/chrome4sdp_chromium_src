// Copyright (c) 2013, The Linux Foundation. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//     * Neither the name of The Linux Foundation nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
// ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
// BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef CONTENT_RENDERER_MEDIA_AUDIO_DECODER_MESSAGE_FILTER_H_
#define CONTENT_RENDERER_MEDIA_AUDIO_DECODER_MESSAGE_FILTER_H_

#include "base/gtest_prod_util.h"
#include "base/id_map.h"
#include "base/shared_memory.h"
#include "base/sync_socket.h"
#include "content/common/content_export.h"
#include "ipc/ipc_channel_proxy.h"
#include "media/audio/audio_buffers_state.h"
#include "media/audio/android/audio_decoder_ipc.h"
#include "media/audio/audio_parameters.h"



namespace content {

class CONTENT_EXPORT AudioDecoderMessageFilter
    : public IPC::ChannelProxy::MessageFilter,
      public NON_EXPORTED_BASE(media::AudioDecoderIPC) {
public:
    AudioDecoderMessageFilter();

    // Getter for the one AudioDecoderMessageFilter object.
    static AudioDecoderMessageFilter* Get();

    // media::AudioDecoderIPCDelegate implementation.
    virtual int AddDelegate(media::AudioDecoderIPCDelegate* delegate) OVERRIDE;
    virtual void RemoveDelegate(int id) OVERRIDE;
    virtual void InitializeDecoder(int id) OVERRIDE;
    virtual void DecodeBuffer(const char* data,uint32 size,base::SharedMemoryHandle data_mem) OVERRIDE;
    virtual void NotifyDecodeCompletion() OVERRIDE;

    // IPC::ChannelProxy::MessageFilter override. Called on IO thread.
    virtual bool OnMessageReceived(const IPC::Message& message) OVERRIDE;
    virtual void OnFilterAdded(IPC::Channel* channel) OVERRIDE;
    virtual void OnFilterRemoved() OVERRIDE;
    virtual void OnChannelClosing() OVERRIDE;

   void OnDecodeDone(int id,base::SharedMemoryHandle handle, uint32 size,uint16 nb_of_channel,const media::AudioParameters& decoded_params);

protected:
    virtual ~AudioDecoderMessageFilter();

private:
    // Sends an IPC message using |channel_|.
    bool Send(IPC::Message* message);

    // The singleton instance for this filter.
    static AudioDecoderMessageFilter* filter_;

    // A map of stream ids to delegates.
    IDMap<media::AudioDecoderIPCDelegate> delegates_;

    IPC::Channel* channel_;

    DISALLOW_COPY_AND_ASSIGN(AudioDecoderMessageFilter);
};

}  // namespace content

#endif  // CONTENT_RENDERER_MEDIA_AUDIO_MESSAGE_FILTER_H_
