// Copyright (c) 2013, The Linux Foundation. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//     * Neither the name of The Linux Foundation nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
// ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
// BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "content/renderer/android/audio_decoder_message_filter.h"

#include "base/bind.h"
#include "base/message_loop.h"
#include "base/time.h"
#include "content/common/child_process.h"
#include "content/common/media/audio_messages.h"
#include "content/renderer/render_thread_impl.h"
#include "ipc/ipc_logging.h"

namespace content {

AudioDecoderMessageFilter* AudioDecoderMessageFilter::filter_ = NULL;

// static
AudioDecoderMessageFilter* AudioDecoderMessageFilter::Get() {
    return filter_;
}

AudioDecoderMessageFilter::AudioDecoderMessageFilter()
    : channel_(NULL) {
    DVLOG(1) << "AudioDecoderMessageFilter::AudioDecoderMessageFilter()";
    DCHECK(!filter_);
    filter_ = this;
}

int AudioDecoderMessageFilter::AddDelegate(media::AudioDecoderIPCDelegate* delegate) {
    return delegates_.Add(delegate);
}

void AudioDecoderMessageFilter::RemoveDelegate(int id) {
    delegates_.Remove(id);
}

void AudioDecoderMessageFilter::InitializeDecoder(int id) {
    DVLOG(1) << "AudioDecoderMessageFilter::InitializeDecoder() ";
    Send(new AudioDecoderHostMsg_InitializeDecoder(id));
}

void AudioDecoderMessageFilter::DecodeBuffer(const char* data,uint32 size,base::SharedMemoryHandle data_mem){
    DVLOG(1) << "AudioDecoderMessageFilter::DecodeBuffer() ";
    Send(new AudioDecoderHostMsg_DecodeBuffer(std::string(data),size,data_mem));
}

void AudioDecoderMessageFilter::NotifyDecodeCompletion() {
    DVLOG(1) << "AudioDecoderMessageFilter::DecodeBuffer() ";
    Send(new AudioDecoderHostMsg_NotifyDecodeCompletion());
}

bool AudioDecoderMessageFilter::Send(IPC::Message* message) {
    if (!channel_) {
        delete message;
        return false;
    }

    if (MessageLoop::current() != ChildProcess::current()->io_message_loop()) {
    // Can only access the IPC::Channel on the IPC thread since it's not thread
    // safe.
        ChildProcess::current()->io_message_loop()->PostTask(
                                                             FROM_HERE,
                                                             base::Bind(base::IgnoreResult(&AudioDecoderMessageFilter::Send), this, message));
        return true;
     }

  return channel_->Send(message);
}

bool AudioDecoderMessageFilter::OnMessageReceived(const IPC::Message& message) {
    bool handled = true;
    IPC_BEGIN_MESSAGE_MAP(AudioDecoderMessageFilter, message)
        IPC_MESSAGE_HANDLER(AudioDecoderMsg_DecodeDone, OnDecodeDone)
        IPC_MESSAGE_UNHANDLED(handled = false)
    IPC_END_MESSAGE_MAP()
    return handled;
}

void AudioDecoderMessageFilter::OnFilterAdded(IPC::Channel* channel) {
    DVLOG(1) << "AudioMessageFilter::OnFilterAdded()";
    channel_ = channel;
}

void AudioDecoderMessageFilter::OnFilterRemoved() {
    channel_ = NULL;
}

void AudioDecoderMessageFilter::OnChannelClosing() {
    channel_ = NULL;
    LOG_IF(WARNING, !delegates_.IsEmpty())
        << "Not all audio devices have been closed.";

    IDMap<media::AudioDecoderIPCDelegate>::iterator it(&delegates_);
    while (!it.IsAtEnd()) {
        it.GetCurrentValue()->OnIPCClosed();
        delegates_.Remove(it.GetCurrentKey());
        it.Advance();
    }
}

void AudioDecoderMessageFilter::OnDecodeDone(int id,base::SharedMemoryHandle handle, uint32 size, uint16 nb_of_channel,const media::AudioParameters& decoded_params) {
    if (size) {
        media::AudioDecoderIPCDelegate* delegate = delegates_.Lookup(id);
        if (!delegate) {
            DLOG(WARNING) << "No delegate found for the decode completion";
            base::SharedMemory::CloseHandle(handle);
            return;
        }
    media::AudioParameters params(decoded_params);
    delegate->OnDataDecoded(handle,(unsigned long)size,nb_of_channel,params.sample_rate(),params.bits_per_sample()/8);
    }
}

AudioDecoderMessageFilter::~AudioDecoderMessageFilter() {
    DVLOG(1) << "AudioDecoderMessageFilter::~AudioDecoderMessageFilter()";

    OnChannelClosing();

    DCHECK(filter_);
    filter_ = NULL;
}


}  // namespace content
