// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Copyright (c) 2013, Linux Foundation. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.chromium.content_shell;

import android.app.Application;
import android.util.Log;
import java.io.File;
import android.content.Context;

import org.chromium.base.PathUtils;
import org.chromium.content.app.LibraryLoader;
import org.chromium.content.browser.ResourceExtractor;

/**
 * Entry point for the content shell application.  Handles initialization of information that needs
 * to be shared across the main activity and the sandbox services created.
 */
public class ContentShellApplication extends Application {

    private static final String[] NATIVE_LIBRARIES = new String[] {
        /* Custom V8 library. This will be loaded first, to make it possible to
         * resolve the main library. the name should match one in swe/swe.gypi
         */
        "swev8",
        /* Main library */
        "content_shell_content_view",
        /* Optional Network library */
        "netxt_plugin_proxy",
    };
    private static final String[] MANDATORY_PAK_FILES = new String[] {"content_shell.pak"};
    private static final String PRIVATE_DATA_DIRECTORY_SUFFIX = "content_shell";
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        ContentShellApplication.context = getApplicationContext();
        initializeApplicationParameters();
    }

    public static void initializeApplicationParameters() {
        ResourceExtractor.setMandatoryPaksToExtract(MANDATORY_PAK_FILES);
        LibraryLoader.setLibrariesToLoad(NATIVE_LIBRARIES);
        LibraryLoader.loadNow();
        PathUtils.setPrivateDataDirectorySuffix(PRIVATE_DATA_DIRECTORY_SUFFIX);
    }

}
