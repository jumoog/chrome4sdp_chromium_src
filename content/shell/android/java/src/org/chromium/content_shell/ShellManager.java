/*
Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.chromium.content_shell;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import org.chromium.base.CalledByNative;
import org.chromium.base.JNINamespace;
import org.chromium.content.browser.ContentView;
import org.chromium.content.browser.ContentViewRenderView;
import org.chromium.content.browser.ContentViewRenderViewFlex;
import org.chromium.content.common.CommandLine;
import org.chromium.ui.gfx.NativeWindow;

/**
 * Container and generator of ShellViews.
 */
@JNINamespace("content")
public class ShellManager extends FrameLayout {

    private static boolean sStartup = true;
    private NativeWindow mWindow;
    private Shell mActiveShell;

    private String mStartupUrl = ContentShellActivity.DEFAULT_SHELL_URL;

    // The target for all content rendering.
    private ContentViewRenderView mContentViewRenderView;
    private ContentViewRenderViewFlex mRenderTarget;

    private int mCurrentViewType = ContentViewRenderViewFlex.SURFACE_VIEW;

    /**
     * Constructor for inflating via XML.
     */
    public ShellManager(Context context, AttributeSet attrs) {
        super(context, attrs);
        nativeInit(this);
        if (CommandLine.getInstance().hasSwitch(CommandLine.DISABLE_COMPOSITOR_BYPASS)) {
            Log.d("ShellManager","Creating default view");
            mContentViewRenderView = new ContentViewRenderView(context) {
                @Override
                protected void onReadyToRender() {
                    if (sStartup) {
                        mActiveShell.loadUrl(mStartupUrl);
                        sStartup = false;
                    }
                }
            };
        } else {
                Log.d("ShellManager","Creating Flex Render view");
                mRenderTarget = new ContentViewRenderViewFlex(context, true, mCurrentViewType) {
                @Override
                protected void onReadyToRender() {
                    if (sStartup && mActiveShell != null) {
                        mActiveShell.loadUrl(mStartupUrl);
                        sStartup = false;
                    }
                }
            };
        }
    }

    public void toggleViews() {
        //NOTIMPLEMENTED
        Log.e("ShellManager","Toggle Views failed: NOT IMPLEMENTED");
    }

    /**
     * @param window The window used to generate all shells.
     */
    public void setWindow(NativeWindow window) {
        mWindow = window;
    }

    /**
     * @return The window used to generate all shells.
     */
    public NativeWindow getWindow() {
        return mWindow;
    }

    /**
     * Sets the startup URL for new shell windows.
     */
    public void setStartupUrl(String url) {
        mStartupUrl = url;
    }

    /**
     * @return The currently visible shell view or null if one is not showing.
     */
    protected Shell getActiveShell() {
        return mActiveShell;
    }

    /**
     * Creates a new shell pointing to the specified URL.
     * @param url The URL the shell should load upon creation.
     */
    public void launchShell(String url) {
        nativeLaunchShell(url);
    }

    @SuppressWarnings("unused")
    @CalledByNative
    private Object createShell() {
        LayoutInflater inflater =
                (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Shell shellView = (Shell) inflater.inflate(R.layout.shell_view, null);
        shellView.setWindow(mWindow);

        removeAllViews();
        if (mActiveShell != null) {
            ContentView contentView = mActiveShell.getContentView();
            if (contentView != null) contentView.onHide();
            mActiveShell.setContentViewRenderView(null);
        }

        if (CommandLine.getInstance().hasSwitch(CommandLine.DISABLE_COMPOSITOR_BYPASS)) {
            shellView.setContentViewRenderView(mContentViewRenderView);
        } else {
            shellView.setContentViewRenderViewFlex(mRenderTarget);
        }
        addView(shellView, new FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        mActiveShell = shellView;
        ContentView contentView = mActiveShell.getContentView();
        if (contentView != null) {
            if (CommandLine.getInstance().hasSwitch(CommandLine.DISABLE_COMPOSITOR_BYPASS)) {
                mContentViewRenderView.setCurrentContentView(contentView);
            }
            contentView.onShow();
        }

        return shellView;
    }

    private static native void nativeInit(Object shellManagerInstance);
    private static native void nativeLaunchShell(String url);
    protected static native void nativeOnPresumeUserLoadEvent();
}
