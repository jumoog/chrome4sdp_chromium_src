/*
Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CONTENT_GPU_GPU_CHILD_THREAD_H_
#define CONTENT_GPU_GPU_CHILD_THREAD_H_

#include <string>

#include "base/basictypes.h"
#include "base/command_line.h"
#include "base/memory/ref_counted.h"
#include "base/memory/scoped_ptr.h"
#include "base/time.h"
#include "build/build_config.h"
#include "content/common/child_thread.h"
#include "content/common/gpu/gpu_channel.h"
#include "content/common/gpu/gpu_channel_manager.h"
#include "content/common/gpu/gpu_config.h"
#include "content/common/gpu/x_util.h"
#include "content/public/common/gpu_info.h"
#include "ui/gfx/native_widget_types.h"

namespace sandbox {
class TargetServices;
}

#if defined(OS_ANDROID)
namespace base {
class WaitableEvent;
}
struct ANativeWindow;
#endif

namespace content {
class GpuWatchdogThread;

// The main thread of the GPU child process. There will only ever be one of
// these per process. It does process initialization and shutdown. It forwards
// IPC messages to GpuChannelManager, which is responsible for issuing rendering
// commands to the GPU.
class GpuChildThread : public ChildThread {
 public:
  explicit GpuChildThread(GpuWatchdogThread* gpu_watchdog_thread,
                          bool dead_on_arrival,
                          const GPUInfo& gpu_info);

  // For single-process mode.
  explicit GpuChildThread(const std::string& channel_id);

  virtual ~GpuChildThread();

  void Init(const base::Time& process_start_time);
  void StopWatchdog();

  // ChildThread overrides.
  virtual bool Send(IPC::Message* msg) OVERRIDE;
  virtual bool OnControlMessageReceived(const IPC::Message& msg) OVERRIDE;

#if defined(OS_ANDROID)
   // this can be called from any thread
   void SetNativeWindow(int32 view_or_surface_id,
                        int32 renderer_id,
                        ANativeWindow* native_window,
                        base::WaitableEvent* completion);

   void SwapNativeWindow(int32 old_surface_id,
                        int32 old_renderer_id,
                        int32 new_surface_id,
                        int32 new_renderer_id,
                        base::WaitableEvent* completion);

   void CaptureSnapshot(int32 surface_id,
                        int32 renderer_id,
                        const gfx::Size& size,
                        unsigned char* buffer,
                        base::WaitableEvent* completion);
#endif

 private:
  // Message handlers.
  void OnInitialize();
  void OnCollectGraphicsInfo();
  void OnGetVideoMemoryUsageStats();
  void OnSetVideoMemoryWindowCount(uint32 window_count);

  void OnClean();
  void OnCrash();
  void OnHang();
  void OnDisableWatchdog();

#if defined(USE_TCMALLOC)
  void OnGetGpuTcmalloc();
#endif

  // Set this flag to true if a fatal error occurred before we receive the
  // OnInitialize message, in which case we just declare ourselves DOA.
  bool dead_on_arrival_;
  base::Time process_start_time_;
  scoped_refptr<GpuWatchdogThread> watchdog_thread_;

#if defined(OS_WIN)
  // Windows specific client sandbox interface.
  sandbox::TargetServices* target_services_;
#endif

  scoped_ptr<GpuChannelManager> gpu_channel_manager_;

  // Information about the GPU, such as device and vendor ID.
  GPUInfo gpu_info_;

#if defined(OS_ANDROID)
  base::WeakPtrFactory<GpuChildThread> weak_ptr_factory_;
#endif

  DISALLOW_COPY_AND_ASSIGN(GpuChildThread);
};

}  // namespace content

#endif  // CONTENT_GPU_GPU_CHILD_THREAD_H_
