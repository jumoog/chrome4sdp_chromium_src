/*
Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/gpu/gpu_child_thread.h"

#include <string>
#include <vector>

#include "base/bind.h"
#include "base/command_line.h"
#include "base/threading/worker_pool.h"
#include "build/build_config.h"
#include "content/common/child_process.h"
#include "content/common/gpu/gpu_messages.h"
#include "content/gpu/gpu_info_collector.h"
#include "content/gpu/gpu_watchdog_thread.h"
#include "content/public/common/content_client.h"
#include "content/public/common/content_switches.h"
#include "ipc/ipc_channel_handle.h"
#include "ipc/ipc_sync_message_filter.h"
#include "ui/gl/gl_implementation.h"

#if defined(OS_ANDROID)
#include <jni.h>
#include "android/native_window.h"
#include "base/android/jni_android.h"
#include "base/bind.h"
#include "base/message_loop_proxy.h"
#include "base/synchronization/waitable_event.h"
#include "content/app/android/sandboxed_process_service.h"
#include "content/common/android/surface_callback.h"
#include "content/browser/gpu/gpu_surface_tracker.h"
#include "base/threading/platform_thread.h"
#include <sys/resource.h>
#include <android/native_window_jni.h>
#endif

namespace content {
namespace {

bool GpuProcessLogMessageHandler(int severity,
                                 const char* file, int line,
                                 size_t message_start,
                                 const std::string& str) {
  std::string header = str.substr(0, message_start);
  std::string message = str.substr(message_start);

  // If we are not on main thread in child process, send through
  // the sync_message_filter; otherwise send directly.
  if (MessageLoop::current() !=
      ChildProcess::current()->main_thread()->message_loop()) {
    ChildProcess::current()->main_thread()->sync_message_filter()->Send(
        new GpuHostMsg_OnLogMessage(severity, header, message));
  } else {
    ChildThread::current()->Send(new GpuHostMsg_OnLogMessage(severity, header,
        message));
  }

  return false;
}

}  // namespace

GpuChildThread::GpuChildThread(GpuWatchdogThread* watchdog_thread,
                               bool dead_on_arrival,
                               const GPUInfo& gpu_info)
    : dead_on_arrival_(dead_on_arrival),
      gpu_info_(gpu_info)
#if defined(OS_ANDROID)
    , ALLOW_THIS_IN_INITIALIZER_LIST(weak_ptr_factory_(this))
#endif
{
  watchdog_thread_ = watchdog_thread;
#if defined(OS_WIN)
  target_services_ = NULL;
#endif
}

GpuChildThread::GpuChildThread(const std::string& channel_id)
    : ChildThread(channel_id),
      dead_on_arrival_(false)
#if defined(OS_ANDROID)
    , ALLOW_THIS_IN_INITIALIZER_LIST(weak_ptr_factory_(this))
#endif
{
#if defined(OS_WIN)
  target_services_ = NULL;
#endif
  if (CommandLine::ForCurrentProcess()->HasSwitch(switches::kSingleProcess) ||
      CommandLine::ForCurrentProcess()->HasSwitch(switches::kInProcessGPU)) {
    // For single process and in-process GPU mode, we need to load and
    // initialize the GL implementation and locate the GL entry points here.
    if (!gfx::GLSurface::InitializeOneOff()) {
      VLOG(1) << "gfx::GLSurface::InitializeOneOff()";
    }
  }
}

GpuChildThread::~GpuChildThread() {
  logging::SetLogMessageHandler(NULL);
}

void GpuChildThread::Init(const base::Time& process_start_time) {
  process_start_time_ = process_start_time;
}

bool GpuChildThread::Send(IPC::Message* msg) {
  // The GPU process must never send a synchronous IPC message to the browser
  // process. This could result in deadlock.
  DCHECK(!msg->is_sync());

  return ChildThread::Send(msg);
}

bool GpuChildThread::OnControlMessageReceived(const IPC::Message& msg) {
  bool msg_is_ok = true;
  bool handled = true;
  IPC_BEGIN_MESSAGE_MAP_EX(GpuChildThread, msg, msg_is_ok)
    IPC_MESSAGE_HANDLER(GpuMsg_Initialize, OnInitialize)
    IPC_MESSAGE_HANDLER(GpuMsg_CollectGraphicsInfo, OnCollectGraphicsInfo)
    IPC_MESSAGE_HANDLER(GpuMsg_GetVideoMemoryUsageStats,
                        OnGetVideoMemoryUsageStats)
    IPC_MESSAGE_HANDLER(GpuMsg_SetVideoMemoryWindowCount,
                        OnSetVideoMemoryWindowCount)
    IPC_MESSAGE_HANDLER(GpuMsg_Clean, OnClean)
    IPC_MESSAGE_HANDLER(GpuMsg_Crash, OnCrash)
    IPC_MESSAGE_HANDLER(GpuMsg_Hang, OnHang)
    IPC_MESSAGE_HANDLER(GpuMsg_DisableWatchdog, OnDisableWatchdog)
    IPC_MESSAGE_UNHANDLED(handled = false)
  IPC_END_MESSAGE_MAP_EX()

  if (handled)
    return true;

  return gpu_channel_manager_.get() &&
      gpu_channel_manager_->OnMessageReceived(msg);
}

void GpuChildThread::OnInitialize() {
  Send(new GpuHostMsg_Initialized(!dead_on_arrival_));

  if (dead_on_arrival_) {
    VLOG(1) << "Exiting GPU process due to errors during initialization";
    MessageLoop::current()->Quit();
    return;
  }

  // We don't need to pipe log messages if we are running the GPU thread in
  // the browser process.
  if (!CommandLine::ForCurrentProcess()->HasSwitch(switches::kSingleProcess) &&
      !CommandLine::ForCurrentProcess()->HasSwitch(switches::kInProcessGPU))
    logging::SetLogMessageHandler(GpuProcessLogMessageHandler);

  // Record initialization only after collecting the GPU info because that can
  // take a significant amount of time.
  gpu_info_.initialization_time = base::Time::Now() - process_start_time_;

  // Defer creation of the render thread. This is to prevent it from handling
  // IPC messages before the sandbox has been enabled and all other necessary
  // initialization has succeeded.
  gpu_channel_manager_.reset(new GpuChannelManager(
      this,
      watchdog_thread_,
      ChildProcess::current()->io_message_loop_proxy(),
      ChildProcess::current()->GetShutDownEvent()));

#if defined(OS_ANDROID)
  content::NativeWindowCallback cb = base::Bind(&GpuChildThread::SetNativeWindow,
                                       weak_ptr_factory_.GetWeakPtr());
  content::SwapWindowCallback sw_cb = base::Bind(&GpuChildThread::SwapNativeWindow,
                                        weak_ptr_factory_.GetWeakPtr());
  content::CaptureSnapshotCallback sn_cb = base::Bind(&GpuChildThread::CaptureSnapshot,
                                        weak_ptr_factory_.GetWeakPtr());
  content::RegisterNativeWindowCallback(base::MessageLoopProxy::current(), cb, sw_cb, sn_cb);
#endif

  // Ensure the browser process receives the GPU info before a reply to any
  // subsequent IPC it might send.
  Send(new GpuHostMsg_GraphicsInfoCollected(gpu_info_));
}

void GpuChildThread::StopWatchdog() {
  if (watchdog_thread_.get()) {
    watchdog_thread_->Stop();
  }
}

void GpuChildThread::OnCollectGraphicsInfo() {
#if defined(OS_WIN)
  // GPU full info collection should only happen on un-sandboxed GPU process
  // or single process/in-process gpu mode on Windows.
  CommandLine* command_line = CommandLine::ForCurrentProcess();
  DCHECK(command_line->HasSwitch(switches::kDisableGpuSandbox) ||
         command_line->HasSwitch(switches::kSingleProcess) ||
         command_line->HasSwitch(switches::kInProcessGPU));
#endif  // OS_WIN

  if (!gpu_info_collector::CollectContextGraphicsInfo(&gpu_info_))
    VLOG(1) << "gpu_info_collector::CollectGraphicsInfo failed";
  GetContentClient()->SetGpuInfo(gpu_info_);

#if defined(OS_WIN)
  // This is slow, but it's the only thing the unsandboxed GPU process does,
  // and GpuDataManager prevents us from sending multiple collecting requests,
  // so it's OK to be blocking.
  gpu_info_collector::GetDxDiagnostics(&gpu_info_.dx_diagnostics);
  gpu_info_.finalized = true;
#endif  // OS_WIN

  Send(new GpuHostMsg_GraphicsInfoCollected(gpu_info_));

#if defined(OS_WIN)
  if (!command_line->HasSwitch(switches::kSingleProcess) &&
      !command_line->HasSwitch(switches::kInProcessGPU)) {
    // The unsandboxed GPU process fulfilled its duty.  Rest in peace.
    MessageLoop::current()->Quit();
  }
#endif  // OS_WIN
}

void GpuChildThread::OnGetVideoMemoryUsageStats() {
  GPUVideoMemoryUsageStats video_memory_usage_stats;
  if (gpu_channel_manager_.get())
    gpu_channel_manager_->gpu_memory_manager()->GetVideoMemoryUsageStats(
        video_memory_usage_stats);
  Send(new GpuHostMsg_VideoMemoryUsageStats(video_memory_usage_stats));
}

void GpuChildThread::OnSetVideoMemoryWindowCount(uint32 window_count) {
  if (gpu_channel_manager_.get())
    gpu_channel_manager_->gpu_memory_manager()->SetWindowCount(window_count);
}

void GpuChildThread::OnClean() {
  VLOG(1) << "GPU: Removing all contexts";
  if (gpu_channel_manager_.get())
    gpu_channel_manager_->LoseAllContexts();
}

void GpuChildThread::OnCrash() {
  VLOG(1) << "GPU: Simulating GPU crash";
  // Good bye, cruel world.
  volatile int* it_s_the_end_of_the_world_as_we_know_it = NULL;
  *it_s_the_end_of_the_world_as_we_know_it = 0xdead;
}

void GpuChildThread::OnHang() {
  VLOG(1) << "GPU: Simulating GPU hang";
  for (;;) {
    // Do not sleep here. The GPU watchdog timer tracks the amount of user
    // time this thread is using and it doesn't use much while calling Sleep.
  }
}

void GpuChildThread::OnDisableWatchdog() {
  VLOG(1) << "GPU: Disabling watchdog thread";
  if (watchdog_thread_.get()) {
    // Disarm the watchdog before shutting down the message loop. This prevents
    // the future posting of tasks to the message loop.
    if (watchdog_thread_->message_loop())
      watchdog_thread_->PostAcknowledge();
    // Prevent rearming.
    watchdog_thread_->Stop();
  }
}

#if defined(OS_ANDROID)
void GpuChildThread::SetNativeWindow(int32 view_or_surface_id,
                                     int32 renderer_id,
                                     ANativeWindow* native_window,
                                     base::WaitableEvent* completion) {
  // Must be called on the gpu thread.
  DCHECK(message_loop() == MessageLoop::current());
  DCHECK(gpu_channel_manager_.get());

  gpu_channel_manager_->SetNativeWindow(native_window, view_or_surface_id, renderer_id);

  if (native_window)
    ANativeWindow_release(native_window);

  if (completion) {
    completion->Signal();
  }
}

void GpuChildThread::SwapNativeWindow(int32 old_surface_id, int32 old_renderer_id,
                            int32 new_surface_id, int32 new_renderer_id,
                            base::WaitableEvent* completion) {
  //Must be called on the gpu thread
  DCHECK(message_loop() == MessageLoop::current());
  DCHECK(gpu_channel_manager_.get());

  gpu_channel_manager_->SwapNativeWindow(old_surface_id, old_renderer_id, new_surface_id, new_renderer_id);

  if (completion) {
    completion->Signal();
  }
}

void GpuChildThread::CaptureSnapshot(int32 surface_id, int32 renderer_id, const gfx::Size& size, unsigned char* buffer,
                                        base::WaitableEvent* completion) {
  //Must be called on the gpu thread
  DCHECK(message_loop() == MessageLoop::current());
  DCHECK(gpu_channel_manager_.get());

  gpu_channel_manager_->CaptureSnapshot(surface_id, renderer_id, size, buffer);

  if (completion) {
    completion->Signal();
  }
}
#endif

}  // namespace content

