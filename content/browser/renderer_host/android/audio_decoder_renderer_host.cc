// Copyright (c) 2013, The Linux Foundation. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above
//      copyright notice, this list of conditions and the following
//      disclaimer in the documentation and/or other materials provided
//      with the distribution.
//    * Neither the name of The Linux Foundation nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
// ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
// BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "content/browser/renderer_host/android/audio_decoder_renderer_host.h"

#include "base/bind.h"
#include "base/metrics/histogram.h"
#include "base/process.h"
#include "base/shared_memory.h"
#include "content/browser/browser_main_loop.h"
#include "content/browser/renderer_host/media/audio_sync_reader.h"
#include "content/common/media/audio_messages.h"
#include "content/public/browser/media_observer.h"
#include "media/audio/shared_memory_util.h"
#include "media/base/limits.h"

#include "media/audio/android/audio_factory.h"

#include "third_party/ashmem/ashmem.h"

#include <sys/mman.h>


using content::BrowserMessageFilter;
using content::BrowserThread;

using media::AudioDecoderBridge;
using media::AudioDecodeBuffer;
using media::AudioParameters;


#define AUDIO_BUFFER_MEM_SIZE (1024 * 2)
#define AUDIO_COMPRESSION_COEFF 11

///////////////////////////////////////////////////////////////////////////////
// AudioDecoderRendererHost implementations.
AudioDecoderRendererHost::AudioDecoderRendererHost(media::AudioFactory* audio_factory)
      : audio_factory_(audio_factory),
        message_loop_(audio_factory->GetMessageLoop()){
}

AudioDecoderRendererHost::~AudioDecoderRendererHost() {}

void AudioDecoderRendererHost::OnChannelClosing() {
    BrowserMessageFilter::OnChannelClosing();
}

void AudioDecoderRendererHost::OnDestruct() const {
    BrowserThread::DeleteOnIOThread::Destruct(this);
}


///////////////////////////////////////////////////////////////////////////////
// IPC Messages handler
bool AudioDecoderRendererHost::OnMessageReceived(const IPC::Message& message,
                                          bool* message_was_ok) {
    bool handled = true;
    IPC_BEGIN_MESSAGE_MAP_EX(AudioDecoderRendererHost, message, *message_was_ok)
        IPC_MESSAGE_HANDLER(AudioDecoderHostMsg_InitializeDecoder, OnInitializeDecoder)
        IPC_MESSAGE_HANDLER(AudioDecoderHostMsg_DecodeBuffer, OnDecodeBuffer)
        IPC_MESSAGE_HANDLER(AudioDecoderHostMsg_NotifyDecodeCompletion, OnNotifyDecodeCompletion)
        IPC_MESSAGE_UNHANDLED(handled = false)
    IPC_END_MESSAGE_MAP_EX()

    return handled;
}


void AudioDecoderRendererHost::OnInitializeDecoder(int id) {
    DCHECK(BrowserThread::CurrentlyOn(BrowserThread::IO));

    message_loop_->PostTask(FROM_HERE, base::Bind(
        &AudioDecoderRendererHost::DoInitializeDecoder, this, id));
}

void AudioDecoderRendererHost::OnDecodeBuffer(const std::string& data,uint32 size,base::SharedMemoryHandle data_mem) {
    DCHECK(BrowserThread::CurrentlyOn(BrowserThread::IO));

    message_loop_->PostTask(FROM_HERE, base::Bind(
        &AudioDecoderRendererHost::DoDecodeBuffer, this, data, size, data_mem));
}


void AudioDecoderRendererHost::OnNotifyDecodeCompletion() {
    DCHECK(BrowserThread::CurrentlyOn(BrowserThread::IO));

    message_loop_->PostTask(FROM_HERE, base::Bind(
        &AudioDecoderRendererHost::DoNotifyDecodeCompletion, this));
}

void AudioDecoderRendererHost::DoDecodeComplete(int id,unsigned long size,unsigned int channel_count, unsigned int sample_rate,unsigned int byte_per_sample) {
    DCHECK(BrowserThread::CurrentlyOn(BrowserThread::IO));
    if (peer_handle()) {
        base::SharedMemoryHandle memory_handle;
        unsigned long output_buffer_size = size;
        DCHECK(audio_decode_buffer_);
        if (size > audio_decode_buffer_->memory_size) {
            LOG(WARNING) << "AudioDecoderRendererHost::DoDecodeComplete - audio data may be truncated:" << size;
            // In some cases, the allocated size of decoder output buffer is smaller than the
            // calculated size of the decoded data. Therefore, only the data contained in
            // the buffer be processed.
            output_buffer_size = audio_decode_buffer_->memory_size;
        }

        media::AudioParameters decoded_params(AudioParameters::AUDIO_PCM_LINEAR, media::CHANNEL_LAYOUT_NONE,
            sample_rate, byte_per_sample * 8, output_buffer_size / (byte_per_sample * channel_count));
        if (audio_decode_buffer_->shared_memory.ShareToProcess(peer_handle(), &memory_handle)) {
            Send(new AudioDecoderMsg_DecodeDone(id, memory_handle, output_buffer_size, channel_count, decoded_params));
        }
    }
}

void AudioDecoderRendererHost::OnDecodeComplete(int id,unsigned long size, unsigned int channel_count, unsigned int sample_rate,unsigned int byte_per_sample) {
    BrowserThread::PostTask(
                            BrowserThread::IO,
                            FROM_HERE,
                            base::Bind(
                                       &AudioDecoderRendererHost::DoDecodeComplete,
                                       this,
                                       id,size,channel_count,sample_rate,byte_per_sample));
}


void AudioDecoderRendererHost::DoInitializeDecoder(int id) {
    DCHECK(message_loop_->BelongsToCurrentThread());
    audio_decoder_ = media::AudioDecoderBridge::Create(id,
                                                      base::Bind(&AudioDecoderRendererHost::OnDecodeComplete,
                                                                 base::Unretained(this)));

    audio_decoder_->Initialize();
}


void AudioDecoderRendererHost::DoDecodeBuffer(const std::string& data,uint32 size,base::SharedMemoryHandle data_mem) {
    DCHECK(message_loop_->BelongsToCurrentThread());

    uint32 mem_size = (0 == size) ? AUDIO_BUFFER_MEM_SIZE:size*AUDIO_COMPRESSION_COEFF;

    scoped_ptr<AudioDecodeBuffer> buffer(new AudioDecodeBuffer());

    if (buffer->shared_memory.CreateAndMapAnonymous(mem_size)) {
        buffer->memory_size = mem_size;
        audio_decode_buffer_ = buffer.release();
        audio_decoder_->DecodeAudioFileData(audio_decode_buffer_,data.c_str(),size,data_mem);
    }
}

void AudioDecoderRendererHost::DoNotifyDecodeCompletion() {
    DCHECK(message_loop_->BelongsToCurrentThread());

    audio_decoder_->NotifyDecodeCompletion();
    audio_decode_buffer_->shared_memory.Close();
}

