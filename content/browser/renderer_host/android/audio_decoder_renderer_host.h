// Copyright (c) 2013, The Linux Foundation. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above
//      copyright notice, this list of conditions and the following
//      disclaimer in the documentation and/or other materials provided
//      with the distribution.
//    * Neither the name of The Linux Foundation nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
// ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
// BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef CONTENT_BROWSER_RENDERER_HOST_MEDIA_AUDIO_DECODER_RENDERER_HOST_H_
#define CONTENT_BROWSER_RENDERER_HOST_MEDIA_AUDIO_DECODER_RENDERER_HOST_H_

#include <map>

#include "base/gtest_prod_util.h"
#include "base/memory/ref_counted.h"
#include "base/memory/scoped_ptr.h"
#include "base/process.h"
#include "base/sequenced_task_runner_helpers.h"
#include "base/shared_memory.h"
#include "content/common/content_export.h"
#include "content/public/browser/browser_message_filter.h"
#include "content/public/browser/browser_thread.h"

#include "media/base/android/audio_decoder_bridge.h"

namespace media {
class AudioDecodeBuffer;
class AudioFactory;
}


class CONTENT_EXPORT AudioDecoderRendererHost
    : public content::BrowserMessageFilter {
public:

    // Called from UI thread from the owner of this object.
    AudioDecoderRendererHost(media::AudioFactory* audio_factory);

    // content::BrowserMessageFilter implementation.
    virtual void OnChannelClosing() OVERRIDE;
    virtual void OnDestruct() const OVERRIDE;
    virtual bool OnMessageReceived(const IPC::Message& message,
                                 bool* message_was_ok) OVERRIDE;

private:
    friend class content::BrowserThread;
    friend class base::DeleteHelper<AudioDecoderRendererHost>;

    virtual ~AudioDecoderRendererHost();

    // Methods called on IO thread ----------------------------------------------

    void OnInitializeDecoder(int id);

    void OnDecodeBuffer(const std::string& data, uint32 size, base::SharedMemoryHandle data_mem);

    void OnNotifyDecodeCompletion();

    void DoNotifyDecodeCompletion();

    void DoInitializeDecoder(int id);

    void DoDecodeBuffer(const std::string& data,uint32 size, base::SharedMemoryHandle data_mem);

    void DoDecodeComplete(int id,unsigned long size,unsigned int channel_count, unsigned int sample_rate,unsigned int byte_per_sample);

    void OnDecodeComplete(int id,unsigned long size,unsigned int channel_count, unsigned int sample_rate,unsigned int byte_per_sample);

    media::AudioFactory* audio_factory_;

    scoped_refptr<media::AudioDecoderBridge> audio_decoder_;

    media::AudioDecodeBuffer *audio_decode_buffer_;

    scoped_refptr<base::MessageLoopProxy> message_loop_;

    DISALLOW_COPY_AND_ASSIGN(AudioDecoderRendererHost);
};

#endif  // CONTENT_BROWSER_RENDERER_HOST_MEDIA_AUDIO_RENDERER_HOST_H_
