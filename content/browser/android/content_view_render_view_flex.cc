/*
Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include "content/browser/android/content_view_render_view_flex.h"

#include "base/command_line.h"
#include "base/android/jni_android.h"
#include "base/android/jni_string.h"
#include "base/android/scoped_java_ref.h"
#include "base/bind.h"
#include "base/lazy_instance.h"
#include "base/logging.h"
#include "base/memory/scoped_ptr.h"
#include "base/message_loop.h"
#include "base/synchronization/waitable_event.h"
#include "base/synchronization/lock.h"
#include "content/browser/gpu/gpu_surface_tracker.h"
#include "content/common/android/surface_callback.h"
#include "content/common/android/surface_texture_peer.h"
#include "content/browser/gpu/gpu_data_manager_impl.h"
#include "content/public/browser/android/compositor.h"
#include "content/public/browser/android/content_view_layer_renderer.h"
#include "content/public/browser/render_process_host.h"
#include "content/public/browser/render_view_host.h"
#include "content/public/browser/render_widget_host_view.h"
#include "content/public/browser/web_contents.h"
#include "content/public/common/content_switches.h"
#include "content/shell/shell.h"
#include "content/shell/shell_browser_context.h"
#include "content/shell/shell_content_browser_client.h"
#include "cutils/properties.h"
#include "jni/ContentViewRenderViewFlex_jni.h"
#include "third_party/WebKit/Source/Platform/chromium/public/WebLayer.h"
#include "ui/gfx/size.h"
#include "ui/gfx/android/java_bitmap.h"
#include <android/bitmap.h>
#include <android/native_window_jni.h>

using base::android::ScopedJavaLocalRef;
using base::android::ScopedJavaGlobalRef;
using content::Compositor;
using content::ShellBrowserContext;
using content::ShellContentBrowserClient;
using content::Shell;

namespace {

struct GlobalState {
  GlobalState() {}
  ScopedJavaGlobalRef<jobject> j_obj;

  std::list<int> g_views;
  std::map<int, ANativeWindow*> g_native_window_map;
  std::map<int, int> g_rendererID_map;
  std::map<int, int> g_routingID_map;
  std::map<int, int> g_surfaceID_map;
  base::Lock g_lock;
};

base::LazyInstance<GlobalState> g_global_state = LAZY_INSTANCE_INITIALIZER;
static bool s_bypassavailability = true;
static bool s_availabilitycheck = false;

} // anonymous namespace

namespace content {

// Register native methods
bool RegisterContentViewRenderViewFlex(JNIEnv* env) {
  return RegisterNativesImpl(env);
}

static void Init(JNIEnv* env, jobject obj) {
  g_global_state.Get().j_obj.Reset(ScopedJavaLocalRef<jobject>(env, obj));
}

static void RendererSetNativeWidget(JNIEnv* env, jclass clazz, int rendererId, int rendererWidgetId, jobject jsurface, int sType, int viewId) {
    base::AutoLock lock(g_global_state.Get().g_lock);
    ANativeWindow* native_window = 0;

    if (!IsNativeLoopRegistered()) {
        return;
    }

    std::list<int>::iterator views_it = std::find(g_global_state.Get().g_views.begin(), g_global_state.Get().g_views.end(), viewId);

    if (views_it != g_global_state.Get().g_views.end()
            && g_global_state.Get().g_native_window_map.find(viewId) != g_global_state.Get().g_native_window_map.end()) {
        native_window = g_global_state.Get().g_native_window_map.find(viewId)->second;
    } else {
        native_window = ANativeWindow_fromSurface(env, jsurface);
    }

    if (native_window) {
        std::list<int>::iterator views_it = std::find(g_global_state.Get().g_views.begin(), g_global_state.Get().g_views.end(), viewId);

        if (views_it != g_global_state.Get().g_views.end()
                && g_global_state.Get().g_rendererID_map.find(viewId) != g_global_state.Get().g_rendererID_map.end()
                && g_global_state.Get().g_routingID_map.find(viewId) != g_global_state.Get().g_routingID_map.end()
                && g_global_state.Get().g_surfaceID_map.find(viewId) != g_global_state.Get().g_surfaceID_map.end()) {

            int c_renderer_id = g_global_state.Get().g_rendererID_map.find(viewId)->second;
            int c_routing_id = g_global_state.Get().g_routingID_map.find(viewId)->second;
            int c_surface_id = g_global_state.Get().g_surfaceID_map.find(viewId)->second;

            GpuSurfaceTracker* tracker = GpuSurfaceTracker::Get();
            int surfaceId = tracker->LookupSurfaceForRenderer(rendererId, rendererWidgetId);

            if (c_renderer_id != rendererId || c_routing_id != rendererWidgetId || c_surface_id != surfaceId) {
                tracker->SetNativeWidget(c_surface_id, 0);
                base::WaitableEvent completion(false, false);
                SwapSurfaceAsync(c_surface_id, c_renderer_id, surfaceId, rendererId, &completion);
                completion.Wait();
                tracker->SetNativeWidget(surfaceId, native_window);

                g_global_state.Get().g_rendererID_map.erase(g_global_state.Get().g_rendererID_map.find(viewId));
                g_global_state.Get().g_routingID_map.erase(g_global_state.Get().g_routingID_map.find(viewId));
                g_global_state.Get().g_surfaceID_map.erase(g_global_state.Get().g_surfaceID_map.find(viewId));

                g_global_state.Get().g_rendererID_map.insert(std::make_pair(viewId, rendererId));
                g_global_state.Get().g_routingID_map.insert(std::make_pair(viewId, rendererWidgetId));
                g_global_state.Get().g_surfaceID_map.insert(std::make_pair(viewId, surfaceId));
            }

            return;
        }
        GpuSurfaceTracker* tracker = GpuSurfaceTracker::Get();
        int surface_id = tracker->LookupSurfaceForRenderer(rendererId, rendererWidgetId);
        tracker->SetNativeWidget(surface_id, native_window);

        base::WaitableEvent completion(false, false);
        SetSurfaceAsync(env, jsurface, SurfaceTexturePeer::SET_GPU_SURFACE_TEXTURE, surface_id, rendererId, &completion);
        completion.Wait();

        g_global_state.Get().g_views.push_back(viewId);
        g_global_state.Get().g_native_window_map.insert(std::make_pair(viewId, native_window));
        g_global_state.Get().g_rendererID_map.insert(std::make_pair(viewId, rendererId));
        g_global_state.Get().g_routingID_map.insert(std::make_pair(viewId, rendererWidgetId));
        g_global_state.Get().g_surfaceID_map.insert(std::make_pair(viewId, surface_id));
    }
}

static void DetachRenderer(JNIEnv* env, jclass clazz, jobject jsurface, int viewId) {
    //TODO::This will be needed for toggling between views feature later.. disabled for now
    base::AutoLock lock(g_global_state.Get().g_lock);
    return;

    std::list<int>::iterator views_it = std::find(g_global_state.Get().g_views.begin(), g_global_state.Get().g_views.end(), viewId);

    int c_renderer_id = g_global_state.Get().g_rendererID_map.find(viewId)->second;
    int c_surface_id = g_global_state.Get().g_surfaceID_map.find(viewId)->second;
    if (views_it != g_global_state.Get().g_views.end()
                && g_global_state.Get().g_rendererID_map.find(viewId) != g_global_state.Get().g_rendererID_map.end()
                && g_global_state.Get().g_routingID_map.find(viewId) != g_global_state.Get().g_routingID_map.end()
                && g_global_state.Get().g_surfaceID_map.find(viewId) != g_global_state.Get().g_surfaceID_map.end()) {

        base::WaitableEvent completion(false, false);
        SetSurfaceAsync(env, 0, SurfaceTexturePeer::SET_GPU_SURFACE_TEXTURE, c_surface_id, c_renderer_id, &completion);
        completion.Wait();

        //Remove previous surface
        GpuSurfaceTracker* tracker = GpuSurfaceTracker::Get();
        if (tracker) {
            tracker->SetNativeWidget(c_surface_id, 0);
        }

        //Do not erase on detaching.. only erase when the surface is actually destroyed
        g_global_state.Get().g_rendererID_map.erase(g_global_state.Get().g_rendererID_map.find(viewId));
        g_global_state.Get().g_routingID_map.erase(g_global_state.Get().g_routingID_map.find(viewId));
        g_global_state.Get().g_surfaceID_map.erase(g_global_state.Get().g_surfaceID_map.find(viewId));
    }
}

static void SurfaceDestroyed(JNIEnv* env, jclass clazz, jobject jsurface, int viewId) {
    base::AutoLock lock(g_global_state.Get().g_lock);
    ANativeWindow* native_window = 0;

    std::list<int>::iterator views_it = std::find(g_global_state.Get().g_views.begin(), g_global_state.Get().g_views.end(), viewId);

    if (views_it != g_global_state.Get().g_views.end()
            && g_global_state.Get().g_native_window_map.find(viewId) != g_global_state.Get().g_native_window_map.end()) {
        native_window = g_global_state.Get().g_native_window_map.find(viewId)->second;
    }

    if (native_window != NULL) {
        std::list<int>::iterator views_it = std::find(g_global_state.Get().g_views.begin(), g_global_state.Get().g_views.end(), viewId);

        int c_renderer_id = g_global_state.Get().g_rendererID_map.find(viewId)->second;
        int c_surface_id = g_global_state.Get().g_surfaceID_map.find(viewId)->second;
        if (views_it != g_global_state.Get().g_views.end()
                && g_global_state.Get().g_rendererID_map.find(viewId) != g_global_state.Get().g_rendererID_map.end()
                && g_global_state.Get().g_routingID_map.find(viewId) != g_global_state.Get().g_routingID_map.end()
                && g_global_state.Get().g_surfaceID_map.find(viewId) != g_global_state.Get().g_surfaceID_map.end()) {

            base::WaitableEvent completion(false, false);
            SetSurfaceAsync(env, 0, SurfaceTexturePeer::SET_GPU_SURFACE_TEXTURE, c_surface_id, c_renderer_id, &completion);
            completion.Wait();
        }

        ANativeWindow_release(native_window);

        g_global_state.Get().g_native_window_map.erase(g_global_state.Get().g_native_window_map.find(viewId));
        g_global_state.Get().g_rendererID_map.erase(g_global_state.Get().g_rendererID_map.find(viewId));
        g_global_state.Get().g_routingID_map.erase(g_global_state.Get().g_routingID_map.find(viewId));
        g_global_state.Get().g_surfaceID_map.erase(g_global_state.Get().g_surfaceID_map.find(viewId));
    }
}

static jboolean CaptureSnapshot(JNIEnv* env, jclass clazz, jobject jbitmap, int viewId) {
    base::AutoLock lock(g_global_state.Get().g_lock);

    bool snapshotting = false;
    std::list<int>::iterator views_it = std::find(g_global_state.Get().g_views.begin(), g_global_state.Get().g_views.end(), viewId);

    int c_renderer_id = g_global_state.Get().g_rendererID_map.find(viewId)->second;
    int c_surface_id = g_global_state.Get().g_surfaceID_map.find(viewId)->second;
    if (views_it != g_global_state.Get().g_views.end()
            && g_global_state.Get().g_rendererID_map.find(viewId) != g_global_state.Get().g_rendererID_map.end()
            && g_global_state.Get().g_routingID_map.find(viewId) != g_global_state.Get().g_routingID_map.end()
            && g_global_state.Get().g_surfaceID_map.find(viewId) != g_global_state.Get().g_surfaceID_map.end()) {

        gfx::JavaBitmap bitmap(jbitmap);

        DCHECK(bitmap.format() == ANDROID_BITMAP_FORMAT_RGBA_8888);

        base::WaitableEvent completion(false, false);
        CaptureSnapshotAsync(c_surface_id, c_renderer_id, bitmap.size(), static_cast<unsigned char*> (bitmap.pixels()), &completion);
        completion.Wait();

        snapshotting = true;

    }

    return snapshotting;
}

static void SurfaceSetSize(
    JNIEnv* env, jclass clazz, jint width, jint height) {
    return;
}

static jboolean CheckBypassAvailability(JNIEnv* env, jclass clazz) {
    if (!s_availabilitycheck) {
        GPUInfo gpuInfo = GpuDataManagerImpl::GetInstance()->GetGPUInfo();
        if (gpuInfo.gl_renderer.find("Adreno") != std::string::npos) {
            float driver_version = atof(gpuInfo.driver_version.c_str());

            if (driver_version < 6.0)
                s_bypassavailability = false;
        } else {
            s_bypassavailability = false;
        }
        s_availabilitycheck = true;
    }

    return s_bypassavailability;
}

//Needs to be called by ui's that don't initialize the gpu information
//about the vendor/driver early enough (ex: contentshell). This disables
//the compositor bypass pass unless overridden by an android system prop
static void ConditionalBypassAvailability(JNIEnv* env, jclass clazz) {
    char pval[PROPERTY_VALUE_MAX];
    property_get("debug.enablebypasscompositor", pval, "0");

    bool bypasscompositor = atoi(pval) ? true : false;

    if(!bypasscompositor) {
        CommandLine& command_line = *CommandLine::ForCurrentProcess();
        if(!command_line.HasSwitch(switches::kDisableCompositorBypass)) {
            command_line.AppendSwitch(switches::kDisableCompositorBypass);
            LOG(WARNING) << "Compositor Bypass disabled because of unknown GPU";
        }
    }
}

}  // namespace content
