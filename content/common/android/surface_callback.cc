/*
Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/common/android/surface_callback.h"

#include <android/native_window_jni.h>

#include "base/android/jni_android.h"
#include "base/bind.h"
#include "base/lazy_instance.h"
#include "base/logging.h"
#include "base/message_loop.h"
#include "base/message_loop_proxy.h"
#include "base/synchronization/lock.h"
#include "base/synchronization/waitable_event.h"
#include "ui/gl/android_native_window.h"
#include "jni/SurfaceCallback_jni.h"

using base::android::AttachCurrentThread;
using base::android::CheckException;
using base::android::GetClass;
using base::android::MethodID;
using base::WaitableEvent;
using content::SurfaceTexturePeer;

namespace content {

namespace {

struct GlobalState {
  base::Lock registration_lock;
  // We hold a reference to a message loop proxy which handles message loop
  // destruction gracefully, which is important since we post tasks from an
  // arbitrary binder thread while the main thread might be shutting down.
  // Also, in single-process mode we have two ChildThread objects for render
  // and gpu thread, so we need to store the msg loops separately.
  scoped_refptr<base::MessageLoopProxy> native_window_loop;
  scoped_refptr<base::MessageLoopProxy> video_surface_loop;
  NativeWindowCallback native_window_callback;
  VideoSurfaceCallback video_surface_callback;
  SwapWindowCallback swap_window_callback;
  CaptureSnapshotCallback capture_snapshot_callback;
};

base::LazyInstance<GlobalState>::Leaky g_state = LAZY_INSTANCE_INITIALIZER;

void RunNativeWindowCallback(int32 routing_id,
                             int32 renderer_id,
                             ANativeWindow* native_window,
                             WaitableEvent* completion) {
  g_state.Pointer()->native_window_callback.Run(
      routing_id, renderer_id, native_window, completion);
}

void RunSwapWindowCallback(int32 old_routing_id,
                             int32 old_renderer_id,
                             int32 new_routing_id,
                             int32 new_renderer_id,
                             WaitableEvent* completion) {
  g_state.Pointer()->swap_window_callback.Run(
      old_routing_id, old_renderer_id, new_routing_id, new_renderer_id, completion);
}

void RunCaptureSnapshotCallback(int32 surface_id,
                                int32 renderer_id,
                                const gfx::Size& size,
                                unsigned char* buffer,
                                WaitableEvent* completion) {
  g_state.Pointer()->capture_snapshot_callback.Run(
          surface_id, renderer_id, size, buffer, completion);
}

void RunVideoSurfaceCallback(int32 routing_id,
                             int32 renderer_id,
                             jobject surface) {
  g_state.Pointer()->video_surface_callback.Run(
      routing_id, renderer_id, surface);
}

}  // namespace <anonymous>

static void SetSurface(JNIEnv* env, jclass clazz,
                       jint type,
                       jobject surface,
                       jint primaryID,
                       jint secondaryID) {
  SetSurfaceAsync(env, surface,
                  static_cast<SurfaceTexturePeer::SurfaceTextureTarget>(type),
                  primaryID, secondaryID, NULL);
}

void ReleaseSurface(jobject surface) {
  if (surface == NULL)
    return;

  JNIEnv* env = AttachCurrentThread();
  CHECK(env);

  ScopedJavaLocalRef<jclass> cls(GetClass(env, "android/view/Surface"));

  jmethodID method = MethodID::Get<MethodID::TYPE_INSTANCE>(
      env, cls.obj(), "release", "()V");

  env->CallVoidMethod(surface, method);
}

void SetSurfaceAsync(JNIEnv* env,
                     jobject jsurface,
                     SurfaceTexturePeer::SurfaceTextureTarget type,
                     int primary_id,
                     int secondary_id,
                     WaitableEvent* completion) {
  base::AutoLock lock(g_state.Pointer()->registration_lock);

  ANativeWindow* native_window = NULL;

  if (jsurface &&
      type != SurfaceTexturePeer::SET_VIDEO_SURFACE_TEXTURE) {
    native_window = ANativeWindow_fromSurface(env, jsurface);
    ReleaseSurface(jsurface);
  }
  GlobalState* const global_state = g_state.Pointer();

  switch (type) {
    case SurfaceTexturePeer::SET_GPU_SURFACE_TEXTURE: {
      // This should only be sent as a reaction to the renderer
      // activating compositing. If the GPU process crashes, we expect this
      // to be resent after the new thread is initialized.
      if (global_state->native_window_loop) {
        global_state->native_window_loop->PostTask(
            FROM_HERE,
            base::Bind(&RunNativeWindowCallback,
                primary_id,
                static_cast<uint32_t>(secondary_id),
                native_window,
                completion));
        // ANativeWindow_release will be called in SetNativeWindow()
      }
      break;
    }
    case SurfaceTexturePeer::SET_VIDEO_SURFACE_TEXTURE: {
      jobject surface = env->NewGlobalRef(jsurface);
      DCHECK(global_state->video_surface_loop);
      global_state->video_surface_loop->PostTask(
          FROM_HERE,
          base::Bind(&RunVideoSurfaceCallback,
                     primary_id,
                     secondary_id,
                     surface));
      break;
    }
  }
}

void SwapSurfaceAsync(int old_primary_id, int old_secondary_id,
                        int new_primary_id, int new_secondary_id,
                        base::WaitableEvent* completion){
    base::AutoLock lock(g_state.Pointer()->registration_lock);
    GlobalState* const global_state = g_state.Pointer();

    if (global_state->native_window_loop) {
        global_state->native_window_loop->PostTask(
            FROM_HERE,
            base::Bind(&RunSwapWindowCallback,
                old_primary_id,
                old_secondary_id,
                new_primary_id,
                new_secondary_id,
                completion));
    } else if (!completion) {
        completion->Signal();
    }
}

void CaptureSnapshotAsync(int surface_id, int renderer_id, const gfx::Size& size, unsigned char* buffer,
                            base::WaitableEvent* completion){
    base::AutoLock lock(g_state.Pointer()->registration_lock);
    GlobalState* const global_state = g_state.Pointer();

    if (global_state->native_window_loop) {
        global_state->native_window_loop->PostTask(
            FROM_HERE,
            base::Bind(&RunCaptureSnapshotCallback,
                surface_id,
                renderer_id,
                size,
                buffer,
                completion));
    } else if (!completion) {
        completion->Signal();
    }
}

void RegisterVideoSurfaceCallback(base::MessageLoopProxy* loop,
                                  VideoSurfaceCallback& callback) {
  GlobalState* const global_state = g_state.Pointer();
  base::AutoLock lock(global_state->registration_lock);
  global_state->video_surface_loop = loop;
  global_state->video_surface_callback = callback;
}

void RegisterNativeWindowCallback(base::MessageLoopProxy* loop,
                                  NativeWindowCallback& callback,
                                  SwapWindowCallback& swap_callback,
                                  CaptureSnapshotCallback& capture_callback) {
  GlobalState* const global_state = g_state.Pointer();
  base::AutoLock lock(global_state->registration_lock);
  global_state->native_window_loop = loop;
  global_state->native_window_callback = callback;
  global_state->swap_window_callback = swap_callback;
  global_state->capture_snapshot_callback = capture_callback;
}

bool IsNativeLoopRegistered(){
  GlobalState* const global_state = g_state.Pointer();
  base::AutoLock lock(global_state->registration_lock);
  return (global_state->native_window_loop != NULL);
}

bool RegisterSurfaceCallback(JNIEnv* env) {
  return RegisterNativesImpl(env);
}

}  // namespace content
