/*
Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CONTENT_COMMON_ANDROID_SURFACE_CALLBACK_H_
#define CONTENT_COMMON_ANDROID_SURFACE_CALLBACK_H_

#include <jni.h>

#include "base/callback.h"
#include "content/common/android/surface_texture_peer.h"
#include "ui/gfx/size.h"

struct ANativeWindow;

namespace base {
class MessageLoopProxy;
class WaitableEvent;
}

namespace content {

// This file implements support for passing surface handles from Java
// to the correct thread on the native side. On Android, these surface
// handles can only be passed across processes through Java IPC (Binder),
// which means calls from Java come in on arbitrary threads. Hence the
// static nature and the need for the client to register a callback with
// the corresponding message loop.

// Asynchronously sets the Surface. This can be called from any thread.
// The Surface will be set to the proper thread based on the type. The
// nature of primary_id and secondary_id depend on the type of surface
// and are used to route the surface to the correct client.
// This method will call release() on the jsurface object to release
// all the resources. So after calling this method, the caller should
// not use the jsurface object again.
void SetSurfaceAsync(JNIEnv* env,
                     jobject jsurface,
                     SurfaceTexturePeer::SurfaceTextureTarget type,
                     int primary_id,
                     int secondary_id,
                     base::WaitableEvent* completion);

void SwapSurfaceAsync(int old_primary_id, int old_secondary_id,
                      int new_primary_id, int new_secondary_id,
                      base::WaitableEvent* completion);

void CaptureSnapshotAsync(int surface_id, int renderer_id,
                          const gfx::Size& size, unsigned char* buffer,
                          base::WaitableEvent* completion);

void ReleaseSurface(jobject surface);

typedef base::Callback<void(int, int, ANativeWindow*, base::WaitableEvent*)>
                            NativeWindowCallback;
typedef base::Callback<void(int, int, int, int, base::WaitableEvent*)>
                            SwapWindowCallback;
typedef base::Callback<void(int, int, const gfx::Size&, unsigned char*, base::WaitableEvent*)>
                            CaptureSnapshotCallback;
void RegisterNativeWindowCallback(base::MessageLoopProxy* loop,
                                  NativeWindowCallback& callback,
                                  SwapWindowCallback& swap_callback,
                                  CaptureSnapshotCallback& capture_callback);

typedef base::Callback<void(int, int, jobject surface)> VideoSurfaceCallback;
void RegisterVideoSurfaceCallback(base::MessageLoopProxy* loop,
                                  VideoSurfaceCallback& callback);
bool IsNativeLoopRegistered();

bool RegisterSurfaceCallback(JNIEnv* env);

}  // namespace content

#endif  // CONTENT_COMMON_ANDROID_SURFACE_CALLBACK_H_
