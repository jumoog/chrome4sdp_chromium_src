/*
Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/common/gpu/image_transport_surface.h"

#include "base/logging.h"
#include "content/common/gpu/gpu_surface_lookup.h"
#include "content/common/gpu/texture_image_transport_surface.h"
#include "ui/gl/gl_surface_egl.h"
#include "ui/gl/gl_surface_android.h"

namespace content {

// static
scoped_refptr<gfx::GLSurface> ImageTransportSurface::CreateSurface(
    GpuChannelManager* manager,
    GpuCommandBufferStub* stub,
    const gfx::GLSurfaceHandle& handle,
    bool bypass_compositor) {
  scoped_refptr<gfx::GLSurface> surface;
  if (!handle.handle && handle.transport) {
    surface = new TextureImageTransportSurface(manager, stub, handle);
  } else if (handle.handle == gfx::kDummyPluginWindow && !handle.transport) {
    if (!bypass_compositor) {
        DCHECK(GpuSurfaceLookup::GetInstance());
        ANativeWindow* window = GpuSurfaceLookup::GetInstance()->GetNativeWidget(
            stub->surface_id());
        DCHECK(window);
        surface = new gfx::NativeViewGLSurfaceEGL(false, window);
        if (!surface.get() || !surface->Initialize())
            return NULL;

        surface = new PassThroughImageTransportSurface(
            manager, stub, surface.get(), handle.transport);
    } else {
        surface = new gfx::AndroidViewSurface();
        surface->Initialize();

        surface = new PassThroughImageTransportSurface(
            manager, stub, surface.get(), handle.transport);
        surface->Initialize();
        return surface;
    }
  } else {
    NOTIMPLEMENTED();
    return NULL;
  }

  if (surface->Initialize())
    return surface;
  else {
    LOG(ERROR) << "Failed to initialize ImageTransportSurface";
    return NULL;
  }
}

}  // namespace content
