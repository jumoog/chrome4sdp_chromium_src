// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Copyright (c) 2013, The Linux Foundation. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CONTENT_COMMON_GPU_STREAM_TEXTURE_MANAGER_ANDROID_H_
#define CONTENT_COMMON_GPU_STREAM_TEXTURE_MANAGER_ANDROID_H_

#include "base/callback.h"
#include "base/id_map.h"
#include "base/memory/scoped_ptr.h"
#include "base/memory/weak_ptr.h"
#include "content/common/android/surface_texture_peer.h"
#include "gpu/command_buffer/service/stream_texture.h"
#include "gpu/command_buffer/service/stream_texture_manager.h"
#include "ui/gfx/size.h"

struct GpuStreamTextureMsg_MatrixChanged_Params;

namespace content {
class GpuChannel;

class SurfaceTextureBridge;

// Class for managing the stream texture.
class StreamTextureManagerAndroid : public gpu::StreamTextureManager {
 public:
  StreamTextureManagerAndroid(GpuChannel* channel);
  virtual ~StreamTextureManagerAndroid();

  // implement gpu::StreamTextureManager:
  virtual GLuint CreateStreamTexture(uint32 service_id,
                                     uint32 client_id) OVERRIDE;
  virtual void DestroyStreamTexture(uint32 service_id) OVERRIDE;
  virtual gpu::StreamTexture* LookupStreamTexture(uint32 service_id) OVERRIDE;

  // Methods invoked from GpuChannel.
  void RegisterStreamTextureProxy(
      int32 stream_id, const gfx::Size& initial_size, int32 route_id);
  void EstablishStreamTexture(
      int32 stream_id, content::SurfaceTexturePeer::SurfaceTextureTarget type,
      int32 primary_id, int32 secondary_id);
  void SetStreamTextureFrameSize(
      int32 stream_id, int width, int height);

  // Send new transform matrix.
  void SendMatrixChanged(int route_id,
      const GpuStreamTextureMsg_MatrixChanged_Params& params);

 private:
  // The stream texture class for android.
  class StreamTextureAndroid
      : public gpu::StreamTexture,
        public base::SupportsWeakPtr<StreamTextureAndroid> {
   public:
    StreamTextureAndroid(GpuChannel* channel, int service_id);
    virtual ~StreamTextureAndroid();

    virtual void Update() OVERRIDE;

    // Validates the update context for StreamTextureAndroid
    // Should be called before calling Update() as it sets the cached
    // update context to the first unique context that calls it.
    // Validation is done because android SurfaceTexture only allows a
    // single GLContext to update a stream texture. If another
    // GLContext tries to update the texture, an assertion is thrown.
    virtual bool ValidateUpdateContext(gfx::GLContext* context) OVERRIDE;

    virtual int FrameWidth() const OVERRIDE {
        return frame_size_.width();
    }

    virtual int FrameHeight() const OVERRIDE {
        return frame_size_.height();
    }

    scoped_refptr<SurfaceTextureBridge> surface_texture_bridge() {
        return surface_texture_bridge_;
    }

    // Called when a new frame is available.
    void OnFrameAvailable(int route_id);

    // Callback function when transform matrix of the surface texture
    // has changed.
    typedef base::Callback<
        void(const GpuStreamTextureMsg_MatrixChanged_Params&)>
            MatrixChangedCB;

    void set_matrix_changed_callback(const MatrixChangedCB& callback) {
        matrix_callback_ = callback;
    }

    void set_frame_size(const gfx::Size& size) {
        frame_size_ = size;
    }

   private:
    scoped_refptr<SurfaceTextureBridge> surface_texture_bridge_;

    // Current transform matrix of the surface texture.
    float current_matrix_[16];

    // Whether the surface texture has been updated.
    bool has_updated_;

    MatrixChangedCB matrix_callback_;

    GpuChannel* channel_;
    gfx::Size frame_size_;
    // For stream textures, there can only be one update context
    // Keep track of the first context it is updated on. No
    // other contexts will be allowed to update.
    scoped_refptr<gfx::GLContext> update_context_;
    DISALLOW_COPY_AND_ASSIGN(StreamTextureAndroid);
  };

  GpuChannel* channel_;

  typedef IDMap<StreamTextureAndroid, IDMapOwnPointer> TextureMap;
  TextureMap textures_;

  // Map for more convenient lookup.
  typedef IDMap<gpu::StreamTexture, IDMapExternalPointer> TextureServiceIdMap;
  TextureServiceIdMap textures_from_service_id_;

  DISALLOW_COPY_AND_ASSIGN(StreamTextureManagerAndroid);
};

}  // namespace content

#endif  // CONTENT_COMMON_GPU_STREAM_TEXTURE_MANAGER_ANDROID_H_
