// Copyright (c) 2013, The Linux Foundation. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//     * Neither the name of The Linux Foundation nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
// ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
// BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef MEDIA_AUDIO_ANDROID_AUDIO_DECODER_IPC_H_
#define MEDIA_AUDIO_ANDROID_AUDIO_DECODER_IPC_H_


#include "base/shared_memory.h"
#include "base/sync_socket.h"
#include "media/base/media_export.h"

namespace media {

// Contains IPC notifications for the state of the server side
// (AudioOutputController) audio state changes and when an AudioOutputController
// has been created.  Implemented by AudioOutputDevice.
class MEDIA_EXPORT AudioDecoderIPCDelegate {
public:

    // Called when an audio stream has been created.
    // The shared memory |handle| points to a memory section that's used to
    // transfer audio buffers from the AudioOutputIPCDelegate back to the
    // AudioRendererHost.  The implementation of OnStreamCreated takes ownership.
    // The |socket_handle| is used by AudioRendererHost to signal requests for
    // audio data to be written into the shared memory. The AudioOutputIPCDelegate
    // must read from this socket and provide audio whenever data (search for
    // "pending_bytes") is received.
    virtual void OnDataDecoded(base::SharedMemoryHandle handle,
                                  unsigned long length,
                                  unsigned int channel_nb,
                                  unsigned int sample_rate,
                                  unsigned int byte_per_sample) = 0;

    // Called when the AudioOutputIPC object is going away and/or when the IPC
    // channel has been closed and no more ipc requests can be made.
    // Implementations must clear any references to the AudioOutputIPC object
    // at this point.
    virtual void OnIPCClosed() = 0;

protected:
    virtual ~AudioDecoderIPCDelegate();
};

// Provides IPC functionality for an AudioOutputDevice.  The implementation
// should asynchronously deliver the messages to an AudioOutputController object
// (or create one in the case of CreateStream()), that may live in a separate
// process.
class MEDIA_EXPORT AudioDecoderIPC {
public:
    // Registers an AudioOutputIPCDelegate and returns a |stream_id| that must
    // be used with all other IPC functions in this interface.
    virtual int AddDelegate(AudioDecoderIPCDelegate* delegate) = 0;

    // Unregisters a delegate that was previously registered via a call to
    // AddDelegate().  The audio stream should be in a closed state prior to
    // calling this function.
    virtual void RemoveDelegate(int id) = 0;

    virtual void InitializeDecoder(int id) = 0;

    virtual void DecodeBuffer(const char* data,uint32 size, base::SharedMemoryHandle data_mem) = 0;

    virtual void NotifyDecodeCompletion() = 0;

protected:
    virtual ~AudioDecoderIPC();
};

}  // namespace media

#endif  // MEDIA_AUDIO_AUDIO_OUTPUT_IPC_H_
