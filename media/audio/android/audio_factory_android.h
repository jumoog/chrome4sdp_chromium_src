// Copyright (c) 2013, The Linux Foundation. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//     * Neither the name of The Linux Foundation nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
// ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
// BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef MEDIA_AUDIO_ANDROID_AUDIO_FACTORY_ANDROID_H_
#define MEDIA_AUDIO_ANDROID_AUDIO_FACTORY_ANDROID_H_


#include <map>
#include <string>

#include "base/atomic_ref_count.h"
#include "base/compiler_specific.h"
#include "base/memory/scoped_ptr.h"
#include "base/observer_list.h"
#include "base/synchronization/lock.h"
#include "media/audio/android/audio_factory.h"

namespace base {
class Thread;
}

namespace media {

// Android implemention of AudioFactory.
class AudioFactoryAndroid : public AudioFactory {
public:
    AudioFactoryAndroid();

    virtual scoped_refptr<base::MessageLoopProxy> GetMessageLoop() OVERRIDE;
    virtual void Init() OVERRIDE;

protected:
    virtual ~AudioFactoryAndroid();

    void Shutdown();

private:

    // Thread used to interact with audio streams created by this audio manager.
    scoped_ptr<base::Thread> audio_factory_thread_;
    mutable base::Lock audio_factory_thread_lock_;

    // The message loop of the audio thread this object runs on.  Set on Init().
    // Used for internal tasks which run on the audio thread even after Shutdown()
    // has been started and GetMessageLoop() starts returning NULL.
    scoped_refptr<base::MessageLoopProxy> message_loop_;

    DISALLOW_COPY_AND_ASSIGN(AudioFactoryAndroid);
};

}  // namespace media

#endif  // MEDIA_AUDIO_ANDROID_AUDIO_FACTORY_ANDROID_H_
