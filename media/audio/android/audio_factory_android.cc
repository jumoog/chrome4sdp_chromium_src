// Copyright (c) 2013, The Linux Foundation. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//     * Neither the name of The Linux Foundation nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
// ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
// BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "media/audio/android/audio_factory_android.h"

#include "base/logging.h"
#include "base/threading/thread.h"

namespace media {

AudioFactory* CreateAudioFactory() {
    return new AudioFactoryAndroid();
}

AudioFactoryAndroid::AudioFactoryAndroid() {

}

AudioFactoryAndroid::~AudioFactoryAndroid() {
    Shutdown();
}

scoped_refptr<base::MessageLoopProxy> AudioFactoryAndroid::GetMessageLoop() {
    base::AutoLock lock(audio_factory_thread_lock_);
    // Don't return |message_loop_| here because we don't want any new tasks to
    // come in once we've started tearing down the audio thread.
    return audio_factory_thread_.get() ? audio_factory_thread_->message_loop_proxy() : NULL;
}


void AudioFactoryAndroid::Init() {
    base::AutoLock lock(audio_factory_thread_lock_);
    DCHECK(!audio_factory_thread_.get());
    audio_factory_thread_.reset(new base::Thread("AudioFactoryThread"));
    CHECK(audio_factory_thread_->Start());
    message_loop_ = audio_factory_thread_->message_loop_proxy();
}

void AudioFactoryAndroid::Shutdown() {
    // To avoid running into deadlocks while we stop the thread, shut it down
    // via a local variable while not holding the audio thread lock.
    scoped_ptr<base::Thread> audio_factory_thread;
    {
        base::AutoLock lock(audio_factory_thread_lock_);
        audio_factory_thread_.swap(audio_factory_thread);
    }

    if (!audio_factory_thread.get())
        return;

    CHECK_NE(MessageLoop::current(), audio_factory_thread->message_loop());

    // Stop() will wait for any posted messages to be processed first.
    audio_factory_thread->Stop();
}

}  // namespace media
