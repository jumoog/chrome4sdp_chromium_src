// Copyright (c) 2013, The Linux Foundation. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//     * Neither the name of The Linux Foundation nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
// ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
// BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef MEDIA_BASE_ANDROID_AUDIO_DECODER_BRIDGE_H_
#define MEDIA_BASE_ANDROID_AUDIO_DECODER_BRIDGE_H_

#include <jni.h>
#include <map>
#include <string>

#include "base/android/scoped_java_ref.h"
#include "base/callback.h"
#include "base/shared_memory.h"
#include "base/memory/scoped_ptr.h"
#include "base/memory/weak_ptr.h"
#include "base/synchronization/waitable_event.h"

#include "base/time.h"
#include "base/timer.h"

#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>

namespace WebKit { class WebAudioBus; }

namespace media {

// Callback when error happens. Args: player ID, error type.
typedef base::Callback<void(int,unsigned long,unsigned int,unsigned int,unsigned int)> DecodeDoneCB;


class  AudioDecodeBuffer {
public:
    AudioDecodeBuffer();
    ~AudioDecodeBuffer();

    // Shared memory for transmission of the audio data.
    base::SharedMemory shared_memory;

    uint32 memory_size;
};


class AudioDecoderBridge :public base::RefCountedThreadSafe<AudioDecoderBridge>{
public:

    ~AudioDecoderBridge();

    static scoped_refptr<AudioDecoderBridge> Create(int id,
                                                    const DecodeDoneCB& decode_done_cb);

    static AudioDecoderBridge* Get();

    bool Initialize();

    int GetId(){ return id_; };

    bool DecodeAudioFileData(AudioDecodeBuffer* decoded_buffer, const char* data,
                         uint32 data_size/*, double sample_rate*/,base::SharedMemoryHandle data_mem);

    void NotifyDecodeCompletion();

    void OnDecodeDone(int id, unsigned long size, unsigned int nb_of_channels, unsigned int sample_rate, unsigned int byte_per_sample);

    void DecodeAudioFileDataComplete();

private:
    friend class base::RefCountedThreadSafe<AudioDecoderBridge>;

    AudioDecoderBridge(int id,
                       const DecodeDoneCB& decode_done_cb);

    SLObjectItf engine_object_;
    SLEngineItf engine_itf_;
    SLObjectItf player_itf_;
    SLPlayItf player_play_;
    SLMetadataExtractionItf meta_extr_itf_;
    SLAndroidSimpleBufferQueueItf audio_buffer_queue_;

    int id_;

    // The singleton instance for this filter.
    static AudioDecoderBridge* bridge_;

    DecodeDoneCB decode_done_cb_;

    void PrepareData(uint32 data_size, base::SharedMemoryHandle data_mem, base::WaitableEvent *event);

    void DoPrepareData(uint32 data_size, base::SharedMemoryHandle data_mem, base::WaitableEvent *event);

    DISALLOW_COPY_AND_ASSIGN(AudioDecoderBridge);
};

}  // namespace media

#endif  // MEDIA_BASE_ANDROID_MEDIA_PLAYER_BRIDGE_H_
