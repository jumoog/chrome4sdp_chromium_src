// Copyright (c) 2013, The Linux Foundation. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//     * Neither the name of The Linux Foundation nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
// ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
// BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "media/base/android/audio_decoder_bridge.h"

#include "base/android/jni_android.h"
#include "base/android/jni_string.h"
#include "base/basictypes.h"
#include "base/logging.h"
#include "base/message_loop_proxy.h"

#include "base/file_path.h"
#include "base/file_util.h"

#include "content/browser/browser_main_loop.h"

#include "webkit/media/audio_decoder.h"
#include "webkit/media/android/webaudio_assets_impl_android.h"

#include <sys/mman.h>
#include "base/shared_memory.h"

#include "media/audio/shared_memory_util.h"

#include <fcntl.h>

using base::android::AttachCurrentThread;
using base::android::GetApplicationContext;
using base::android::CheckException;
using base::android::ClearException;
using base::android::GetClass;
using base::android::JavaRef;
using base::android::MethodID;
using base::android::ScopedJavaLocalRef;
using content::BrowserThread;

static int channel_count_key_index = -1;
static int sample_rate_key_index = -1;
static int bits_per_sample_key_index = -1;
static int container_size_key_index = -1;


/* Structure for passing information to callback function */
typedef struct CallbackCntxt_ {
    SLPlayItf play_itf;
    SLMetadataExtractionItf meta_itf;
    SLuint32  size;
    SLint8*   data_base;    // Base address of local audio data storage
    SLint8*   data;        // Current address of local audio data storage
} CallbackCntxt;


FilePath tmp_path;
int fd = 0;

namespace media {

CallbackCntxt cntxt;

void AudioBufferCallback(SLAndroidSimpleBufferQueueItf bq, void *context){
}

void AudioDecoderCallback(SLPlayItf decoder, void *context, SLuint32 event) {
    if (SL_PLAYEVENT_HEADATEND == event) {
        CallbackCntxt *ctx = (CallbackCntxt*)context;
        SLMetadataInfo *pcm_meta_data = NULL;

        SLmillisecond msec;
        uint64 sample_rate,channel_count,byte_per_sample;
        SLresult result = (*ctx->play_itf)->GetPosition(ctx->play_itf, &msec);
        DCHECK(SL_RESULT_SUCCESS == result);

        uint64 size =0;

        if (!pcm_meta_data) {
            pcm_meta_data = (SLMetadataInfo*) malloc(32);
        }

        result = (*ctx->meta_itf)->GetValue(ctx->meta_itf, sample_rate_key_index,
                                                32, pcm_meta_data);
        sample_rate = *((SLuint32*)pcm_meta_data->data);
        result = (*ctx->meta_itf)->GetValue(ctx->meta_itf, channel_count_key_index,
                                                32, pcm_meta_data);
        channel_count = *((SLuint32*)pcm_meta_data->data);
        result = (*ctx->meta_itf)->GetValue(ctx->meta_itf, bits_per_sample_key_index,
                                                32, pcm_meta_data);
        byte_per_sample = (*((SLuint32*)pcm_meta_data->data))/8;

        free(pcm_meta_data);
        pcm_meta_data = NULL;

        // the size will be casted as a unsigned long as
        // it's not possible (or very unlikely) that it
        // has a value higher than a unsigned long.
        // However during the calculation the intermediate
        // values could be of a unsigned long long range
        size = sample_rate*channel_count*byte_per_sample*msec/1000;

        // the decoder generates multiple of 1024 according to google

        size = (size%1024)?(size/1024 +1)*1024 : (size/1024)*1024;

        AudioDecoderBridge* bridge = AudioDecoderBridge::Get();
        bridge->OnDecodeDone(bridge->GetId(),(unsigned long)size,(unsigned int)channel_count,(unsigned int)sample_rate,(unsigned int)byte_per_sample);
        bridge->DecodeAudioFileDataComplete();
    }
}

AudioDecodeBuffer::AudioDecodeBuffer() {}

AudioDecodeBuffer::~AudioDecodeBuffer() {}


AudioDecoderBridge* AudioDecoderBridge::bridge_ = NULL;

// static
AudioDecoderBridge* AudioDecoderBridge::Get() {
    return bridge_;
}

AudioDecoderBridge::AudioDecoderBridge(int id,
                                       const DecodeDoneCB& decode_done_cb)
                                         :id_(id),
                                          decode_done_cb_(decode_done_cb){
    bridge_ = this;
}

AudioDecoderBridge::~AudioDecoderBridge() {}

scoped_refptr<AudioDecoderBridge> AudioDecoderBridge::Create(int id,
                                                             const DecodeDoneCB& decode_done_cb) {
    DCHECK(id);

    scoped_refptr<AudioDecoderBridge> decoderBridge(new AudioDecoderBridge(id, decode_done_cb));

    return decoderBridge;
}

void AudioDecoderBridge::OnDecodeDone(int id, unsigned long size, unsigned int nb_of_channels, unsigned int sample_rate,unsigned int byte_per_sample){
    decode_done_cb_.Run(id,size,nb_of_channels,sample_rate,byte_per_sample);
}

bool AudioDecoderBridge::Initialize(){
    SLresult result;
    SLEngineItf engine_itf = NULL;

    result = slCreateEngine(&engine_object_, 0, NULL, 0, NULL, NULL);
    if (SL_RESULT_SUCCESS == result) {
        result = (*engine_object_)->Realize(engine_object_, SL_BOOLEAN_FALSE);
        if (SL_RESULT_SUCCESS == result) {
              result = (*engine_object_)->GetInterface(engine_object_, SL_IID_ENGINE, (void*)&engine_itf);
              if (SL_RESULT_SUCCESS == result) {
                  engine_itf_ = engine_itf;
              }
        }
    }

    return (SL_RESULT_SUCCESS == result) ? true:false;
}

bool AudioDecoderBridge::DecodeAudioFileData(AudioDecodeBuffer* decoded_buffer, const char* data,
                       uint32 data_size/*, double sample_rate*/,base::SharedMemoryHandle data_mem){
    SLresult result;
    bool assetFound = true;

    off_t start,length;
    int fd;
    SLDataLocator_AndroidFD loc_fd;
    SLDataFormat_MIME format_mime = {SL_DATAFORMAT_MIME, NULL, SL_CONTAINERTYPE_UNSPECIFIED};
    SLDataSource audioSource;

    SLDataLocator_AndroidSimpleBufferQueue buffer_queue = {SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE,2};
    SLDataFormat_PCM format_pcm = {SL_DATAFORMAT_PCM, 2, SL_SAMPLINGRATE_44_1,
                                   SL_PCMSAMPLEFORMAT_FIXED_16, SL_PCMSAMPLEFORMAT_FIXED_16,
                                   SL_SPEAKER_FRONT_LEFT | SL_SPEAKER_FRONT_RIGHT,
                                   SL_BYTEORDER_LITTLEENDIAN};
    SLDataSink audioSink = {&buffer_queue, &format_pcm};

    if (data_size == 0) {
        if (webkit_media::webAudioAsset(data,&fd,&start,&length)) {
            loc_fd.locatorType = SL_DATALOCATOR_ANDROIDFD;
            loc_fd.fd = (SLint32)fd;
            loc_fd.offset = (SLAint64)start;
            loc_fd.length = (SLAint64)length;
        } else
            assetFound = false;
    } else {
        base::WaitableEvent event(false,false);
        PrepareData(data_size,data_mem,&event);
        event.Wait();

        fd = open(tmp_path.value().c_str(), O_RDONLY);
        if (fd < 0) {
            LOG(ERROR) << "AudioDecoderBridge::DecodeAudioFileData can't create temporary file";
            return false;
        }
        loc_fd.locatorType = SL_DATALOCATOR_ANDROIDFD;
        loc_fd.fd = (SLint32)fd;
        loc_fd.offset = (SLAint64)0;
        loc_fd.length = (SLAint64)SL_DATALOCATOR_ANDROIDFD_USE_FILE_SIZE;
    }

    audioSource.pLocator = &loc_fd;
    audioSource.pFormat = &format_mime;

    if (assetFound) {
        const SLInterfaceID ids[3] = {SL_IID_ANDROIDSIMPLEBUFFERQUEUE, SL_IID_PREFETCHSTATUS, SL_IID_METADATAEXTRACTION};
        const SLboolean req[3] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE};
        result = (*engine_itf_)->CreateAudioPlayer(engine_itf_, &player_itf_,&audioSource, &audioSink, 3, ids, req);
         DCHECK(SL_RESULT_SUCCESS == result);

         result = (*player_itf_)->Realize(player_itf_, SL_BOOLEAN_FALSE);
         DCHECK(SL_RESULT_SUCCESS == result);

         result = (*player_itf_)->GetInterface(player_itf_, SL_IID_METADATAEXTRACTION, (void*)&meta_extr_itf_);
         if (SL_RESULT_SUCCESS == result) {
             SLuint32 itemCount;
             result = (*meta_extr_itf_)->GetItemCount(meta_extr_itf_, &itemCount);

             SLuint32 i, keySize, valueSize;
             SLMetadataInfo *keyInfo;
             for(i=0 ; i<itemCount ; i++) {
                 keyInfo = NULL;
                 keySize = 0;
                 valueSize = 0;
                 result = (*meta_extr_itf_)->GetKeySize(meta_extr_itf_, i, &keySize);
                 DCHECK(SL_RESULT_SUCCESS == result);
                 result = (*meta_extr_itf_)->GetValueSize(meta_extr_itf_, i, &valueSize);
                 DCHECK(SL_RESULT_SUCCESS == result);
                 keyInfo = (SLMetadataInfo*) malloc(keySize);
                 if (NULL != keyInfo) {
                     result = (*meta_extr_itf_)->GetKey(meta_extr_itf_, i, keySize, keyInfo);
                     DCHECK(SL_RESULT_SUCCESS == result);

                     /* find out the key index of the metadata we're interested in */
                     if (!strcmp((char*)keyInfo->data, "AndroidPcmFormatNumChannels")) {
                         channel_count_key_index = i;
                     } else if (!strcmp((char*)keyInfo->data, "AndroidPcmFormatSampleRate")){
                         sample_rate_key_index = i;
                     } else if (!strcmp((char*)keyInfo->data, "AndroidPcmFormatBitsPerSample")) {
                         bits_per_sample_key_index = i;
                     } else if (!strcmp((char*)keyInfo->data, "AndroidPcmFormatContainerSize")) {
                         container_size_key_index = i;
                     }

                     free(keyInfo);
                 }
             }
         } else {
             LOG(ERROR) << "AudioDecoderBridge::DecodeAudioFileData error:" << result;
         }

         // get the buffer queue interface
         result = (*player_itf_)->GetInterface(player_itf_, SL_IID_ANDROIDSIMPLEBUFFERQUEUE,&audio_buffer_queue_);
         DCHECK(SL_RESULT_SUCCESS == result);

         uint8 *buffer = static_cast<uint8*>(decoded_buffer->shared_memory.memory());

         result = (*audio_buffer_queue_)->Enqueue(audio_buffer_queue_,buffer,decoded_buffer->memory_size);
         DCHECK(SL_RESULT_SUCCESS == result);

         result = (*audio_buffer_queue_)->RegisterCallback(audio_buffer_queue_, AudioBufferCallback, NULL);
         DCHECK(SL_RESULT_SUCCESS == result);

         result = (*player_itf_)->GetInterface(player_itf_, SL_IID_PLAY, (void *)&player_play_);
         DCHECK(SL_RESULT_SUCCESS == result);

         cntxt.play_itf = player_play_;
         cntxt.meta_itf = meta_extr_itf_;
         cntxt.data_base = (int8_t*)&buffer;
         cntxt.data = cntxt.data_base;
         cntxt.size = sizeof(buffer);

         result = (*player_play_)->RegisterCallback(player_play_, AudioDecoderCallback, &cntxt);
         DCHECK(SL_RESULT_SUCCESS == result);

         result = (*player_play_)->SetCallbackEventsMask( player_play_, SL_PLAYEVENT_HEADATEND);
         DCHECK(SL_RESULT_SUCCESS == result);

         result = (*player_play_)->SetPlayState( player_play_, SL_PLAYSTATE_PLAYING );
         if (SL_RESULT_SUCCESS == result)
             return true;
         else
             LOG(ERROR) << "AudioDecoderBridge::DecodeAudioFileData - Decode start fails " << result;
    }else
        LOG(ERROR) << "AudioDecoderBridge::DecodeAudioFileData - No data found";

    return false;
}

void AudioDecoderBridge::DecodeAudioFileDataComplete() {
    if (fd > 0)
        close(fd);
    file_util::Delete(tmp_path,false);
}

void AudioDecoderBridge::NotifyDecodeCompletion() {
    if (player_itf_ != NULL) {
        (*player_play_)->SetPlayState( player_play_, SL_PLAYSTATE_STOPPED );
        (*player_itf_)->Destroy(player_itf_);
        player_play_ = NULL;
        meta_extr_itf_ = NULL;
    }

    if (engine_object_ != NULL)
        (*engine_object_)->Destroy(engine_object_);
}

void AudioDecoderBridge::PrepareData(uint32 data_size, base::SharedMemoryHandle data_mem, base::WaitableEvent *event) {
    BrowserThread::PostTask(
                            BrowserThread::FILE,
                            FROM_HERE,
                            base::Bind(
                                       &AudioDecoderBridge::DoPrepareData,
                                       this,
                                       data_size,data_mem,event));
}

void AudioDecoderBridge::DoPrepareData(uint32 data_size, base::SharedMemoryHandle data_mem,base::WaitableEvent *event) {
      base::SharedMemory mem(data_mem,false);
      mem.Map(data_size);
      char *decoded_buffer = reinterpret_cast<char*>(mem.memory());

      if(file_util::CreateTemporaryFile(&tmp_path)) {
          int bytes_written = file_util::WriteFile(tmp_path,&decoded_buffer[0],data_size);
          if (bytes_written > 0) {
              if ((uint32)bytes_written == data_size)
                  event->Signal();
          }
      } else
          LOG(ERROR) << "AudioDecoderBridge::DoPrepareData - can't create temp file";
}

}  // namespace media
