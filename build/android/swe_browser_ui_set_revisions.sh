#!/bin/bash
#
# Copyright (c) 2013, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#    * Neither the name of The Linux Foundation nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Update the SWE Browser, WebKit, and V8 versions in
# content/shell/android/res/values/swe_versions.xml file based on the
# latest configuration information at build time.
#

COMPILER=$1
PROGNAME=$(basename $0)

SWE_BROWSER_VERSION_FILE="$CHROME_SRC/swe/browser/VERSION"
WEBKIT_VERSION_FILE="$CHROME_SRC/third_party/WebKit/configure.ac"
V8_VERSION_FILE="$CHROME_SRC/v8/src/version.cc"
V8_VERSION_FILE_PREBUILT="$CHROME_SRC/v8/VERSION"
VERSIONS_FILE="$CHROME_SRC/swe/browser/java/res/values/swe_versions.xml"



# FIXME: To remove stale VERSION_FILE
if [ -f $VERSIONS_FILE ];
then
  rm $VERSIONS_FILE
fi

# Create VERSIONS_FILE if it doesn't exist
if [ ! -f $VERSIONS_FILE ];
then
  cat > $VERSIONS_FILE << EOF
<?xml version="1.0" encoding="utf-8"?>

<resources>

    <string name="swe_browser_partial_version"></string>
    <string name="swe_browser_commit_id"></string>
    <string name="webkit_version"></string>
    <string name="v8_version"></string>
    <string name="compiler"></string>

</resources>
EOF
  echo "[I] ${PROGNAME}: $VERSIONS_FILE does not exist and has been created."
fi

# fetch SWE Browser version information
if [ -f $SWE_BROWSER_VERSION_FILE ];
then
  SWE_BROWSER_PARTIAL_VERSION=$(awk 'BEGIN { FS = "=" }
    /MAJOR/ { swe_browser_major = $2 }
    /MINOR/ { swe_browser_minor = $2 }
    /BRANCH/ { swe_browser_branch = $2 }
    END { print swe_browser_major"."swe_browser_minor"."swe_browser_branch }
    ' $SWE_BROWSER_VERSION_FILE)
  SWE_BROWSER_BASE_COMMIT_ID=$(awk 'BEGIN { FS = "=" }
    /BASE_COMMIT_ID/ { swe_browser_base_commit_id = $2 }
    END { print swe_browser_base_commit_id }
    ' $SWE_BROWSER_VERSION_FILE)
else
  SWE_BROWSER_PARTIAL_VERSION="x.x.x.x"
  SWE_BROWSER_BASE_COMMIT_ID="HEAD"
  echo "[W] ${PROGNAME}: $SWE_BROWSER_VERSION_FILE not found. \
MAJOR.MINOR.BRANCH.BUILD_ID is set to x.x.x.x and SWE_BROWSER_BASE_COMMIT_ID \
is set to HEAD."
fi

# run git log in src to calculate the BUILD_ID (= number of commits since
# SWE_BROWSER_BASE_COMMIT_ID) and also fetch the COMMIT_ID.

if [ -d $CHROME_SRC ]
then
  pushd $CHROME_SRC
  SWE_BROWSER_BUILD_ID=$(git log --format=oneline $SWE_BROWSER_BASE_COMMIT_ID.. | wc -l)
  SWE_BROWSER_COMMIT_ID=$(git log | grep -m 1 "commit" | awk '{print $2}')
  popd
else
  SWE_BROWSER_BUILD_ID="x"
  SWE_BROWSER_COMMIT_ID="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
  echo "[W] ${PROGNAME}: Unable to cd in src directory (to run git log). \
BUILD_ID.COMMIT_ID is set to x.xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx."
fi

# fetch WebKit version
if [ -f $WEBKIT_VERSION_FILE ];
then
  WEBKIT_VERSION=$(awk '
    /m4_define\(\[webkit_user_agent_major_version\],/ { webkit_major = gensub(/\[(.+)\]\)/, "\\1", "", $2) }
    /m4_define\(\[webkit_user_agent_minor_version\],/ { webkit_minor = gensub(/\[(.+)\]\)/, "\\1", "", $2) }
    END { print webkit_major"."webkit_minor }
  ' $WEBKIT_VERSION_FILE)
else
  WEBKIT_VERSION=""
  echo "[W] ${PROGNAME}: $WEBKIT_VERSION_FILE not found. WEBKIT_VERSION is \
set to empty string."
fi

# Set V8 version based on the V8_BUILD value.

if [ -f $V8_VERSION_FILE_PREBUILT ];
then
  V8_VERSION=$(awk ' { v8_version = $1 }
    END { print v8_version }
  ' $V8_VERSION_FILE_PREBUILT)
  echo "[I] ${PROGNAME}: V8 Version File = $V8_VERSION_FILE_PREBUILT"
elif [ -f $V8_VERSION_FILE ];
then
  V8_VERSION=$(awk '
    /\#define MAJOR_VERSION/ { v8_major = $3 }
    /\#define MINOR_VERSION/ { v8_minor = $3 }
    /\#define BUILD_NUMBER/ { v8_build = $3 }
    /\#define PATCH_LEVEL/ { v8_patch = $3 }
    END { print v8_major"."v8_minor"."v8_build"."v8_patch }
  ' $V8_VERSION_FILE)
  echo "[I] ${PROGNAME}: V8 Version File = $V8_VERSION_FILE"
else
  V8_VERSION=""
  echo "[W] ${PROGNAME}: Neither $V8_VERSION_FILE_PREBUILT nor $V8_VERSION_FILE were found.\
  V8_VERSION is set to empty string."
fi

# replace current version strings in $VERSIONS_FILE with latest values
sed -i "/swe_browser_partial_version/s/>[^<]*</>$SWE_BROWSER_PARTIAL_VERSION"."\
$SWE_BROWSER_BUILD_ID</" $VERSIONS_FILE
sed -i "/swe_browser_commit_id/s/>[^<]*</>$SWE_BROWSER_COMMIT_ID</" $VERSIONS_FILE
sed -i "/webkit_version/s/>[^<]*</>$WEBKIT_VERSION</" $VERSIONS_FILE
sed -i "/v8_version/s/>[^<]*</>$V8_VERSION</" $VERSIONS_FILE
sed -i "/compiler/s/>[^<]*</>$COMPILER</" $VERSIONS_FILE
