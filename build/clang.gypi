############################################################################
# Copyright (c) 2013, Code Aurora Forum. All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
############################################################################

{
  'conditions': [
    ['clang_use_integrated_as==1', {
      'cflags': [
         '-integrated-as',
      ],
    }],
    ['clang_use_integrated_as==0', {
      'cflags': [
        # Use GNU's 'as' to assemble
        '-no-integrated-as',
        # current GNU assembler is not able to assemble
        # integer division. This flag instructs LLVM to
        # use .word directive to assemble integer division
        '-mllvm -enable-hwdiv-encode=true',
      ],
    }, {
      'cflags!': [
        # The exception handling abi was not supported by
        # the integrated assembler until after the 3.2 release.
        '-mllvm -arm-enable-ehabi',
      ],
    }],
  ],
}

