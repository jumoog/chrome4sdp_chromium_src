// Copyright 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "cc/frame_rate_controller.h"

#include "base/debug/trace_event.h"
#include "base/logging.h"
#include "cc/delay_based_time_source.h"
#include "cc/time_source.h"
#include "cc/thread.h"

namespace {

// This will be the maximum number of pending frames unless
// FrameRateController::setMaxFramesPending is called.
const int defaultMaxFramesPending = 2;

// Before setTimebaseAndInterval() is called, we assume the max FPS is 60
const double defaultFPS = 60.0;
// When using vsync, we switch to manual tick if the FPS drops below
// 57 (60 * 0.95).
const double defaultFPSManualTickLimit = 0.95;
// When not using vsync, we switch to vsync if the FPS goes above
// 58 (60 * 0.966)
const double defaultFPSTimeSourceThrottlingLimit = 0.966;
}  // namespace

namespace cc {
class FPSAverage
{
public:
    // Use a long running average of 120 samples to ensure the FPS does not
    // vary widely across frames
    static const int kNumValues = 120;
    FPSAverage()
    : m_index(0)
    , m_numSet(0)
    , m_sum(0)
    , m_lastFrameTimeSet(false)
    {
        m_deltas = new float[kNumValues];
    }
    ~FPSAverage()
    {
        if (m_deltas)
            delete[] m_deltas;
    }

    void markFrame(base::TimeTicks time)
    {
        if (!m_lastFrameTimeSet) {
            float delta = (time - m_lastFrameTime).InSecondsF();
            add(delta);
        } else
            m_lastFrameTimeSet = true;
        m_lastFrameTime = time;
    }

    float getAverageFPS() {
        if (m_numSet < kNumValues)
            return defaultFPS;
        DCHECK(getAverage() != 0.0);
        return 1.0 / getAverage();
    }
private:
    inline void nextIndex(int& counter)
    {
        counter++;
        if (counter >= kNumValues)
            counter = 0;
    }

    void add(float delta)
    {
        m_sum += delta;

        if (m_numSet >= kNumValues)
            m_sum -= m_deltas[m_index];

        m_deltas[m_index] = delta;
        nextIndex(m_index);
        if (m_numSet < kNumValues)
            m_numSet++;
    }

    float getAverage() {
        DCHECK(m_numSet != 0);
        return m_sum / m_numSet;
    }

    float* m_deltas;
    int m_index;
    int m_numSet;
    float m_sum;
    base::TimeTicks m_lastFrameTime;
    bool m_lastFrameTimeSet;
};

class FrameRateControllerTimeSourceAdapter : public TimeSourceClient {
public:
    static scoped_ptr<FrameRateControllerTimeSourceAdapter> create(FrameRateController* frameRateController) {
        return make_scoped_ptr(new FrameRateControllerTimeSourceAdapter(frameRateController));
    }
    virtual ~FrameRateControllerTimeSourceAdapter() {}

    virtual void onTimerTick() OVERRIDE {
      m_frameRateController->onTimerTick();
    }

private:
    explicit FrameRateControllerTimeSourceAdapter(FrameRateController* frameRateController)
        : m_frameRateController(frameRateController) {}

    FrameRateController* m_frameRateController;
};

FrameRateController::FrameRateController(scoped_refptr<TimeSource> timer, Thread* thread)
    : m_client(0)
    , m_numFramesPending(0)
    , m_maxFramesPending(defaultMaxFramesPending)
    , m_timeSource(timer)
    , m_active(false)
    , m_swapBuffersCompleteSupported(true)
    , m_isTimeSourceThrottling(true)
    , m_forceManualTick(false)
    , m_maxFPS(defaultFPS)
    , m_fpsAverage(new FPSAverage())
    , m_thread(thread)
    , m_weakFactory(ALLOW_THIS_IN_INITIALIZER_LIST(this))
{
    m_timeSourceClientAdapter = FrameRateControllerTimeSourceAdapter::create(this);
    m_timeSource->setClient(m_timeSourceClientAdapter.get());
}

FrameRateController::FrameRateController(Thread* thread)
    : m_client(0)
    , m_numFramesPending(0)
    , m_maxFramesPending(defaultMaxFramesPending)
    , m_active(false)
    , m_swapBuffersCompleteSupported(true)
    , m_isTimeSourceThrottling(false)
    , m_forceManualTick(false)
    , m_maxFPS(defaultFPS)
    , m_fpsAverage(new FPSAverage())
    , m_thread(thread)
    , m_weakFactory(ALLOW_THIS_IN_INITIALIZER_LIST(this))
{
}

FrameRateController::~FrameRateController()
{
    if (m_isTimeSourceThrottling)
        m_timeSource->setActive(false);
}

void FrameRateController::setActive(bool active)
{
    if (m_active == active)
        return;
    TRACE_EVENT1("cc", "FrameRateController::setActive", "active", active);
    m_active = active;

    if (m_isTimeSourceThrottling && !m_forceManualTick)
        m_timeSource->setActive(active);
    else {
        if (active)
            postManualTick();
        else
            m_weakFactory.InvalidateWeakPtrs();
    }
}

void FrameRateController::chooseManualOrTimeSourceThrottling()
{
    if (!m_isTimeSourceThrottling)
        return;

    double fps = m_fpsAverage->getAverageFPS();
    bool set;
    if (!m_forceManualTick)
        set = fps < (m_maxFPS * defaultFPSManualTickLimit);
    else
        set = fps < (m_maxFPS * defaultFPSTimeSourceThrottlingLimit);

    if (set == m_forceManualTick)
        return;

    if (m_active) {
        if (!set) {
            //switch to timer
            m_timeSource->setActive(true);
            m_weakFactory.InvalidateWeakPtrs();
        } else  {
            //switch to manual
            m_timeSource->setActive(false);
        }
    }
    m_forceManualTick = set;
}

void FrameRateController::setMaxFramesPending(int maxFramesPending)
{
    DCHECK(maxFramesPending > 0);
    m_maxFramesPending = maxFramesPending;
}

void FrameRateController::setTimebaseAndInterval(base::TimeTicks timebase, base::TimeDelta interval)
{
    if (m_isTimeSourceThrottling)
        m_timeSource->setTimebaseAndInterval(timebase, interval);

    double delta = interval.InSecondsF();
    if (delta > 0.0)
        m_maxFPS = 1.0 / delta;
    else
        m_maxFPS = defaultFPS;
}

void FrameRateController::setSwapBuffersCompleteSupported(bool supported)
{
    m_swapBuffersCompleteSupported = supported;
}

void FrameRateController::onTimerTick()
{
    DCHECK(m_active);

    // Check if we have too many frames in flight.
    bool throttled = m_numFramesPending >= m_maxFramesPending;
    TRACE_COUNTER_ID1("cc", "ThrottledVSyncInterval", m_thread, throttled);

    if (m_client)
        m_client->vsyncTick(throttled);

    // only check for throttling if a thread is available for posting manual ticks
    if (m_thread)
        chooseManualOrTimeSourceThrottling();

    if (m_swapBuffersCompleteSupported && (!m_isTimeSourceThrottling || m_forceManualTick) && m_numFramesPending < m_maxFramesPending)
        postManualTick();
}

void FrameRateController::postManualTick()
{
    if (m_active && m_thread)
        m_thread->postTask(base::Bind(&FrameRateController::manualTick, m_weakFactory.GetWeakPtr()));
}

void FrameRateController::manualTick()
{
    onTimerTick();
}

void FrameRateController::didBeginFrame()
{
    m_fpsAverage->markFrame(base::TimeTicks::Now());
    if (m_swapBuffersCompleteSupported)
        m_numFramesPending++;
    else if (!m_isTimeSourceThrottling || m_forceManualTick)
        postManualTick();
}

void FrameRateController::didFinishFrame()
{
    DCHECK(m_swapBuffersCompleteSupported);
    m_numFramesPending--;
    if (!m_isTimeSourceThrottling || m_forceManualTick)
        postManualTick();
}

void FrameRateController::didAbortAllPendingFrames()
{
    m_numFramesPending = 0;
}

base::TimeTicks FrameRateController::nextTickTime()
{
    if (m_isTimeSourceThrottling && !m_forceManualTick)
        return m_timeSource->nextTickTime();

    return base::TimeTicks();
}

}  // namespace cc
