{
  'targets': [
  {
    'target_name': 'swe_browser_test_apk',
      'type': 'none',
      'dependencies': [
        'swe_browser_java',
      ],
      'variables': {
        'package_name': 'swe_browser_test',
        'apk_name': 'SWE_BrowserTest',
        'java_in_dir': 'browser/tests',
        'resource_dir': '../tests/res',
        'is_test_apk': 1,
      },
      'includes': [ '../build/java_apk.gypi' ],
  },
    ],
}
