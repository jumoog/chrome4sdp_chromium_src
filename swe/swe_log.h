/*
 *  Copyright (c) 2012, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *      * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


#ifndef SWE_LOG_H
#define SWE_LOG_H

#include <android/log.h>

#ifndef ANDROID_LOG_TAG
#define ANDROID_LOG_TAG "SWE"
#endif

// ANDROID_LOG_SILENT for no logging
// ANDROID_LOG_VERBOSE for all logging
// ANDROID_LOG_WARN (log errors/warnings) - default
#define LOG_LEVEL ANDROID_LOG_VERBOSE

#if LOG_LEVEL <= ANDROID_LOG_DEBUG
#define SWE_LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , ANDROID_LOG_TAG, __VA_ARGS__);
#else
#define SWE_LOGD(...)
#endif

#if LOG_LEVEL <= ANDROID_LOG_INFO
#define SWE_LOGI(...) __android_log_print(ANDROID_LOG_INFO   , ANDROID_LOG_TAG, __VA_ARGS__);
#else
#define SWE_LOGD(...)
#endif

#if LOG_LEVEL <= ANDROID_LOG_WARN
#define SWE_LOGW(...) __android_log_print(ANDROID_LOG_WARN   , ANDROID_LOG_TAG, __VA_ARGS__);
#define SWE_LOGE(...) __android_log_print(ANDROID_LOG_ERROR  , ANDROID_LOG_TAG, __VA_ARGS__);
#else
#define SWE_LOGD(...)
#endif

#if LOG_LEVEL == ANDROID_LOG_VERBOSE
#define SWE_LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, ANDROID_LOG_TAG, __VA_ARGS__);
#define SWE_LOG_FUNC() __android_log_print(ANDROID_LOG_VERBOSE, ANDROID_LOG_TAG, "%s:%d", __FUNCTION__, __LINE__);
#else
#define SWE_LOGV(...)
#define SWE_LOG_FUNC(...)
#endif

#endif
