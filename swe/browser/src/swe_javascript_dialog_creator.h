// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Copyright (c) 2013, The Linux Foundation. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef SWE_JAVASCRIPT_DIALOG_CREATOR_H_
#define SWE_JAVASCRIPT_DIALOG_CREATOR_H_

#include "base/compiler_specific.h"
#include "base/callback_forward.h"
#include "base/memory/scoped_ptr.h"
#include "base/memory/singleton.h"
#include "content/public/browser/javascript_dialogs.h"

namespace content {

class SWEJavaScriptDialog;

class SWEJavaScriptDialogCreator : public JavaScriptDialogCreator {
 public:

  // JavaScriptDialogCreator:
  virtual void RunJavaScriptDialog(
      WebContents* web_contents,
      const GURL& origin_url,
      const std::string& accept_lang,
      JavaScriptMessageType javascript_message_type,
      const string16& message_text,
      const string16& default_prompt_text,
      const DialogClosedCallback& callback,
      bool* did_suppress_message) OVERRIDE;

  virtual void RunBeforeUnloadDialog(
      WebContents* web_contents,
      const string16& message_text,
      bool is_reload,
      const DialogClosedCallback& callback) OVERRIDE;

  virtual void ResetJavaScriptState(WebContents* web_contents) OVERRIDE;

  void CancelPendingDialogs(WebContents* web_contents);

  // Used for content_browsertests.
  void set_dialog_request_callback(const base::Closure& callback) {
    dialog_request_callback_ = callback;
  }

  static SWEJavaScriptDialogCreator* GetInstance();

 private:
  explicit SWEJavaScriptDialogCreator();
  virtual ~SWEJavaScriptDialogCreator();

  friend struct DefaultSingletonTraits<SWEJavaScriptDialogCreator>;

  // For tests
  scoped_ptr<SWEJavaScriptDialog> dialog_;

  base::Closure dialog_request_callback_;

  DISALLOW_COPY_AND_ASSIGN(SWEJavaScriptDialogCreator);
};

}  // namespace content

#endif  // SWE_JAVASCRIPT_DIALOG_CREATOR_H_
