// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "swe/browser/src/shell_resource_dispatcher_host_delegate.h"

#include "base/bind.h"
#include "base/command_line.h"
#include "base/utf_string_conversions.h"
#include "content/shell/shell_switches.h"
#include "content/public/browser/render_view_host.h"
#include "content/browser/renderer_host/render_view_host_delegate.h"
#include "content/public/browser/browser_thread.h"
#include "swe/browser/src/tab.h"
#include "swe/browser/src/shell_login_dialog.h"

namespace content {

ShellResourceDispatcherHostDelegate::ShellResourceDispatcherHostDelegate() {
}

ShellResourceDispatcherHostDelegate::~ShellResourceDispatcherHostDelegate() {
}

bool ShellResourceDispatcherHostDelegate::AcceptAuthRequest(
    net::URLRequest* request,
    net::AuthChallengeInfo* auth_info) {
  bool accept_auth_request =
      !CommandLine::ForCurrentProcess()->HasSwitch(switches::kDumpRenderTree);
  return accept_auth_request;
}

ResourceDispatcherHostLoginDelegate*
ShellResourceDispatcherHostDelegate::CreateLoginDelegate(
    net::AuthChallengeInfo* auth_info, net::URLRequest* request) {
  if (!login_request_callback_.is_null()) {
    login_request_callback_.Run();
    login_request_callback_.Reset();
    return NULL;
  }

#if !defined(OS_MACOSX) && !defined(TOOLKIT_GTK) && !defined(OS_ANDROID)
// TODO: implement ShellLoginDialog for other platforms, drop this #if
  return NULL;
#else
  return new ShellLoginDialog(auth_info, request);
#endif
}

bool ShellResourceDispatcherHostDelegate::HandleExternalProtocol(const GURL& url,
                                      int child_id,
                                      int route_id) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::IO));

  static const char* const allowed_schemes[] = {
    "tel:",
    "market:",
    "rtsp:",
  };
  std::string urlStr(url.spec().c_str());
  for (size_t i = 0; i < arraysize(allowed_schemes); ++i) {
    if (0 == urlStr.find(allowed_schemes[i])) {
      // Notify BrowserThread about handling request.
      BrowserThread::PostTask(BrowserThread::UI,
                              FROM_HERE,
                              base::Bind(
                                &ShellResourceDispatcherHostDelegate::ProcessExternalProtocol,
                                base::Unretained(this),
                                ASCIIToUTF16(urlStr.c_str()),
                                child_id,
                                route_id));
      return true;
    }
  }
  return false;
}

void ShellResourceDispatcherHostDelegate::ProcessExternalProtocol(const string16& url,
                                      int child_id,
                                      int route_id) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  RenderViewHost* render_view_host =
      RenderViewHost::FromID(child_id, route_id);
  if (render_view_host) {
    content::WebContents* web_contents = render_view_host->GetDelegate()->GetAsWebContents();
    Tab* tab = Tab::GetTabFromContents(web_contents);
    tab->HandleExternalProtocol(url);
  }
}

}  // namespace content
