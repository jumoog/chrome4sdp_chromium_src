// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Copyright (c) 2013, The Linux Foundation. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "swe/browser/src/swe_javascript_dialog_creator.h"

#include "base/command_line.h"
#include "base/logging.h"
#include "base/utf_string_conversions.h"
#include "content/public/browser/web_contents.h"
#include "content/public/browser/web_contents_view.h"
#include "swe/browser/src/swe_javascript_dialog.h"
#include "content/shell/shell_switches.h"
#include "content/shell/webkit_test_controller.h"
#include "net/base/net_util.h"
#include "swe/browser/src/swe_javascript_dialog_queue.h"

namespace content {

SWEJavaScriptDialogCreator::SWEJavaScriptDialogCreator() {
}

SWEJavaScriptDialogCreator::~SWEJavaScriptDialogCreator() {
}

void SWEJavaScriptDialogCreator::RunJavaScriptDialog(
    WebContents* web_contents,
    const GURL& origin_url,
    const std::string& accept_lang,
    JavaScriptMessageType javascript_message_type,
    const string16& message_text,
    const string16& default_prompt_text,
    const DialogClosedCallback& callback,
    bool* did_suppress_message) {

  if (CommandLine::ForCurrentProcess()->HasSwitch(switches::kDumpRenderTree)) {
    WebKitTestResultPrinter* printer = WebKitTestController::Get()->printer();
    if (javascript_message_type == JAVASCRIPT_MESSAGE_TYPE_ALERT) {
      printer->AddMessage(std::string("ALERT: ") + UTF16ToUTF8(message_text));
    } else if (javascript_message_type == JAVASCRIPT_MESSAGE_TYPE_CONFIRM) {
      printer->AddMessage(std::string("CONFIRM: ") + UTF16ToUTF8(message_text));
    } else {  // JAVASCRIPT_MESSAGE_TYPE_PROMPT
      printer->AddMessage(std::string("PROMPT: ") + UTF16ToUTF8(message_text) +
                         "default text: " + UTF16ToUTF8(default_prompt_text));
    }
    callback.Run(true, string16());
    return;
  }

  if (!dialog_request_callback_.is_null()) {
    dialog_request_callback_.Run();
    callback.Run(true, string16());
    dialog_request_callback_.Reset();
    return;
  }

  *did_suppress_message = false;

  if (dialog_.get()) {
    // One dialog at a time, please.
    *did_suppress_message = true;
    return;
  }

  string16 new_message_text = net::FormatUrl(origin_url, accept_lang) +
                              ASCIIToUTF16("\n\n") +
                              message_text;

  SWEJavascriptDialogQueue::GetInstance()->AddDialog(new SWEJavaScriptDialog(web_contents,
                                          javascript_message_type,
                                          new_message_text,
                                          default_prompt_text,
                                          callback));

}

void SWEJavaScriptDialogCreator::RunBeforeUnloadDialog(
    WebContents* web_contents,
    const string16& message_text,
    bool is_reload,
    const DialogClosedCallback& callback) {
  if (CommandLine::ForCurrentProcess()->HasSwitch(switches::kDumpRenderTree)) {
    WebKitTestResultPrinter* printer = WebKitTestController::Get()->printer();
    printer->AddMessage(
        std::string("CONFIRM NAVIGATION: ") + UTF16ToUTF8(message_text));
    WebKitTestController* controller = WebKitTestController::Get();
    callback.Run(
        !controller->should_stay_on_page_after_handling_before_unload(),
        string16());
    return;
  }

  if (!dialog_request_callback_.is_null()) {
    dialog_request_callback_.Run();
    callback.Run(true, string16());
    dialog_request_callback_.Reset();
    return;
  }

  if (dialog_.get()) {
    // Seriously!?
    callback.Run(true, string16());
    return;
  }

  string16 new_message_text =
      message_text +
      ASCIIToUTF16("\n\nIs it OK to leave/reload this page?");

  SWEJavascriptDialogQueue::GetInstance()->AddDialog(new SWEJavaScriptDialog(web_contents,
                                          JAVASCRIPT_MESSAGE_TYPE_CONFIRM,
                                          new_message_text,
                                          string16(),  // default_prompt_text
                                          callback));

}

void SWEJavaScriptDialogCreator::ResetJavaScriptState(
    WebContents* web_contents) {
  CancelPendingDialogs(web_contents);
}

void SWEJavaScriptDialogCreator::CancelPendingDialogs(
    WebContents* web_contents) {
  SWEJavascriptDialogQueue* queue = SWEJavascriptDialogQueue::GetInstance();
  SWEJavaScriptDialog* active_dialog = queue->active_dialog();
  if (active_dialog && active_dialog->web_contents() == web_contents)
    active_dialog->Invalidate();
  for (SWEJavascriptDialogQueue::iterator i = queue->begin();
       i != queue->end(); ++i) {
    if ((*i)->web_contents() == web_contents)
      (*i)->Invalidate();
  }
}

/* static */
SWEJavaScriptDialogCreator* SWEJavaScriptDialogCreator::GetInstance() {
  return Singleton<SWEJavaScriptDialogCreator>::get();
}

}  // namespace content
