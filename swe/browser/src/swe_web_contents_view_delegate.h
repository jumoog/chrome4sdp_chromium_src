/*
 *  Copyright (c) 2013, The Linux Foundation. All rights reserved.
 *  Copyright (c) 2012  The Chromium Authors. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *      * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef SWE_WEB_CONTENTS_VIEW_DELEGATE_H_
#define SWE_WEB_CONTENTS_VIEW_DELEGATE_H_

#include "content/public/browser/web_contents_view_delegate.h"
#include "base/compiler_specific.h"

namespace content {
class WebContents;

class SWEWebContentsViewDelegate : public content::WebContentsViewDelegate {
 public:
  static content::WebContentsViewDelegate* CreateSWEWebContentsViewDelegate(
      content::WebContents* web_contents);

  virtual ~SWEWebContentsViewDelegate();

  // WebContentsViewDelegate implementation.
  virtual WebDragDestDelegate* GetDragDestDelegate() OVERRIDE;
  virtual void ShowContextMenu(
      const ContextMenuParams& params, ContextMenuSourceType type) OVERRIDE;

 private:
  SWEWebContentsViewDelegate(WebContents* web_contents);

  content::WebContents* web_contents_;

  DISALLOW_COPY_AND_ASSIGN(SWEWebContentsViewDelegate);
};

}  // namespace content


#endif  // SWE_WEB_CONTENTS_VIEW_DELEGATE_H_
