// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef SWE_BROWSER_TAB_H
#define SWE_BROWSER_TAB_H

#include <jni.h>

#include "base/compiler_specific.h"
#include "base/memory/scoped_ptr.h"
#include "base/android/scoped_java_ref.h"
#include "content/components/web_contents_delegate_android/web_contents_delegate_android.h"
#include "swe/browser/src/swe_javascript_dialog_creator.h"
#include "content/public/common/context_menu_params.h"

namespace ui {
class WindowAndroid;
}

namespace WebKit {
class WebLayer;
}

namespace content {

class WebContents;
class Tab;

class WebContentsDelegateImpl : public WebContentsDelegateAndroid {
 public:
  WebContentsDelegateImpl(Tab* tab_android_impl, JNIEnv* env, jobject web_contents_delegate);
  virtual ~WebContentsDelegateImpl() {}
  virtual JavaScriptDialogCreator* GetJavaScriptDialogCreator() OVERRIDE;

 private:
  Tab* tab_;
  DISALLOW_COPY_AND_ASSIGN(WebContentsDelegateImpl);
};

class Tab {
 public:
  Tab(JNIEnv* env, jobject obj, content::WebContents* web_contents, ui::WindowAndroid* window_android);
  void Destroy(JNIEnv* env, jobject obj);
  void ShowContextMenu(const content::ContextMenuParams& params);
  void HandleExternalProtocol(const string16& url);

    // Register the Tab's native methods through JNI.
  static bool Register(JNIEnv* env);

  // --------------------------------------------------------------------------
  // Methods called from Java via JNI
  // --------------------------------------------------------------------------
  void InitWebContentsDelegate(JNIEnv* env, jobject obj, jobject web_contents_delegate);
  base::android::ScopedJavaLocalRef<jbyteArray> GetOpaqueState(
      JNIEnv* env, jobject obj);
  jboolean RestoreState(JNIEnv* env, jobject obj, jbyteArray state);

  static jint CreateNativeWebContents(JNIEnv* env, jclass clazz);
  static Tab* GetTabFromContents(WebContents* web_contents);

 protected:
  virtual ~Tab();

 private:
  scoped_ptr<content::WebContents> web_contents_;
  scoped_ptr<content::WebContentsDelegateImpl>
          web_contents_delegate_;
  base::android::ScopedJavaGlobalRef<jobject> java_object_;

  DISALLOW_COPY_AND_ASSIGN(Tab);
};

}

#endif
