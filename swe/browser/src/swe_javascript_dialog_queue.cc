// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Copyright (c) 2013, The Linux Foundation. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "swe/browser/src/swe_javascript_dialog_queue.h"
#include "base/memory/singleton.h"

namespace content {

void SWEJavascriptDialogQueue::AddDialog(SWEJavaScriptDialog* dialog) {
  if (!active_dialog_) {
    ShowJSDialog(dialog);
    return;
  }
  app_js_dialog_queue_.push_back(dialog);
}

void SWEJavascriptDialogQueue::ShowNextDialog() {
  SWEJavaScriptDialog* dialog = GetNextDialog();
  if (dialog)
    ShowJSDialog(dialog);
  else
    active_dialog_ = NULL;
}

void SWEJavascriptDialogQueue::ActivateJSDialog() {
  if (showing_js_dialog_) {
    // As part of showing a js dialog we may end up back in this method
    // (showing a dialog activates the TabContents, which can trigger a call
    // to ActivateJSDialog). We ignore such a request as after the call to
    // activate the tab contents the dialog is shown.
    return;
  }
  if (active_dialog_)
    active_dialog_->ActivateJSDialog();
}

SWEJavascriptDialogQueue::SWEJavascriptDialogQueue()
    : active_dialog_(NULL), showing_js_dialog_(false) {
}

SWEJavascriptDialogQueue::~SWEJavascriptDialogQueue() {}

// static
SWEJavascriptDialogQueue* SWEJavascriptDialogQueue::GetInstance() {
  return Singleton<SWEJavascriptDialogQueue>::get();
}

void SWEJavascriptDialogQueue::ShowJSDialog(SWEJavaScriptDialog* dialog) {
  // Be sure and set the active_dialog_ field first, otherwise if
  // ShowJSDialog triggers a call back to the queue they'll get the old
  // dialog. Also, if the dialog calls |ShowNextDialog()| before returning, that
  // would write NULL into |active_dialog_| and this function would then undo
  // that.
  active_dialog_ = dialog;
  showing_js_dialog_ = true;
  dialog->ShowJSDialog();
  showing_js_dialog_ = false;
}

SWEJavaScriptDialog* SWEJavascriptDialogQueue::GetNextDialog() {
  while (!app_js_dialog_queue_.empty()) {
    SWEJavaScriptDialog* dialog = app_js_dialog_queue_.front();
    app_js_dialog_queue_.pop_front();
    if (dialog->IsValid())
      return dialog;
    delete dialog;
  }
  return NULL;
}

}
