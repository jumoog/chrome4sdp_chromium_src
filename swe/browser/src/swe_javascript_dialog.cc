// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Copyright (c) 2013, The Linux Foundation. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "swe/browser/src/swe_javascript_dialog.h"

#include "base/string_util.h"
#include "base/utf_string_conversions.h"
#include "content/shell/resource.h"
#include "content/shell/shell.h"
#include "base/android/scoped_java_ref.h"
#include "base/android/jni_android.h"
#include "base/android/jni_string.h"
#include "jni/SWEJavaScriptDialog_jni.h"
#include "swe/swe_log.h"
#include "content/public/browser/browser_thread.h"
#include "swe/browser/src/swe_javascript_dialog_creator.h"
#include "swe/browser/src/swe_javascript_dialog_queue.h"

using base::android::AttachCurrentThread;
using base::android::ConvertJavaStringToUTF16;

namespace content {
// Native implementation to route displaying Javascript Dialogs.
// Javascript Dialogs are always modal. This class notifies browser
// using JNI to display a dialog . The UI is implemented using
// Android's AlertDialog.
//


SWEJavaScriptDialog::SWEJavaScriptDialog(
    WebContents* web_contents,
    JavaScriptMessageType javascript_message_type,
    const string16& message_text,
    const string16& default_prompt_text,
    const JavaScriptDialogCreator::DialogClosedCallback& callback)
    : web_contents_(web_contents),
      message_type_(javascript_message_type),
      message_text_(message_text),
      default_prompt_text_(default_prompt_text),
      callback_(callback),
      valid_(true) {
}

void SWEJavaScriptDialog::ActivateJSDialog() {
  ShowJSDialog();
}

void SWEJavaScriptDialog::ShowJSDialog() {
      DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
      jstring title = 0;
      jstring prompt_text = 0;
      JNIEnv* env = AttachCurrentThread();
      java_object_.Reset(Java_SWEJavaScriptDialog_createJavaScriptDialog(env, reinterpret_cast<jint>(this)));
      title = env->NewStringUTF(UTF16ToUTF8(message_text_).c_str());
      switch (message_type_) {
        case content::JAVASCRIPT_MESSAGE_TYPE_ALERT:
          Java_SWEJavaScriptDialog_alert(env, java_object_.obj(), title);
          break;

        case content::JAVASCRIPT_MESSAGE_TYPE_CONFIRM:
          Java_SWEJavaScriptDialog_confirm(env, java_object_.obj(), title);
          break;

        case content::JAVASCRIPT_MESSAGE_TYPE_PROMPT:\
          prompt_text = env->NewStringUTF(UTF16ToUTF8(default_prompt_text_).c_str());
          Java_SWEJavaScriptDialog_prompt(env, java_object_.obj(), title, prompt_text);
          env->DeleteLocalRef(prompt_text);
          break;

        default:
          NOTREACHED();
      }
      env->DeleteLocalRef(title);
}

SWEJavaScriptDialog::~SWEJavaScriptDialog() {
}

void SWEJavaScriptDialog::Invalidate() {
  if (!valid_)
    return;

  valid_ = false;
  callback_.Reset();
  CloseModalDialog();
}

void SWEJavaScriptDialog::CloseModalDialog() {
  CompleteDialog();
  delete this;
}

void SWEJavaScriptDialog::CompleteDialog() {
  SWEJavascriptDialogQueue::GetInstance()->ShowNextDialog();
}

bool SWEJavaScriptDialog::Register(JNIEnv* env) {
  return RegisterNativesImpl(env);
}

void SWEJavaScriptDialog::OnResponse(JNIEnv* env, jobject obj, jint response_id, jstring value) {
  // Take the next Dialog
  CompleteDialog();
  // Check if we are still valid
  if (!valid_)
    return;
  switch (response_id) {
    case RESPONSE_OK:
      //  callback synchronously
      callback_.Run(true, ConvertJavaStringToUTF16(env, value));
      break;
    case RESPONSE_CANCEL:
      //  callback synchronously
      callback_.Run(false, string16());
      break;
    default:
      NOTREACHED();
  }
  valid_ = false;
  delete this;
}
}  // namespace content
