// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Copyright (c) 2013, The Linux Foundation. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef SWE_JAVASCRIPT_DIALOG_QUEUE_H_
#define SWE_JAVASCRIPT_DIALOG_QUEUE_H_
#pragma once

#include <deque>
#include "swe/browser/src/swe_javascript_dialog.h"

namespace content {

template <typename T> struct DefaultSingletonTraits;

// Keeps a queue of JavascriptDialogs, making sure only one js dialog is shown at a time.
// This class is a singleton.
class SWEJavascriptDialogQueue {
 public:
  // Returns the singleton instance.
  static SWEJavascriptDialogQueue* GetInstance();

  // Adds a js dialog to the queue.
  void AddDialog(SWEJavaScriptDialog* dialog);

  // Removes the js dialog in the queue (the one that is being shown).
  // Shows the next dialog in the queue, if any is present.
  void ShowNextDialog();

  // Activates and shows the current js dialog,
  void ActivateJSDialog();

  // Returns true if there is currently an active app js dialog box.
  bool HasActiveDialog() {
    return active_dialog_ != NULL;
  }

  // Accessor for |active_dialog_|.
  SWEJavaScriptDialog* active_dialog() {
    return active_dialog_;
  }

  // Iterators to walk the queue. The queue does not include the currently
  // active app js dialog box.
  typedef std::deque<SWEJavaScriptDialog*>::iterator iterator;
  iterator begin() {
    return app_js_dialog_queue_.begin();
  }

  iterator end() {
    return app_js_dialog_queue_.end();
  }

  SWEJavascriptDialogQueue();
  ~SWEJavascriptDialogQueue();
 private:
  friend struct DefaultSingletonTraits<SWEJavascriptDialogQueue>;


  // Shows dialog.
  void ShowJSDialog(SWEJavaScriptDialog* dialog);

  // Returns the next js dialog to show.
  SWEJavaScriptDialog* GetNextDialog();

  // Contains all app js dialogs which are waiting to be shown. The currently
  // active js dialog is not included.
  std::deque<SWEJavaScriptDialog*> app_js_dialog_queue_;

  // The currently active js dialog
  SWEJavaScriptDialog* active_dialog_;

  // if currently we are showing a js dialog
  bool showing_js_dialog_;

  DISALLOW_COPY_AND_ASSIGN(SWEJavascriptDialogQueue);
};

}
#endif  // SWE_JAVASCRIPT_DIALOG_QUEUE_H_
