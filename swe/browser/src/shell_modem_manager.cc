/*
 *  Copyright (c) 2013, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *      * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "jni/ModemManager_jni.h"
#include "shell_modem_manager.h"

#include "base/file_path.h"
#include "base/android/path_utils.h"
#include "cutils/log.h"
#include "net/libnetxt/dyn_lib_loader.h"
#include "net/libnetxt/libnetxt_base.h"

#undef LOG_TAG
#define LOG_TAG "Modem Warmup"

using base::android::GetNativeLibraryDirectory;

// Method definitions for the ModemManager class. The ModemManager class
// encapsulates the native implementation of any modem related functionality
// exposed to the SWE browser.
namespace content {

    // Register ModemManager's native methods through JNI.
    bool ModemManager::Register(JNIEnv* env) {
    return RegisterNativesImpl(env);
    }

    // Wrapper method to call into a libqmodem_plugin.so function that wakes
    // up the modem in anticipation of a user initiated task that requires the
    // network.
    void OnPresumeUserLoadEvent(JNIEnv* env, jclass clazz)
    {
        static void (*DoPresumeUserLoadEvent)() = NULL;
        static bool initialized = false;

        if (!initialized) {
            void* fh = LibraryManager::GetLibraryHandle("libqmodem_plugin");
            if (fh) {
                *(void **)(&DoPresumeUserLoadEvent) = LibraryManager::GetLibrarySymbol(fh, "presumeUserLoadEvent", false);
                if (!DoPresumeUserLoadEvent) {
                    LIBNETXT_LOGD("Failed to find presumeUserLoadEvent symbol in libqmodem_plugin.so in %s", __FILE__);
                    return;
                } else {
                    initialized = true;
                }
            } else {
                LIBNETXT_LOGD("Failed to load libqmodem_plugin.so in %s", __FILE__);
            }
        }

        if (DoPresumeUserLoadEvent) {
            DoPresumeUserLoadEvent();
        }
    }
} // namespace content
