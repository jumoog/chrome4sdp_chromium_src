// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Copyright (c) 2013, The Linux Foundation. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "swe/browser/src/shell_content_renderer_client.h"

#include "base/callback.h"
#include "base/command_line.h"
#include "base/sys_string_conversions.h"
#include "base/utf_string_conversions.h"
#include "content/public/common/content_constants.h"
#include "content/public/common/content_switches.h"
#include "content/public/common/url_constants.h"
#include "content/public/test/layouttest_support.h"
#include "content/shell/shell_render_process_observer.h"
#include "content/shell/shell_switches.h"
#include "content/shell/webkit_test_runner.h"
#include "third_party/WebKit/Source/WebKit/chromium/public/WebFrame.h"
#include "third_party/WebKit/Source/WebKit/chromium/public/WebPluginParams.h"
#include "third_party/WebKit/Tools/DumpRenderTree/chromium/TestRunner/public/WebTestProxy.h"
#include "third_party/WebKit/Source/WebKit/chromium/public/platform/WebURL.h"
#include "third_party/WebKit/Source/WebKit/chromium/public/platform/WebURLError.h"
#include "third_party/WebKit/Source/WebKit/chromium/public/platform/WebURLRequest.h"
#include "v8/include/v8.h"
#include "chrome/renderer/extensions/dispatcher.h"
#include "chrome/common/localized_error.h"
#include "net/base/net_errors.h"
#include "ui/base/resource/resource_bundle.h"
#include "extensions/common/constants.h"
#include "grit/renderer_resources.h"
#include "chrome/common/jstemplate_builder.h"

using WebKit::WebFrame;
using WebTestRunner::WebTestProxyBase;
using WebKit::WebString;
using extensions::Extension;

namespace content {

namespace {

bool IsLocalhost(const std::string& host) {
  return host == "127.0.0.1" || host == "localhost";
}

bool HostIsUsedBySomeTestsToGenerateError(const std::string& host) {
  return host == "255.255.255.255";
}

bool IsExternalPage(const GURL& url) {
  return !url.host().empty() &&
         (url.SchemeIs(chrome::kHttpScheme) ||
          url.SchemeIs(chrome::kHttpsScheme)) &&
         !IsLocalhost(url.host()) &&
         !HostIsUsedBySomeTestsToGenerateError(url.host());
}

}  // namespace

ShellContentRendererClient::ShellContentRendererClient() {
  if (CommandLine::ForCurrentProcess()->HasSwitch(switches::kDumpRenderTree)) {
    EnableWebTestProxyCreation(
        base::Bind(&ShellContentRendererClient::WebTestProxyCreated,
                   base::Unretained(this)));
  }
}

ShellContentRendererClient::~ShellContentRendererClient() {
}

void ShellContentRendererClient::RenderThreadStarted() {
  shell_observer_.reset(new ShellRenderProcessObserver());
  extension_dispatcher_.reset(new extensions::Dispatcher());
}

bool ShellContentRendererClient::OverrideCreatePlugin(
    RenderView* render_view,
    WebKit::WebFrame* frame,
    const WebKit::WebPluginParams& params,
    WebKit::WebPlugin** plugin) {
  std::string mime_type = params.mimeType.utf8();
  if (mime_type == content::kBrowserPluginMimeType) {
    // Allow browser plugin in content_shell only if it is forced by flag.
    // Returning true here disables the plugin.
    return !CommandLine::ForCurrentProcess()->HasSwitch(
        switches::kEnableBrowserPluginForAllViewTypes);
  }
  return false;
}

bool ShellContentRendererClient::WillSendRequest(
    WebFrame* frame,
    PageTransition transition_type,
    const GURL& url,
    const GURL& first_party_for_cookies,
    GURL* new_url) {
  CommandLine* command_line = CommandLine::ForCurrentProcess();
  if (!command_line->HasSwitch(switches::kDumpRenderTree))
    return false;
  if (!command_line->HasSwitch(switches::kAllowExternalPages) &&
      IsExternalPage(url) && !IsExternalPage(first_party_for_cookies)) {
    ShellRenderProcessObserver::GetInstance()->test_delegate()->printMessage(
        std::string("Blocked access to external URL " + url.spec() + "\n"));
    *new_url = GURL();
    return true;
  }
  *new_url = RewriteLayoutTestsURL(url);
  return true;
}

void ShellContentRendererClient::WebTestProxyCreated(RenderView* render_view,
                                                     WebTestProxyBase* proxy) {
  WebKitTestRunner* test_runner = new WebKitTestRunner(render_view);
  if (!ShellRenderProcessObserver::GetInstance()->test_delegate()) {
    ShellRenderProcessObserver::GetInstance()->SetMainWindow(render_view,
                                                             test_runner,
                                                             test_runner);
  }
  test_runner->set_proxy(proxy);
  proxy->setDelegate(
      ShellRenderProcessObserver::GetInstance()->test_delegate());
  proxy->setInterfaces(
      ShellRenderProcessObserver::GetInstance()->test_interfaces());
}

GURL ShellContentRendererClient::RewriteLayoutTestsURL(const GURL& url) {
  const char kPrefix[] = "file:///tmp/LayoutTests/";
  const int kPrefixLen = arraysize(kPrefix) - 1;

  if (url.spec().compare(0, kPrefixLen, kPrefix, kPrefixLen))
    return url;

  FilePath replace_path =
      ShellRenderProcessObserver::GetInstance()->webkit_source_dir().Append(
          FILE_PATH_LITERAL("LayoutTests/"));
#if defined(OS_WIN)
  std::string utf8_path = WideToUTF8(replace_path.value());
#else
  std::string utf8_path =
      WideToUTF8(base::SysNativeMBToWide(replace_path.value()));
#endif
  std::string new_url =
      std::string("file://") + utf8_path + url.spec().substr(kPrefixLen);
  return GURL(new_url);
}

bool ShellContentRendererClient::HasErrorPage(int http_status_code,
                                               std::string* error_domain) {
  // Use an internal error page, if we have one for the status code.
  if (!LocalizedError::HasStrings(LocalizedError::kHttpErrorDomain,
                                  http_status_code)) {
    return false;
  }

  *error_domain = LocalizedError::kHttpErrorDomain;
  return true;
}

void ShellContentRendererClient::GetNavigationErrorStrings(
    const WebKit::WebURLRequest& failed_request,
    const WebKit::WebURLError& error,
    std::string* error_html,
    string16* error_description) {
  const GURL failed_url = error.unreachableURL;
  const Extension* extension = NULL;
  const bool is_repost =
      error.reason == net::ERR_CACHE_MISS &&
      error.domain == WebString::fromUTF8(net::kErrorDomain) &&
      EqualsASCII(failed_request.httpMethod(), "POST");

  if (failed_url.is_valid() &&
      !failed_url.SchemeIs(extensions::kExtensionScheme)) {
    extension = extension_dispatcher_->extensions()->GetExtensionOrAppByURL(
        ExtensionURLInfo(failed_url));
  }

  if (error_html) {
    // Use a local error page.
    int resource_id;
    DictionaryValue error_strings;
    if (extension && !extension->from_bookmark()) {
      LocalizedError::GetAppErrorStrings(error, failed_url, extension,
                                         &error_strings);

      // TODO(erikkay): Should we use a different template for different
      // error messages?
      resource_id = IDR_ERROR_APP_HTML;
    } else {
      if (is_repost) {
        LocalizedError::GetFormRepostStrings(failed_url, &error_strings);
      } else {
        // SWE: FIXME, support locale
        LocalizedError::GetStrings(error, &error_strings, std::string("en-US")
                                    /*RenderThreadImpl::Get()->GetLocale()*/);
      }
      resource_id = IDR_NET_ERROR_HTML;
    }

    const base::StringPiece template_html(
        ResourceBundle::GetSharedInstance().GetRawDataResource(
            resource_id));
    if (template_html.empty()) {
      NOTREACHED() << "unable to load template. ID: " << resource_id;
    } else {
      // "t" is the id of the templates root node.
      *error_html = jstemplate_builder::GetTemplatesHtml(
          template_html, &error_strings, "t");
    }
  }

  if (error_description) {
    if (!extension && !is_repost)
      *error_description = LocalizedError::GetErrorDetails(error);
  }
}

}  // namespace content
