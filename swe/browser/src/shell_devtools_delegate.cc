// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Copyright (c) 2013, The Linux Foundation. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "content/shell/shell_devtools_delegate.h"

#include "base/bind.h"
#include "content/public/browser/devtools_http_handler.h"
#include "content/public/browser/web_contents.h"
#include "content/public/common/url_constants.h"
#include "swe/browser/src/tab.h"
#include "grit/shell_resources.h"
#include "net/base/tcp_listen_socket.h"
#include "ui/base/resource/resource_bundle.h"

#include "swe/browser/src/shell_browser_context.h"
#include "content/shell/shell_content_browser_client.h"
#include "ipc/ipc_message.h"

#include "base/android/jni_string.h"
#include "base/android/jni_android.h"
#include <jni.h>
#include "jni/TabManager_jni.h"

using base::android::AttachCurrentThread;
using base::android::ScopedJavaLocalRef;

#if defined(OS_ANDROID)
#include "content/public/browser/android/devtools_auth.h"
#include "net/base/unix_domain_socket_posix.h"

namespace {
  const char kSocketName[] = "swe_browser_devtools_remote";
}
#endif

namespace content {

class BrowserContext;

ShellDevToolsDelegate::ShellDevToolsDelegate(BrowserContext* browser_context,
                                             int port)
    : browser_context_(browser_context) {
  devtools_http_handler_ = DevToolsHttpHandler::Start(
#if defined(OS_ANDROID)
      new net::UnixDomainSocketWithAbstractNamespaceFactory(
          kSocketName,
          base::Bind(&CanUserConnectToDevTools)),
#else
      new net::TCPListenSocketFactory("127.0.0.1", port),
#endif
      "",
      this);
}

ShellDevToolsDelegate::~ShellDevToolsDelegate() {
}

void ShellDevToolsDelegate::Stop() {
  // The call below destroys this.
  devtools_http_handler_->Stop();
}

std::string ShellDevToolsDelegate::GetDiscoveryPageHTML() {
  return ResourceBundle::GetSharedInstance().GetRawDataResource(
      IDR_CONTENT_SHELL_DEVTOOLS_DISCOVERY_PAGE).as_string();
}

bool ShellDevToolsDelegate::BundlesFrontendResources() {
  return true;
}

FilePath ShellDevToolsDelegate::GetDebugFrontendDir() {
  return FilePath();
}

std::string ShellDevToolsDelegate::GetPageThumbnailData(const GURL& url) {
  return "";
}


RenderViewHost* ShellDevToolsDelegate::CreateNewTarget() {
  ShellBrowserContext* browserContext =
    static_cast<ShellContentBrowserClient*>(
        GetContentClient()->browser())->browser_context();

  WebContents::CreateParams create_params(browserContext, 0);
  create_params.routing_id = MSG_ROUTING_NONE;

  content::WebContents* web_contents =
      content::WebContents::Create(create_params);

  JNIEnv* env = AttachCurrentThread();
  RegisterNativesImpl(env);

  Java_TabManager_createWebContentsDelegateImpl(env,
    reinterpret_cast<jint>(web_contents));

  return web_contents->GetRenderViewHost();
}

}  // namespace content
