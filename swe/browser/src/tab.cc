// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "swe/browser/src/tab.h"

#include "base/android/jni_string.h"
#include "base/android/jni_array.h"
#include "base/pickle.h"
#include "base/logging.h"
#include "chrome/browser/ui/android/window_android_helper.h"
#include "content/public/browser/android/content_view_core.h"
#include "content/public/browser/web_contents.h"
#include "content/public/browser/browser_thread.h"
#include "third_party/WebKit/Source/Platform/chromium/public/WebLayer.h"
#include "third_party/WebKit/Source/WebKit/chromium/public/WebContextMenuData.h"
#include "ui/gfx/android/window_android.h"
#include "content/public/browser/web_contents_delegate.h"
#include "content/public/browser/notification_observer.h"
#include "swe/browser/src/shell_browser_context.h"
#include "swe/browser/src/state_serializer.h"
#include "content/shell/shell_content_browser_client.h"
#include "content/public/browser/android/content_view_layer_renderer.h"
#include "ipc/ipc_message.h"
#include "jni/WebTab_jni.h"
#include "content/components/web_contents_delegate_android/web_contents_delegate_android.h"

#include "swe/swe_log.h"

using base::android::ConvertJavaStringToUTF8;
using base::android::ConvertUTF8ToJavaString;
using base::android::ConvertUTF16ToJavaString;
using base::android::ScopedJavaLocalRef;
using ui::WindowAndroid;
using base::android::AttachCurrentThread;

namespace content {

const void* kSWEContentsUserDataKey = &kSWEContentsUserDataKey;

class SWEContentsUserData : public base::SupportsUserData::Data {
 public:
  SWEContentsUserData(Tab* ptr) : contents_(ptr) {}

  static Tab* GetContents(WebContents* web_contents) {
    if (!web_contents)
      return NULL;
    SWEContentsUserData* data = reinterpret_cast<SWEContentsUserData*>(
        web_contents->GetUserData(kSWEContentsUserDataKey));
    return data ? data->contents_ : NULL;
  }

 private:
  Tab* contents_;
};

WebContentsDelegateImpl::WebContentsDelegateImpl(Tab* tab,
    JNIEnv* env, jobject web_contents_delegate)
: WebContentsDelegateAndroid(env, web_contents_delegate),
  tab_(tab) {
    SWE_LOGV("WebContentsDelegate %p, created for tab %p", this, tab);
}

JavaScriptDialogCreator* WebContentsDelegateImpl::GetJavaScriptDialogCreator() {
  return SWEJavaScriptDialogCreator::GetInstance();
}

Tab::Tab(JNIEnv* env, jobject obj, WebContents* web_contents, WindowAndroid* window_android)
  : web_contents_(web_contents) {
    SWE_LOG_FUNC();
    java_object_.Reset(base::android::ScopedJavaLocalRef<jobject>(env, obj));
    web_contents_->SetUserData(kSWEContentsUserDataKey, new SWEContentsUserData(this));
    // FIXME: init tab helpers here
  }

Tab::~Tab() {
  SWE_LOG_FUNC();
}

void Tab::Destroy(JNIEnv* env, jobject obj) {
  SWE_LOG_FUNC();
  delete this;
}

bool Tab::Register(JNIEnv* env) {
  return RegisterNativesImpl(env);
}

// This is called by java with a webcontentsdelegate java object
// to create the corresponding native object and set it on the webcontents
// webcontents (updates) -> wcd (native) -> wcd (java)
void Tab::InitWebContentsDelegate(JNIEnv* env,
    jobject obj, jobject web_contents_delegate) {
  web_contents_delegate_.reset(
      new WebContentsDelegateImpl(this, env, web_contents_delegate));
  web_contents_->SetDelegate(web_contents_delegate_.get());
}

base::android::ScopedJavaLocalRef<jbyteArray>
Tab::GetOpaqueState(JNIEnv* env, jobject obj) {
  // Required optimization in WebViewClassic to not save any state if
  // there has been no navigations.
  if (!web_contents_->GetController().GetEntryCount())
    return ScopedJavaLocalRef<jbyteArray>();

  Pickle pickle;
  if (!WriteToPickle(*web_contents_, &pickle)) {
    return ScopedJavaLocalRef<jbyteArray>();
  } else {
    return base::android::ToJavaByteArray(env,
       reinterpret_cast<const uint8*>(pickle.data()), pickle.size());
  }
}

jboolean Tab::RestoreState(
    JNIEnv* env, jobject obj, jbyteArray state) {
  // TODO(boliu): This copy can be optimized out if this is a performance
  // problem.
  std::vector<uint8> state_vector;
  base::android::JavaByteArrayToByteVector(env, state, &state_vector);

  Pickle pickle(reinterpret_cast<const char*>(state_vector.begin()),
                state_vector.size());
  PickleIterator iterator(pickle);

  return RestoreFromPickle(&iterator, web_contents_.get());
}

void Tab::ShowContextMenu(const content::ContextMenuParams& params) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  JNIEnv* env = AttachCurrentThread();
  bool has_link = !params.unfiltered_link_url.is_empty();
  // only if it has a link
  if (has_link) {
    std::string linkURLStr(params.link_url.spec().c_str());

    ScopedJavaLocalRef<jstring> linkUrl;
    if (linkURLStr.length())
      linkUrl = ConvertUTF8ToJavaString(env, linkURLStr);

    Java_WebTab_showContextMenu(env,
                                java_object_.obj(),
                                linkUrl.obj(),
                                params.media_type);
  }
}

void Tab::HandleExternalProtocol(const string16& url) {
  DCHECK(BrowserThread::CurrentlyOn(BrowserThread::UI));
  JNIEnv* env = AttachCurrentThread();
  ScopedJavaLocalRef<jstring> reqUrl;
  reqUrl = ConvertUTF16ToJavaString(env, url);
  Java_WebTab_handleExternalProtocol(env,
                                java_object_.obj(),
                                reqUrl.obj());
}

// FIXME: can we put this in Tab
jint CreateNativeWebContents(JNIEnv* env, jclass clazz) {
  ShellBrowserContext* browserContext =
    static_cast<ShellContentBrowserClient*>(
        GetContentClient()->browser())->browser_context();

  WebContents::CreateParams create_params(browserContext, 0);
  create_params.routing_id = MSG_ROUTING_NONE;

  content::WebContents* web_contents =
      content::WebContents::Create(create_params);
  return reinterpret_cast<jint>(web_contents);
}

static jint CreateTab(JNIEnv* env, jclass clazz, jobject obj, // java tab object
    jint web_contents_ptr, jint window_android_ptr) {
  SWE_LOGI("creating a new tab (native)");
  Tab* tab = new Tab(
      env,
      obj,
      reinterpret_cast<WebContents*>(web_contents_ptr),
      reinterpret_cast<WindowAndroid*>(window_android_ptr));
  return reinterpret_cast<jint>(tab);
}

Tab* Tab::GetTabFromContents(WebContents* web_contents) {
    return SWEContentsUserData::GetContents(web_contents);
}



}
