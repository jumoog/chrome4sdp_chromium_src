/*
 *  Copyright (c) 2013, The Linux Foundation. All rights reserved.
 *  Copyright (c) 2012  The Chromium Authors. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *      * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include "swe/browser/src/swe_web_contents_view_delegate.h"

#include "content/public/browser/android/content_view_core.h"
#include "content/public/browser/web_contents.h"
#include "content/public/common/context_menu_params.h"

#include "swe/swe_log.h"
#include "swe/browser/src/tab.h"

namespace content {

// static
WebContentsViewDelegate* SWEWebContentsViewDelegate::CreateSWEWebContentsViewDelegate
    (WebContents* web_contents) {
  return new SWEWebContentsViewDelegate(web_contents);
}

SWEWebContentsViewDelegate::SWEWebContentsViewDelegate(WebContents* web_contents)
    : web_contents_(web_contents) {
}

SWEWebContentsViewDelegate::~SWEWebContentsViewDelegate() {}

WebDragDestDelegate* SWEWebContentsViewDelegate::GetDragDestDelegate() {
  // Not used in SWE
  NOTREACHED();
  return NULL;
}

void SWEWebContentsViewDelegate::ShowContextMenu(
    const ContextMenuParams& contextMenuParams,
    ContextMenuSourceType contextMenuSourceType) {
  SWE_LOGI("SWEWebContentsViewDelegate::ShowContextMenu");
  // Display paste pop-up only when selection is empty and editable.
  if (contextMenuParams.is_editable && contextMenuParams.selection_text.empty()) {
    ContentViewCore* contentViewCore = web_contents_->GetContentNativeView();
    if (contentViewCore != NULL) {
      contentViewCore->ShowPastePopup(contextMenuParams.selection_start.x(),
                                        contextMenuParams.selection_start.y());
      return;
    }
  }

  // Get the Tab instance for the given web_contents
  Tab* tab = Tab::GetTabFromContents(web_contents_);
  if (!tab)
    return;

  // Context menu callback in Chromium is triggered by WebKit
  // We let the tab implementation handle showing the require menu
  tab->ShowContextMenu(contextMenuParams);
}

}  // namespace content
