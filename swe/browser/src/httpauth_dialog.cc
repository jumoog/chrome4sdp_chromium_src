/*
 *  Copyright (c) 2013, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *      * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "swe/browser/src/httpauth_dialog.h"
#include "base/string_util.h"
#include "base/utf_string_conversions.h"
#include "content/shell/resource.h"
#include "content/shell/shell.h"
#include "base/android/scoped_java_ref.h"
#include "base/android/jni_android.h"
#include "base/android/jni_string.h"
#include "jni/HttpAuthDialog_jni.h"
#include "swe/browser/src/shell_login_dialog.h"
#include "swe/swe_log.h"

using base::android::AttachCurrentThread;
using base::android::ConvertJavaStringToUTF16;

namespace content {
// Native implementation to route displaying HttpAuth Dialogs.
// This class notifies browser using JNI to display a dialog .
// The UI is implemented using Android's AlertDialog.

HttpAuthDialog::HttpAuthDialog(
    ShellLoginDialog* login,
    const string16& message_text)
    : login_(login) {
      jstring title = 0;
      JNIEnv* env = AttachCurrentThread();
      java_object_.Reset(Java_HttpAuthDialog_createJavaScriptDialog(env, reinterpret_cast<jint>(this)));
      title = env->NewStringUTF(UTF16ToUTF8(message_text).c_str());
      Java_HttpAuthDialog_prompt(env, java_object_.obj(), title);
      env->DeleteLocalRef(title);
}

HttpAuthDialog::~HttpAuthDialog() {
}

void HttpAuthDialog::Cancel() {
}

bool HttpAuthDialog::Register(JNIEnv* env) {
  return RegisterNativesImpl(env);
}

void HttpAuthDialog::OnResponse(JNIEnv* env, jobject obj, jint response_id, jstring username, jstring password) {
  switch (response_id) {
    case RESPONSE_LOGIN:
      login_->UserAcceptedAuth(ConvertJavaStringToUTF16(env,username), ConvertJavaStringToUTF16(env,password));
      break;
    case RESPONSE_CANCEL:
      login_->UserCancelledAuth();
      break;
    case RESPONSE_DISMISS:
      login_->UserCancelledAuth();
      break;
    default:
      NOTREACHED();
  }
}
}  // namespace content
