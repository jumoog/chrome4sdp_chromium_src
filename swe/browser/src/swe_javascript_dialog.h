// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Copyright (c) 2013, The Linux Foundation. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef SWE_JAVASCRIPT_DIALOG_H_
#define SWE_JAVASCRIPT_DIALOG_H_

#include <jni.h>
#include "base/android/scoped_java_ref.h"
#include "content/public/browser/javascript_dialogs.h"
#include "content/public/browser/web_contents.h"

namespace content {

class SWEJavaScriptDialog {
 public:
  SWEJavaScriptDialog(
    WebContents* web_contents,
    JavaScriptMessageType javascript_message_type,
    const string16& message_text,
    const string16& default_prompt_text,
    const JavaScriptDialogCreator::DialogClosedCallback& callback);
  ~SWEJavaScriptDialog();
  void ActivateJSDialog();
  void ShowJSDialog();
  bool IsValid(){return valid_;};
  WebContents* web_contents(){return web_contents_;};

  // Called to invalidate a dialog mid-flight.
  void Invalidate();
  // Close the dialog
  void CloseModalDialog();
  void CompleteDialog();

  // Register the Tab's native methods through JNI.
  static bool Register(JNIEnv* env);
  // --------------------------------------------------------------------------
  // Methods called from Java via JNI
  // --------------------------------------------------------------------------
  void OnResponse(JNIEnv* env, jobject obj, jint result, jstring promptValue);
  enum { RESPONSE_CANCEL, RESPONSE_OK };


 private:
  WebContents* web_contents_;
  JavaScriptMessageType message_type_;
  string16 message_text_;
  string16 default_prompt_text_;
  JavaScriptDialogCreator::DialogClosedCallback callback_;
  base::android::ScopedJavaGlobalRef<jobject> java_object_;

  // False if the dialog should no longer be shown, e.g. because the underlying
  // tab navigated away while the dialog was queued.
  bool valid_;

  DISALLOW_COPY_AND_ASSIGN(SWEJavaScriptDialog);
};

}  // namespace content

#endif  // SWE_JAVASCRIPT_DIALOG_H_
