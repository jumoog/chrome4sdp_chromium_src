/*
 *  Copyright (c) 2013, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *      * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <jni.h>
#include "swe/browser/src/browser_preferences.h"
#include "base/android/path_utils.h"
#include "base/android/jni_android.h"
#include "base/android/jni_string.h"
#include "jni/BrowserPreferences_jni.h"

#undef LOG_TAG
#define LOG_TAG "BrowserPreferences"

using base::android::AttachCurrentThread;
using base::android::GetClass;
using base::android::ScopedJavaLocalRef;

namespace content {

    struct FieldIds {
        jfieldID javascript_enabled;
        jfieldID javascript_popups_enabled;

        void Initialize(JNIEnv* env, const ScopedJavaLocalRef<jclass>& clazz);
    };

    void FieldIds::Initialize(JNIEnv* env,const ScopedJavaLocalRef<jclass>& clazz) {
        javascript_enabled = GetStaticFieldID(env, clazz, "mJavaScriptEnabled", "Z");
        javascript_popups_enabled = GetStaticFieldID(env, clazz, "mAllowPopupsEnabled", "Z");
    }

    struct FieldIds g_field_ids = {0};

    BrowserPreferences::BrowserPreferences(){
        JNIEnv* env = AttachCurrentThread();
        ScopedJavaLocalRef<jclass> clazz = GetClass(env, kBrowserPreferencesClassPath);
        g_field_ids.Initialize(env, clazz);
    }

    bool BrowserPreferences::Register(JNIEnv* env) {
        return RegisterNativesImpl(env);
    }

    bool BrowserPreferences::getJavaScriptEnabled(){
        JNIEnv* env = AttachCurrentThread();
        ScopedJavaLocalRef<jclass> clazz = GetClass(env, kBrowserPreferencesClassPath);
        bool isJSEnabled = env->GetStaticBooleanField(clazz.obj(),g_field_ids.javascript_enabled);
        return isJSEnabled;
    }

    bool BrowserPreferences::getJavaScriptPopUpsEnabled(){
        JNIEnv* env = AttachCurrentThread();
        ScopedJavaLocalRef<jclass> clazz = GetClass(env, kBrowserPreferencesClassPath);
        bool isJSPopupsEnabled = env->GetStaticBooleanField(clazz.obj(),g_field_ids.javascript_popups_enabled);
        return isJSPopupsEnabled;
    }

    bool BrowserPreferences::getPrefBoolean(const char* prefKey){
      jstring pref = 0;
      JNIEnv* env = AttachCurrentThread();
      pref = env->NewStringUTF(prefKey);
      Java_BrowserPreferences_getPreferenceBoolean(env, pref);
      env->DeleteLocalRef(pref);
      return true;
    }

} // namespace content