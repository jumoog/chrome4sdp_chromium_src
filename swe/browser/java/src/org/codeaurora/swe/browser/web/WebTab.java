// Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
// Copyright (c) 2012 The Chromium Authors. All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.


package org.codeaurora.swe.browser.web;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.chromium.base.CalledByNative;
import org.chromium.base.JNINamespace;
import org.chromium.content.browser.ContentView;
import org.chromium.content.browser.ContentViewRenderView;
import org.chromium.content.browser.ContentViewRenderViewFlex;
import org.chromium.content.browser.LoadUrlParams;
import org.chromium.content.browser.WebContentsObserverAndroid;
import org.chromium.content.components.web_contents_delegate_android.WebContentsDelegateAndroid;
import org.chromium.content.common.CommandLine;
import org.chromium.ui.gfx.NativeWindow;
import org.codeaurora.swe.browser.Tab;
import org.codeaurora.swe.browser.Intention;
import org.codeaurora.swe.browser.utils.Logger;

import org.codeaurora.swe.browser.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.util.Log;

/**
 * The basic Java representation of a tab.  Contains and manages a {@link ContentView}.
 */
@JNINamespace("content")
public class WebTab extends WebTabLayout implements Tab, WebTabLayout.Listener {

    // The following constant matches the one in
    // content/public/common/page_transition_types.h.
    // Add more if you need them.
    public static final int PAGE_TRANSITION_RELOAD = 8;

    private final NativeWindow mWindow;
    private ContentView mContentView = null;
    private int mNativeTab = 0;
    private WebContentsDelegateAndroid mWebContentsDelegate;
    private ContentViewRenderView mRenderTarget;
    private String mUrl = "";
    private ContentViewRenderViewFlex mRenderTargetFlex;
    private final Context mContext;
    //private final WebTabLayout mTabLayout;
    private Matrix bitmapMatrix;
    private boolean mLoaded;
    private boolean mUseDesktopUserAgent;
    private int mNativeWebContents;
    private final List<Tab.Listener> mListeners = new ArrayList<Tab.Listener>();
    private ClientDelegate mClientDelegate;
    private WebContentsObserverAndroid mWebContentsObserver;

    //progress
    private boolean mbMainFrameStartedLoading = false;
    private int mProgress = 0;

    // Restored variables
    private byte[] mRestoredState;
    private String mRestoredTitle;
    private String mRestoredSnapshotFilename;
    private Bitmap mBitmap;


    boolean mIsTabSaved = false;


    public interface ClientDelegate {
        boolean addNewContents(int nativeSourceWebContents, int nativeWebContents,
                int disposition, Rect initialPosition, boolean userGesture);
    }

    public WebTab(Activity activity, Intention args) {
        super(activity);

        // initialize web engine
        WebApplicationGlue.ensureNativeSWEInitialized(activity);

        boolean available = ContentViewRenderViewFlex.checkBypassAvailability();
        Logger.debug("checking for bypass availability: " + available);

        if (!CommandLine.getInstance().hasSwitch(CommandLine.DISABLE_COMPOSITOR_BYPASS) && !available) {
            CommandLine.getInstance().appendSwitch(CommandLine.DISABLE_COMPOSITOR_BYPASS);
        }

        mContext = activity;
        mWindow = WebApplicationGlue.getActivityNativeWindow();
        mNativeWebContents = args.mNativeWebContents;
        mRestoredState = args.mState;
        mRestoredTitle = args.mTitle != null ? args.mTitle : "";
        mRestoredSnapshotFilename = args.mSnapshotFilename;

        super.setListener(this);
        super.setupRenderTarget();

        if (mWebContentsDelegate == null)
            mWebContentsDelegate = new WebContentsDelegateImpl();
    }

    public void setClientDelegate(ClientDelegate delegate) {
        mClientDelegate = delegate;
    }

    //View:
    @Override
    public void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged (changedView, visibility);
        if (mLoaded) {
            if (visibility == View.VISIBLE)
                mContentView.onShow();
            else
                mContentView.onHide();
        }
    }

    // TabLayoutListener interface
    @Override
    public void onReadyToRender(ContentViewRenderViewFlex renderTarget) {
        Logger.debug("onReadyToRender");

        mRenderTargetFlex = renderTarget;
        // This method gets called whenever a surface is created
        // tab switch will cause the surface to be destroyed
        // and recreated when selected.
        // but we need call setup only once
        if (!mLoaded)
            setupContentsandLoadUrl();
    }

    @Override
    public void onReadyToRender(ContentViewRenderView renderTarget) {
        Logger.debug("onReadyToRender");

        mRenderTarget = renderTarget;
        // This method gets called whenever a surface is created
        // tab switch will cause the surface to be destroyed
        // and recreated when selected.
        // but we need call setup only once
        if (!mLoaded)
            setupContentsandLoadUrl();
    }


    //Tab implementation
    @Override
    public void onActivityPause() {
        if (mLoaded)
            mContentView.onActivityPause();
    }

    @Override
    public void onActivityResume() {
        if (mLoaded)
            mContentView.onActivityResume();
    }

    @Override
    public void onShow() {
        if(!isNativeActive())
            restoreContentLayer();

        if (mLoaded)
            mContentView.onShow();
    }

    @Override
    public void onHide() {
        if (mLoaded) {
            mContentView.onHide();
            if (mRenderTargetFlex != null) {
                mRenderTargetFlex.detachRenderer();
            }
        }
    }

    @Override
    public void loadUrl(String url) {
        Logger.debug("loadUrl " + url);
        android.util.Log.d("Pageload", "PageLoadStarted:startLoadingURL: " + url);
        mUrl = url;

        if (mLoaded) {
            loadUrlWithSanitization(url);
        }
    }

    @Override
    public void goBack() {
        if (mLoaded)
            mContentView.goBack();
    }

    @Override
    public void goForward() {
        if (mLoaded)
            mContentView.goForward();
    }

    @Override
    public void onRendererAttached() {
        //Schedule an invalidate while using the bypass compositor as when a new surface
        //is created and set as the target surface for the renderer, the renderer has to
        //be asked to repaint the page, else a black screen will show till an invalidate
        //is triggered by a touch event or a js repaint call
        if (mLoaded)
            mContentView.scheduleInvalidate();
    }

    @Override
    public boolean canGoBack() {
        return (mLoaded && mContentView.canGoBack());
    }

    @Override
    public boolean canGoForward() {
        return (mLoaded && mContentView.canGoForward());
    }

    /**
     * @return WebTab's serialized state. Return mRestoredState, if mNativeTab
     * has not yet been created through setupContentsandLoadUrl().
     */
    @Override
    public byte[] getState() {
        return (mNativeTab == 0 ? mRestoredState : nativeGetOpaqueState(mNativeTab));
    }

    @Override
    public Embodiment getEmbodiment() {
       return Embodiment.E_Web;
    }

    @Override
    public void stopLoading() {
        if (mLoaded)
            mContentView.stopLoading();
    }

    @Override
    public int getCurrentLoadProgress() {
        if (!mLoaded)
            return 0;
        return mProgress;
    }

    @Override
    public void reload() {
        if (mLoaded)
            mContentView.reload();
    }

    @Override
    public String getUrl() {
        return mUrl;
    }

    @Override
    public void setUrl(String url) {
        mUrl = url;
        updateRenderer();
    }

    /**
     * @return page title of the selected navigation entry. Return mRestoredTitle,
     * if mContentView has not yet been created through setupContentsandLoadUrl().
     */
    @Override
    public String getTitle() {
        return (mContentView == null ? mRestoredTitle : mContentView.getTitle());
    }

    @Override
    public boolean getUseDesktopUserAgent() {
        return mLoaded ? mContentView.getUseDesktopUserAgent() : mUseDesktopUserAgent;
    }

    @Override
    public void setUseDesktopUserAgent(boolean value) {
        if (!mLoaded) {
            mUseDesktopUserAgent = value;
        } else if (mUseDesktopUserAgent != value) {
            mUseDesktopUserAgent = value;
            // Set useDesktopUserAgent and reload the page
            if (mUseDesktopUserAgent != mContentView.getUseDesktopUserAgent())
                mContentView.setUseDesktopUserAgent(mUseDesktopUserAgent, true);
        }
    }

    @Override
    public void addListener(Tab.Listener li) {
        mListeners.add(li);
    }

    @Override
    public void removeListener(Tab.Listener li) {
        mListeners.remove(li);
    }

    @Override
    public Bitmap getSnapshot(int width, int height) {
        FileInputStream fis;
        // Ensure we always return a bitmap
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        try {
            if (mContentView != null) {
                if (mRenderTargetFlex != null) {
                    bitmap = mRenderTargetFlex.captureSnapshot(width, height);
                } else {
                    // Get page snapshot
                    bitmap = mContentView.getBitmap(width, height);
                }
                // Remove snapshot file if it exists
                if (mRestoredSnapshotFilename != null) {
                    File file = getContext().getFileStreamPath(mRestoredSnapshotFilename);
                    if (file.exists())
                        file.delete();
                    mRestoredSnapshotFilename = null;
                }
            } else {
                // Restore snapshot from file
                if (mRestoredSnapshotFilename != null) {
                    fis = mContext.openFileInput(mRestoredSnapshotFilename);
                    if (fis != null) {
                        Bitmap tempBitmap = BitmapFactory.decodeStream(fis);
                        if (tempBitmap != null ) {
                            bitmap = Bitmap.createScaledBitmap(tempBitmap,
                                    width, height, false);
                        }
                        fis.close();
                    }
                } else {
                  if (mBitmap != null)
                      bitmap = mBitmap;
                }
            }
            return bitmap;
        }
        catch (IOException e) {
            e.printStackTrace();
            Logger.error("Exception = " + e.toString());
            return bitmap;
        }
    }

    private void saveTabState() {
        mRestoredState = getState();
        mRestoredTitle = getTitle();
        int width, height;
        width = height =  mContext.getResources().getDimensionPixelSize(R.dimen.TilesSize);
        mBitmap = getSnapshot(width, height);
    }

    public void killContentLayer() {
        Logger.debug("killContentLayer: Url"+ mUrl);
        if (isNativeActive()) {
            saveTabState();
            onHide();
            destroy();
            mIsTabSaved = true;
        }
    }

    public void restoreContentLayer() {

      if(!mIsTabSaved)
          return;

      Logger.debug("restoreContentLayer");
      super.setupRenderTarget();
    }

    public boolean isNativeActive() {
        return mLoaded;
    }


    /**
     * Should be called when the tab is no longer needed.  Once this is called this tab should not
     * be used.
     */
    @Override
    public void destroy() {
        destroyContentView();

        // FIXME: can this be null
        if (mNativeTab != 0) {
            nativeDestroy(mNativeTab);
            mNativeTab = 0;
        }
    }

    private void setupContentsandLoadUrl() {
        Logger.info("setupContentsandLoadUrl");
        boolean bOpenedByEngine = (mNativeWebContents != 0);

        // Build the WebContents and the ContentView/ContentViewCore
        if (!bOpenedByEngine) {
            Logger.debug("creating webcontents");
            mNativeWebContents = nativeCreateNativeWebContents();
        }

        Logger.debug("creating contentview");
        mContentView = ContentView.newInstance(mContext, mNativeWebContents, mWindow,
                ContentView.PERSONALITY_CHROME);

        Logger.debug("creating tab");
        mNativeTab = nativeCreateTab(this, mNativeWebContents, mWindow.getNativePointer());

        // We need the java webcontentsdelegate pointer to create the native webcontents
        Logger.debug("creating WebContentsDelegate");
        nativeInitWebContentsDelegate(mNativeTab, mWebContentsDelegate);

        // Restore Tab state
        if (mRestoredState != null) {
            if (nativeRestoreState(mNativeTab, mRestoredState)) {
                // The onTitleUpdated callback normally happens when a page is
                // loaded, but is optimized out in the restore state case because
                // the title is already restored. See WebContentsImpl::UpdateTitleForEntry.
                // So we call the callback explicitly here.
                mWebContentsDelegate.onTitleUpdated();
            } else {
                Logger.error("WebTab.setupContentsandLoadUrl: Unable to restore state");
            }
            mRestoredState = null;
        }

        // add the contentview to the tab
        //mTabLayout.addView(mContentView);
        addView(mContentView);
        mContentView.requestFocus();

        mWebContentsObserver = new WebContentsObserverAndroid(mContentView.getContentViewCore()) {
                @Override
                public void didFailLoad(boolean isProvisionalLoad, boolean isMainFrame, int errorCode,
                    String description, String failingUrl) {
                    for (Tab.Listener li : mListeners)
                        li.didFailLoad(isProvisionalLoad, isMainFrame, errorCode, description, failingUrl);
                }

                @Override
                public void didStartProvisionalLoadForFrame(long frameId,
                    long parentFrameId, boolean isMainFrame, String validatedUrl,
                    boolean isErrorPage) {
                    if (isMainFrame) {
                        mbMainFrameStartedLoading = true;
                        mProgress = 0;
                    }
                    // Notify the listeners
                    for (Tab.Listener li : mListeners)
                        li.onLoadStarted(isMainFrame);
                }

                @Override
                public void didFinishLoad(long frameId,boolean isMainFrame,
                    String validatedUrl) {
                    if (isMainFrame) {
                        mbMainFrameStartedLoading = false;
                        mProgress = 0;
                    }
                    // Notify the listeners
                    for (Tab.Listener li : mListeners)
                        li.onLoadStopped(isMainFrame);
                }

                @Override
                public void didStartLoading(String url) {
                    // Notify the listeners
                    for (Tab.Listener li : mListeners)
                        li.didStartLoading(url);
                }

                @Override
                public void didStopLoading(String url) {
                    // Notify the listeners
                    for (Tab.Listener li : mListeners)
                        li.didStopLoading(url);
                }
            };

        if (CommandLine.getInstance().hasSwitch(CommandLine.DISABLE_COMPOSITOR_BYPASS)) {
            mRenderTarget.setCurrentContentView(mContentView);
        } else {
            updateRenderer();
        }


        loadUrlWithSanitization(mUrl);
        mLoaded = true;
    }

    public void updateRenderer() {
        if (!CommandLine.getInstance().hasSwitch(CommandLine.DISABLE_COMPOSITOR_BYPASS)) {
            int rendererId = mContentView.getContentViewCore().getPendingRendererId();
            int renderWidgetId = mContentView.getContentViewCore().getPendingRoutingId();
            int currentRendererId = mContentView.getContentViewCore().getCurrentRendererId();
            int currentRoutingId = mContentView.getContentViewCore().getCurrentRenderWidgetId();
            if (currentRendererId != -1 && currentRoutingId != -1) {
               if (mRenderTargetFlex != null) {
                   mRenderTargetFlex.attachRendererToView(currentRendererId, currentRoutingId);
               }
            }
        }
    }

    private void destroyContentView() {
        if (mContentView == null)
            return;

        removeView(mContentView);
        mContentView.destroy();
        mContentView = null;
        mNativeWebContents = 0;
        mLoaded = false;
    }

    /**
     * Navigates this Tab's {@link ContentView} to a sanitized version of {@code url}.
     * @param url The potentially unsanitized URL to navigate to.
     */
    public void loadUrlWithSanitization(String url) {
        if (url == null || "".equals(url))
            return;

        // Sanitize the URL.
        url = sanitizeUrl(url);

        // Invalid URLs will just return empty.
        if (TextUtils.isEmpty(url)) return;

        if (TextUtils.equals(url, mContentView.getUrl())) {
            mContentView.reload();
        } else {
            Logger.debug("loading url " + url);
            LoadUrlParams params = new LoadUrlParams(url);
            params.setOverrideUserAgent(mUseDesktopUserAgent ?
                                            LoadUrlParams.UA_OVERRIDE_TRUE :
                                            LoadUrlParams.UA_OVERRIDE_FALSE);
            mContentView.loadUrl(params);
        }
    }

    // FIXME: need to use URLFixer in chrome/net
    private static String sanitizeUrl(String url) {
        if (url == null) return url;
        if (url.startsWith("www.") || url.indexOf(":") == -1) url = "http://" + url;
        return url;
    }

    private void setWebContentsDelegate(WebContentsDelegateAndroid webContentsDelegate) {
        mWebContentsDelegate = webContentsDelegate;
    }

    @CalledByNative
    public void showContextMenu(String linkUrl, int mediaType) {
        Logger.debug("showContextMenu URL : " + linkUrl + " mediaType : " + mediaType );
        for (Tab.Listener li : mListeners)
            li.showContextMenu(linkUrl);
    }

    @CalledByNative
    public void handleExternalProtocol(String url) {
        Logger.debug("handleExternalProtocol URL : " + url);
        if (url != null) {
            Intent intent = null;
            if (url.startsWith("tel:") || url.startsWith("market:") || url.startsWith("rtsp:") ) {
                intent = new Intent("android.intent.action.VIEW", Uri.parse(url));
                mContext.startActivity(intent);
            }
        }
    }

    // instance
    private native void nativeInitWebContentsDelegate(int nativeTab, Object mWebContentsDelegate);

    // Returns null if save state fails.
    private native byte[] nativeGetOpaqueState(int nativeTab);

    // Returns false if restore state fails.
    private native boolean nativeRestoreState(int nativeTab, byte[] state);

    // static
    private static native int nativeCreateTab(Object Tab, int nativeWebContents, int windowAndroidPtr);
    private static native int nativeCreateNativeWebContents();
    private static native void nativeDestroy(int nativeTab);

    // This class allows swe to overide methods in chrome implementation
    // This class only routes signals from the active tab
    private class WebContentsDelegateImpl extends WebContentsDelegateAndroid {

        @Override
        public void onLoadProgressChanged(int progress) {
            if (mbMainFrameStartedLoading) {
                Logger.debug("onLoadProgressChanged progress :" + progress);
                mProgress = progress;
                // Send progress only for main frame
                for (Tab.Listener li : mListeners)
                    li.onLoadProgressChanged(progress);
            }
        }

        @Override
        public void onUpdateUrl(String url) {
            setUrl(url);

            for (Tab.Listener li : mListeners)
                li.onUpdateUrl(url);
        }

        @Override
        public void onLoadStarted() {
        }

        @Override
        public void onLoadStopped() {
        }

        // Invoked by the native layer when requesting a new tab, the URL is loaded
        // automatically by the web contents and updated to the UI layer
        @Override
        public boolean addNewContents(int nativeSourceWebContents, int nativeWebContents,
                int disposition, Rect initialPosition, boolean userGesture) {
            if (mClientDelegate != null) {
                return mClientDelegate.addNewContents(nativeSourceWebContents, nativeWebContents,
                        disposition, initialPosition, userGesture);
            }
            return false;
        }
    }

    // Method added for Instrumentation Testing
    public ContentView getContentView() {
        return mContentView;
    }

    @Override
    public String getSnapshotFilename() {
        return mRestoredSnapshotFilename;
    }

}
