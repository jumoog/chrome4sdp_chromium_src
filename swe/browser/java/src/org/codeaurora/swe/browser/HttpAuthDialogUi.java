/*
 *  Copyright (c) 2013, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *      * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package org.codeaurora.swe.browser;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

/**
 * UI for HttpAuth Dialogs.
 */
public class HttpAuthDialogUi  {

    private final Context mContext;
    private final ImplInterface mImpl;

    /**
     * Abstraction to separate the implementation from the UI.
     */
    public static interface ImplInterface {
        public static final int CANCEL = 0;
        public static final int LOGIN = 1;
        public static final int DISMISS = 2;

        public void onResponse(int response, String username, String password);
    }

    public HttpAuthDialogUi(Context context, ImplInterface obj) {
        mContext = context;
        mImpl = obj;
    }

    public void dialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View httpAuthView = inflater.inflate(R.layout.http_auth, null);

        // Chrome let you dismiss the dialog following the same
        builder.setCancelable(true);

        final TextView messageText = ((TextView)httpAuthView.findViewById(R.id.http_auth_message));
        messageText.setText(message);

        builder.setView(httpAuthView);

        // Populate the title & URL
        final EditText userNameBox = ((EditText)httpAuthView.findViewById(R.id.http_auth_username));
        final EditText passwordBox = ((EditText)httpAuthView.findViewById(R.id.http_auth_password));

        builder.setPositiveButton(R.string.login, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                String username = null;
                String password = null;

                if (userNameBox != null)
                    username = userNameBox.getText().toString().trim();
                if(passwordBox != null)
                    password = passwordBox.getText().toString().trim();
                mImpl.onResponse(ImplInterface.LOGIN, username, password);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                mImpl.onResponse(ImplInterface.CANCEL, null, null);
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                //dialog is dismissed
                mImpl.onResponse(ImplInterface.DISMISS, null, null);
            }
        });
        builder.show();
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

    }

}
