// Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
// Copyright (c) 2012 The Chromium Authors. All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.codeaurora.swe.browser.web;

import org.chromium.content.browser.ContentViewRenderView;
import org.chromium.content.browser.ContentViewRenderViewFlex;
import org.chromium.content.common.CommandLine;
import org.codeaurora.swe.browser.utils.Logger;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.util.Log;

class WebTabLayout extends FrameLayout {

    private ContentViewRenderView mRenderTarget;
    private ContentViewRenderViewFlex mRenderTargetFlex;
    private Listener mListener;

    public interface Listener {

        /**
         * This is invoked once the view is ready to render.
         * @param renderTarget
         */
        public void onReadyToRender(ContentViewRenderViewFlex renderTarget);
        public void onReadyToRender(ContentViewRenderView renderTarget);
        public void onRendererAttached();

    };

    public WebTabLayout(Context context) {
        super(context);
        Logger.warn("new TabLayout");
    }

    public WebTabLayout(Context context, AttributeSet attrSet) {
        super(context, attrSet);
        Logger.warn("new TabLayout");
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    // setup renderTarget (surfaceView) lazily
    public void setupRenderTarget() {
        if (!CommandLine.getInstance().hasSwitch(CommandLine.DISABLE_COMPOSITOR_BYPASS)) {
            Logger.debug("adding a ContentViewRenderViewFlex");
            if (mRenderTargetFlex != null) {
                return;
            }

            Logger.warn("creating RenderTarget");
            mRenderTargetFlex = new ContentViewRenderViewFlex(getContext(), true, 0) {
                @Override
                    protected void onReadyToRender() {
                    Logger.warn("RenderTarget ready");
                        if (mListener != null) {
                            mListener.onReadyToRender(mRenderTargetFlex);
                        }
                    }

                @Override
                    protected void onRendererAttached() {
                        Logger.warn("Renderer attached");
                        if(mListener != null) {
                            mListener.onRendererAttached();
                        }
                    }
            };

            // add this rendertarget to tab
            addView(mRenderTargetFlex,
                    new FrameLayout.LayoutParams(
                            FrameLayout.LayoutParams.MATCH_PARENT,
                            FrameLayout.LayoutParams.MATCH_PARENT));
        } else {
            Log.d("WebTabLayout","Creating default view");
            if (mRenderTarget != null) {
                return;
            }

            Logger.warn("creating RenderTarget");
            mRenderTarget = new ContentViewRenderView(getContext()) {
                @Override
                    protected void onReadyToRender() {
                    Logger.warn("RenderTarget ready");
                        if (mListener != null) {
                            mListener.onReadyToRender(mRenderTarget);
                        }
                    }
            };

            // add this rendertarget to tab
            addView(mRenderTarget,
                    new FrameLayout.LayoutParams(
                            FrameLayout.LayoutParams.MATCH_PARENT,
                            FrameLayout.LayoutParams.MATCH_PARENT));

        }
    }
}

