/*
 *  Copyright (c) 2013, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *      * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package org.codeaurora.swe.power;

import org.codeaurora.swe.browser.Tab;
import org.codeaurora.swe.browser.TabManager;
import org.codeaurora.swe.browser.TabManager.TabData;
import org.codeaurora.swe.browser.utils.Logger;
import org.codeaurora.swe.browser.preferences.BrowserPreferences;

import dalvik.system.PathClassLoader;

import android.content.Context;
import android.os.Handler;

// A class of methods that publish events to trigger Power optimizations.
public class BrowserPower implements Tab.Listener, TabManager.Listener{

    // BrowserMgmt Plugin Handlers
    static private Class mBrowserMgmtClassType = null;
    static private Object mBrowserMgmtInst = null;
    static private Class[] args_types = null;
    static private Context[] args_val = null;
    static private final String BROWSERPOWERPLUGINNAME =
               "/system/framework/browsermanagement.jar";
    static private final String BROWSERPOWERCLASSNAME =
               "com.android.qualcomm.browsermanagement.BrowserManagement";
    private BrowserPreferences mBrowserPrefs;

    private Context mContext;
    private final TabManager mTabManager;

    static private BrowserPower mBrowserPowerInstance = null;

    // Should be called only once
    static public BrowserPower create(Context context) {
        if (mBrowserPowerInstance == null ) {
            mBrowserPowerInstance = new BrowserPower(context);
        }
        return mBrowserPowerInstance;
    }

    static public BrowserPower getInstance() {
        return mBrowserPowerInstance;
    }

    private BrowserPower(Context browserContext) {
        mContext = browserContext;

        mTabManager = TabManager.getInstance();
        mTabManager.addListener(this);

        mBrowserPrefs = BrowserPreferences.getInstance();
        setupBrowserMgmtPlugin();
    }

    public void onDestroy() {
        mTabManager.removeListener(this);
        if (mBrowserMgmtClassType != null) {
            try {
                mBrowserMgmtClassType.getMethod("Destroy",args_types).
                invoke(mBrowserMgmtInst,(Object)args_val[0]);
            } catch ( Exception e) {
                Logger.error("BrowserPower:"+"Destroy "+e);
            }
        }
    }

    // TabManager Events
    @Override
    public void onTabAdded(TabData td, int idx) {}

    @Override
    public void onTabRemoved(TabData td, int idx) {}

    @Override
    public void onTabSelected(TabData td, boolean bSelected) {

        if (bSelected == false) {
            td.tab.removeListener(this);
            return;
        }
        td.tab.addListener(this);
    }

    @Override
    public void onTabShow(TabData tab) {}

    // Tab.Listener implementation
    @Override
    public void onLoadProgressChanged(int progress) {}

    @Override
    public void onUpdateUrl(String url) {}

    @Override
    public void onLoadStarted(boolean isMainFrame) {}

    @Override
    public void onLoadStopped(boolean isMainFrame) {}

    @Override
    public void didStartLoading(String url) {}

    @Override
    public void didStopLoading(String url){

        boolean IsBrowserManagementOn = mBrowserPrefs.getPowerSavingsEnabled();

        if ((mBrowserMgmtClassType != null) && (IsBrowserManagementOn)) {
            Logger.debug("BrowserPower:"+"PageLoadFinished");
            try {
                mBrowserMgmtClassType.getMethod("PageLoadFinished",args_types).
                invoke(mBrowserMgmtInst,(Object)args_val[0]);
            } catch (Exception e) {
                Logger.error("BrowserPower:"+"PageLoadFinished "+e);
            }
        }
    }

    @Override
    public void didFailLoad(boolean isProvisionalLoad, boolean isMainFrame,
                        int errorCode, String description, String failingUrl) {
    }

    @Override
    public void showContextMenu(String url) { }

    private void setupBrowserMgmtPlugin() {

        boolean IsBrowserManagementOn = mBrowserPrefs.getPowerSavingsEnabled();
        Logger.debug("BrowserPower:"+"sys prop is "+ IsBrowserManagementOn);

        if ((IsBrowserManagementOn) && (mBrowserMgmtInst == null)) {
            //allocate mem only first time - since they are declared static
            try {
                PathClassLoader pluginClassLoader =
                new PathClassLoader(
                BROWSERPOWERPLUGINNAME,ClassLoader.getSystemClassLoader());
                mBrowserMgmtClassType =
                        pluginClassLoader.loadClass(BROWSERPOWERCLASSNAME);
            } catch (ClassNotFoundException e) {
                Logger.error("BrowserPower:"+"Class Load failed "+e);
            }

            try {
                mBrowserMgmtInst = mBrowserMgmtClassType.newInstance();
                args_types = new Class[1];
                args_val = new Context[1];
                args_types[0] = Context.class;
                Logger.debug("BrowserPower:"+"BrowserMgmt First Instance ");
            } catch(Exception e) {
                Logger.error("BrowserPower:"+"BrowserMgmt Instance failed "+e);
            }

            if ((mBrowserMgmtClassType != null) && (mContext != null) &&
                                           (mBrowserMgmtInst != null)) {
                args_val[0] = mContext;
                try {
                    mBrowserMgmtClassType.getMethod("Init",args_types).
                    invoke(mBrowserMgmtInst,(Object)args_val[0]);
                } catch ( Exception e) {
                    Logger.error("BrowserPower:"+"Init "+ e);
                }
            }
        } else {
            Logger.debug("BrowserPower:"+"BrowserMgmt Instance already available ");
        }
    }
}
