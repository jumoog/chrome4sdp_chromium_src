/*
 *  Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *      * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package org.codeaurora.swe.browser.test;

import org.codeaurora.swe.browser.Tab.Embodiment;

public class TabManagerTest extends SWETest {

    // milliseconds to wait after loading URL
    private static final int SLEEP = 0;

    private static final int TABS_TO_ADD = 1; // <= URLs.length
    private static final int TABS_TO_CLOSE = 1; // <= TABS_TO_ADD

    private static final String[] URLs = {
        "www.codeaurora.org"
    };

    public void testPreConditions() {
        assertNotNull("Browser activity should not be null", mActivity);
        assertNotNull("Tab manager should not be null", mTabManager);
        assertNotNull("Active Tab should not be null", mTab);

        // Active Tab should be a Web tab
        assertEquals("Active Tab's Embodiment is", Embodiment.E_Web,
                mTab.getEmbodiment());

    }

    // Create TABS_TO_ADD number of Tabs and check the resulting number of Tabs.
    public void testCreateAndCloseTabs() throws Throwable {

        int nTabs = mTabManager.getTabsCount();

        // Create TABS_TO_ADD number of tabs and check resulting number of tabs.
        for (int i = 0; i < TABS_TO_ADD; i++ ) {
            loadUrl(mActivity, mTabManager, URLs[i], true, SLEEP);
        }

        assertEquals("Number of Tabs after adding new tabs should be ",
                nTabs + TABS_TO_ADD, mTabManager.getTabsCount());

        // Close TABS_TO_CLOSE number of tabs and check resulting number of tabs.
        for (int i = 0; i < TABS_TO_CLOSE; i++ )
            closeCurrentTab(mTabManager);

        assertEquals("Number of Tabs after removing tabs should be ",
                nTabs + TABS_TO_ADD - TABS_TO_CLOSE, mTabManager.getTabsCount());

    }

    public void testLoadInSameTab() throws Throwable {

        int nTabs = mTabManager.getTabsCount();

        // Load a page in the current tab
        loadUrl(mActivity, mTabManager, URLs[0], false, SLEEP);

        // Check that no new tabs were added
        assertEquals("Initial number of Tabs should be ", nTabs,
                mTabManager.getTabsCount());
    }

}
