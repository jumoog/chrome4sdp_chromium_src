/*
 *  Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *      * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package org.codeaurora.swe.browser.test;

import android.test.suitebuilder.annotation.SmallTest;
import org.codeaurora.swe.browser.Tab.Embodiment;
import org.codeaurora.swe.browser.utils.Logger;

import org.codeaurora.swe.browser.AbstractTabListener;
import org.codeaurora.swe.browser.Intention;
import org.codeaurora.swe.browser.Intention.Type;
import org.codeaurora.swe.browser.TabManager;
import org.codeaurora.swe.browser.web.WebTab;

public class UserAgentSwitchTest extends SWETest {

    // milliseconds to wait after loading URL
    private static final int SLEEP = 2 * 1000;
    protected static int WAIT_TIMEOUT_SECONDS = 10;
    private static final String DESKTOP_URL_1 = "www.youtube.com";
    private static final String MOBILE_URL_1 = "m.youtube.com";
    private static final String TAG = "SWE_TEST";

    private static Object onPageFinishWaiter;

    @SmallTest
    public void testPreConditions() {
        assertNotNull("Browser activity should not be null", mActivity);
        assertNotNull("Tab manager should not be null", mTabManager);
        assertNotNull("Active Tab should not be null", mTab);

        // Active Tab should be a Web tab
        assertEquals("Active Tab's Embodiment is", Embodiment.E_Web,
                mTab.getEmbodiment());

        // Default User Agent should be mobile
        assertEquals("Default useDesktopUserAgent is", false,
                mTabManager.getActiveTab().getUseDesktopUserAgent());
    }

    public void  loadUrlSync(final String url) throws Throwable {

        onPageFinishWaiter = new Object();

        //create a new tab
        runTestOnUiThread( new Runnable() {
            @Override
            public void run() {
                mTabManager.setTabIntention(
                    new Intention(Type.I_Consume, url));
                WebTab tab = (WebTab) mTabManager.getActiveTab();
                tab.addListener(
                    new AbstractTabListener(){
                        @Override
                        public void onLoadStopped(boolean isMainFrame) {
                            if (isMainFrame) {
                                synchronized (onPageFinishWaiter) {
                                    onPageFinishWaiter.notifyAll();
                                }
                            }
                        }
                    }// end of anonymous class
                );
            }
        });

        synchronized (onPageFinishWaiter) {
            try {
               onPageFinishWaiter.wait(WAIT_TIMEOUT_SECONDS*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public void openUrlInNewTabSync(final String url) throws Throwable {

        onPageFinishWaiter = new Object();

        //create a new tab
        runTestOnUiThread( new Runnable() {
            @Override
            public void run() {
                mTabManager.setTabIntention(
                    new Intention(Type.I_OpenAndConsume, url));
                WebTab tab = (WebTab) mTabManager.getActiveTab();
                tab.addListener(
                    new AbstractTabListener(){
                        @Override
                        public void onLoadStopped(boolean isMainFrame) {
                            if (isMainFrame) {
                                synchronized (onPageFinishWaiter) {
                                    onPageFinishWaiter.notifyAll();
                                }
                            }
                        }
                    }// end of anonymous class
                );
            }
        });

        synchronized (onPageFinishWaiter) {
            try {
              onPageFinishWaiter.wait(WAIT_TIMEOUT_SECONDS*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public String getActiveUrl()
    {
        WebTab tab = (WebTab)mTabManager.getActiveTab();
        return tab.getUrl();
    }


    // Default User Agent in new tab should be mobile
    @SmallTest
    public void testDefaultUserAgent() throws Throwable {

        // Create new Tab and check User Agent is mobile in new tab.
        openUrlInNewTabSync(DESKTOP_URL_1);
        assertEquals("New Tab default useDesktopUserAgent is", false,
                mTabManager.getActiveTab().getUseDesktopUserAgent());

        // Set User Agent to desktop in current tab, create new tab
        // and check User Agent is mobile in new tab
        setUserAgent(mTabManager, "desktop");

        openUrlInNewTabSync(DESKTOP_URL_1);
        assertEquals("New Tab default useDesktopUserAgent is", false,
                mTabManager.getActiveTab().getUseDesktopUserAgent());
    }


    // When User Agent is mobile we should always get mobile URLs
    @SmallTest
    public void testMobileUserAgent() throws Throwable {

        // Set User Agent to Mobile in current tab
        setUserAgent(mTabManager, "mobile");

        // Check User Agent is Mobile in current tab
        assertEquals(" useDesktopUserAgent should be ", false,
               mTabManager.getActiveTab().getUseDesktopUserAgent());

        loadUrlSync(MOBILE_URL_1);

        // Request mobile URL in current tab and get mobile
        assertTrue("Loaded URL should contain " + MOBILE_URL_1,
                getActiveUrl().contains(MOBILE_URL_1));

         // Reverify that desktop agent has not changed
        assertEquals(" useDesktopUserAgent should be ", false,
               mTabManager.getActiveTab().getUseDesktopUserAgent());

        loadUrlSync(DESKTOP_URL_1);
        // Request desktop URL in current tab and get mobile
        assertTrue("Loaded URL should contain " + MOBILE_URL_1,
                 getActiveUrl().contains(MOBILE_URL_1));

    }


    // When User Agent is desktop we should get
    //  - mobile URL when requested URL is mobile
    //  - desktop URL when requested URL is desktop
    @SmallTest
    public void testDesktopUserAgent() throws Throwable {

        // Set User Agent to Desktop in current tab
        setUserAgent(mTabManager, "desktop");

        // TODO: Investigate why without loading a page below assertEquals fails.
        loadUrlSync("about:blank");

        // Check User Agent is Desktop in current tab
        assertEquals(" useDesktopUserAgent should be ", true,
               mTabManager.getActiveTab().getUseDesktopUserAgent());

        // Request mobile URL in current tab and get mobile
        loadUrlSync(MOBILE_URL_1);
        assertTrue("Loaded URL should contain " + MOBILE_URL_1,
               getActiveUrl().contains(MOBILE_URL_1));
    }


   @SmallTest
   public void testSwitchUserAgent() throws Throwable {

       // Set User Agent to Mobile in current tab
       setUserAgent(mTabManager, "mobile");

       // Request mobile URL in current tab and get mobile
       loadUrlSync(MOBILE_URL_1);
       assertTrue("Loaded URL should contain " + MOBILE_URL_1,
              getActiveUrl().contains(MOBILE_URL_1));

       // Set User Agent to Desktop in current tab
       setUserAgent(mTabManager, "desktop");

       // Request desktop URL in current tab and get desktop
       loadUrlSync(DESKTOP_URL_1);
       assertTrue("Loaded URL should contain " + DESKTOP_URL_1,
                getActiveUrl().contains(DESKTOP_URL_1));

       // Set User Agent to Mobile in current tab
       setUserAgent(mTabManager, "mobile");

       // Request mobile URL in current tab and get mobile
       loadUrlSync(MOBILE_URL_1);
       assertTrue("Loaded URL should contain " + MOBILE_URL_1,
              getActiveUrl().contains(MOBILE_URL_1));
   }
}
