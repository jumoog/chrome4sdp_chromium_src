/*
 *  Copyright (c) 2013, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *      * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package org.codeaurora.swe.browser.test;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.MediumTest;
import android.test.suitebuilder.annotation.SmallTest;
import android.test.suitebuilder.annotation.Smoke;
import android.util.Log;

import java.util.*;

import org.apache.http.Header;
import org.apache.http.HttpRequest;

import org.chromium.base.test.util.Feature;
import org.chromium.base.test.util.DisabledTest;

import org.codeaurora.swe.browser.AbstractTabListener;
import org.codeaurora.swe.browser.Bookmark;
import org.codeaurora.swe.browser.BookmarkManager;
import org.codeaurora.swe.browser.BrowserActivity;
import org.codeaurora.swe.browser.Intention;
import org.codeaurora.swe.browser.Intention.Type;
import org.codeaurora.swe.browser.MostFrequentManager;
import org.codeaurora.swe.browser.MostFrequentManager.MostFrequent;
import org.codeaurora.swe.browser.preferences.BrowserPreferences;
import org.codeaurora.swe.browser.preferences.PreferenceKeys;
import org.codeaurora.swe.browser.Tab;
import org.codeaurora.swe.browser.TabManager;
import org.codeaurora.swe.browser.TabManager.TabData;
import org.codeaurora.swe.browser.web.WebTab;
import org.codeaurora.swe.browser.test.util.TestWebServer;

/**
 * A test suite for testing basic features of Browser
 */
public class SmokeTest extends ActivityInstrumentationTestCase2<BrowserActivity> {

    private TabManager mTabManager;
    private static Object onPageFinishWaiter;
    private BrowserActivity mActivity;

    private static final String TAG = "SWE_TEST";
    protected static int WAIT_TIMEOUT_SECONDS = 10;
    private String[] fileList = { "/p0foo.html", "/p1foo.html", "/p2foo.html"};
    private String [] titleList = {"page 0 title foo0", "page 1 title foo1", "page 2 title foo2",};

    @Override
    public void setUp() throws Exception {
        super.setUp();

    }

    @Override
    protected void tearDown() throws Exception {
        try {
            Log.v(TAG,"SWETest.tearDown()");
            closeAllTabs();
            super.tearDown();
            Thread.sleep(1000);
        } catch (Throwable e) {
            throw new Exception(e);
        }
    }

    public SmokeTest() {
        super(BrowserActivity.class);
    }

    private static final String getHTML(String title) {
        StringBuilder html = new StringBuilder();
        html.append("<html><head>");
        if (title != null) {
            html.append("<title>" + title + "</title>");
        }

        html.append("</head><body>BODY</body></html>");
        return html.toString();
    }

    private static final String getGoogleNewsPage(String[] titleList, String[] urlList) {
        StringBuilder html = new StringBuilder();
        html.append("<!doctype html><html><head>");
        html.append("<meta name=\"viewport\" content=\"width=device-width\"/>");
        html.append("<title>Google News</title>");
        html.append("<script type=\"text/javascript\">");
        html.append("var test=function() {");
        html.append("  var interval = 2000;  var count = 0; var aTags = document.getElementsByTagName('a');");
        html.append("  for (var i = 0; i < "+urlList.length+"; i++){ ");
        html.append("     window.setTimeout( function(){  ");
        html.append("       window.open(aTags[count++].href,'_blank');");
        html.append("     }, interval*(i+1) );");
        html.append("  }");
        html.append("};//end of test function</script>");
        html.append("</head><body onload=\" test();\"><h1>Google News</h1><br/>");

        for (int i = 0, n = titleList.length; i < n; i++){
            html.append("<p><a href=\""+urlList[i]+"\">"+titleList[i]+"</a></p>");
        }

        html.append("</body></html>");
        return html.toString();
    }


    /**
    * Loads the url in  the current tab
    **/
    public void loadUrlSync(final String url) throws Throwable {

        //Initiating the SWE Browser Activity
        mActivity = getActivity();
        mTabManager = TabManager.getInstance();
        onPageFinishWaiter = new Object();
        //create a new tab
        runTestOnUiThread( new Runnable() {
            @Override
            public void run() {
                mTabManager.setTabIntention(
                    new Intention(Type.I_Consume, url));
                WebTab tab = (WebTab) mTabManager.getActiveTab();
                tab.addListener(
                    new AbstractTabListener(){
                        public void onLoadStopped(boolean isMainFrame) {
                            if (isMainFrame) {
                                synchronized (onPageFinishWaiter) {
                                    onPageFinishWaiter.notifyAll();
                                }
                            }
                        }
                    }// end of anonymous class
                );
            }
        });

        synchronized (onPageFinishWaiter) {
            try {
               Log.v(TAG, "onPageFinishWaiter waiting...");
               onPageFinishWaiter.wait(WAIT_TIMEOUT_SECONDS*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Set User Agent.
     * @param agent Set to either "desktop" or "mobile".
     */
    protected void setUserAgent(String agent) throws Throwable {
        if (agent.toLowerCase() == "desktop") {
            runTestOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTabManager.getActiveTab().setUseDesktopUserAgent(true);
                }
            });
        } else if (agent.toLowerCase() == "mobile") {
            runTestOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTabManager.getActiveTab().setUseDesktopUserAgent(false);
                }
            });
        } else {
            throw new Exception("Invalid agent parameter");
        }
    }

    public void verifyTitle(String expectedTitle) {
        WebTab tab = (WebTab) mTabManager.getActiveTab();
        Log.v(TAG, "current tab title = "+tab.getTitle());
        assertEquals("Title should be "+expectedTitle, expectedTitle, tab.getTitle());
    }


    public void goBack() throws Throwable {
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mTabManager.getActiveTab().canGoBack()) {
                    mTabManager.getActiveTab().goBack();
                }
            }
        });
    }

    protected void killActivity() throws Throwable {
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                getInstrumentation().callActivityOnDestroy(mActivity);
            }
        });
    }

    protected void closeAllTabs() throws Throwable {
        mTabManager = TabManager.getInstance();
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                while ( mTabManager.getTabsCount() > 0 ) {
                    mTabManager.closeTab(mTabManager.getActiveTabData());
                }
            }
        });
    }

    /**
    * Loads listUrl in new tab
    **/
    @Smoke
    @SmallTest
    @Feature({"SWE", "Main"})
    public void testLoadMultipleurls() throws Throwable {
        loadUrlSync("about:blank");
        TestWebServer webServer = null;
         try {

            webServer = new TestWebServer(false);
            int pageCount = 0;

            for (int i = 0,n = fileList.length ; i < n; i++, pageCount++) {
                final String url = webServer.setResponse(fileList[i], getHTML(titleList[i]), null);
                loadUrlSync(url);
                //allowing rendering of page to complete
                Thread.sleep(500);
                verifyTitle(titleList[i]);
            }

            loadUrlSync("about:blank");

            //reload the pages in reverse order and verify the titles again
            for(int i = fileList.length-1 ; i >= 0; i--) {
                goBack();
                //allowing rendering of page to complete
                Thread.sleep(500);
                verifyTitle(titleList[i]);
            }

        } finally {
            if (webServer != null) {
                webServer.shutdown();
            }
        }
    }

    /**
    * test opening links in new tab with target="_blank"
    **/
    @Smoke
    @MediumTest
    @Feature({"SWE", "Main"})
    public void testGoogleNewsSimulation() throws Throwable {
        loadUrlSync("http://www.google.com");

        //disable popup blocking
        BrowserPreferences browserPrefs = BrowserPreferences.getInstance();
        boolean needToBlockPopups = false;
        if (!browserPrefs.getPopupsEnabled()) {
            needToBlockPopups = true;
            browserPrefs.setPreference(PreferenceKeys.PREF_BLOCK_POPUPS, true);
        }

        TestWebServer webServer = null;
        try {
            webServer = new TestWebServer(false);
            String[] newsUrlList = new String[fileList.length];
            String[] newsTitleList = new String[fileList.length];
            for (int i = 0, n = fileList.length;  i < n; i++) {
                newsTitleList[i] = "News_"+(i+1);
                newsUrlList[i]  = webServer.setResponse(fileList[i], getHTML(newsTitleList[i]), null);
            }

            final String newsMainUrl = webServer.setResponse("/news.html", getGoogleNewsPage(newsTitleList, newsUrlList), null);
            loadUrlSync(newsMainUrl);
            Thread.sleep(10000);

            //verify the tab count
            assertEquals("Tabs should now be 4", mTabManager.getTabsCount(), 4);

            //verifying the titles for each page:-
            WebTab tab;
            tab = (WebTab) mTabManager.getTab(0);
            assertEquals("First tab should be Google News", tab.getTitle(), "Google News");
            for (int i = 0, n = fileList.length; i < n; i++) {
                tab = (WebTab) mTabManager.getTab(i+1);
                assertEquals("Title should be "+newsTitleList[i], tab.getTitle(), newsTitleList[i]);
            }
        } finally {
            if (webServer != null) {
                webServer.shutdown();
            }

            if (needToBlockPopups) {
                browserPrefs.setPreference(PreferenceKeys.PREF_BLOCK_POPUPS, false);
            }
        }
    }

    /**
    * test bookmark operations
    **/
    @Smoke
    @MediumTest
    @Feature({"SWE", "Main"})
    public void testBookmark() throws Throwable {
        loadUrlSync("http://www.google.com");
        Thread.sleep(3000);

        TestWebServer webServer = null;
        try {
            String expectedTitle = titleList[0];
            webServer = new TestWebServer(false);
            String bookmarkUrl = webServer.setResponse(fileList[0], getHTML(titleList[0]), null);
            String anotherUrl =  webServer.setResponse(fileList[1], getHTML(titleList[1]), null);

            loadUrlSync(bookmarkUrl);
            Thread.sleep(2000);

            // get the bookmarkManager
            BookmarkManager sBookmarkManager =  BookmarkManager.getInstance();

            String title = mTabManager.getActiveTab().getTitle().trim();
            String unfilteredUrl = mTabManager.getActiveTab().getUrl().trim();

            // add the bookmark
            sBookmarkManager.addBookmark(title, unfilteredUrl);

            // load some other url
            loadUrlSync(anotherUrl);
            Thread.sleep(2000);

            // verify whether bookmark contains the the expected data
            Bookmark b = null;
            b = sBookmarkManager.getBookmark(unfilteredUrl);
            assertNotNull("Bookmark should exist", b);
            assertEquals("Bookmark title should be "+expectedTitle, b.getTitle(), expectedTitle);
            assertEquals("Bookmark url should be "+unfilteredUrl, b.getUrl(), unfilteredUrl);

            // delete the bookmark
            sBookmarkManager.deleteBookmark(b);

            // bookmark should not exist anymore
            b = sBookmarkManager.getBookmark(unfilteredUrl);
            assertNull("Bookmark should NOT exist", b);

        } finally {
            if (webServer != null) {
                webServer.shutdown();
            }
        }
    }

    /**
    * test most frequent entry operations
    **/
    @Smoke
    @MediumTest
    @Feature({"SWE", "MostFrequentEntry"})
    public void testMostFrequentEntry() throws Throwable {
        TestWebServer webServer = null;
        try {
                webServer = new TestWebServer(false);
                String[] urlList = new String[fileList.length];
                String[] mfTitleList = new String[fileList.length];
                String[] mfFileList = new String[fileList.length];
                for (int i = 0,n = fileList.length;  i < n; i++) {
                    mfTitleList[i] = "MostFrequent_"+(i+1);
                    mfFileList[i] = "/mf_"+(i+1)+".html";
                    urlList[i]  = webServer.setResponse(mfFileList[i], getHTML(mfTitleList[i]), null);
                }

                // load the urls a couple of times in a cycle to assure that
                // they are placed in most frequent entry
                int len = urlList.length;
                int urlCounter = 7;
                for (int i = 0, n = len*urlCounter; i < n; i++) {
                    loadUrlSync(urlList[i%len]);
                    Thread.sleep(100);
                }

                // verify that urls are present in the most frequent entry
                int verifyCount = 0;
                MostFrequentManager sMostFrequentManager = MostFrequentManager.getInstance();
                List<MostFrequent> frequentEntries = sMostFrequentManager.getAllMostFrequents();

                for (int i = 0, n = frequentEntries.size(); i < n; i++) {
                    MostFrequent current = frequentEntries.get(i);
                    for (int j = 0; j < len; j++) {
                        if ( ( current.url.equals(urlList[j]) ) && (current.title.equals(mfTitleList[j])) ){
                            verifyCount++;
                        }
                    }
                }
                assertEquals(" MostFrequent should contain "+len+" entries", len, verifyCount);
        } finally {
            if (webServer != null) {
                webServer.shutdown();
            }
        }
    }

    /**
    * test most frequent entry operations
    **/
    @Smoke
    @MediumTest
    @Feature({"SWE", "MostFrequentEntry"})
    public void testUrlSensitivity() throws Throwable {
        TestWebServer webServer = null;
        String mixedCaseurl;
        String mixedCaseFilePath = "/samplePage.html";
        try {
                //loadUrlSync("about:blank");
                webServer = new TestWebServer(false);
                mixedCaseurl = webServer.setResponse(mixedCaseFilePath, getHTML("mixedCaseUrl"), null);

                setActivityInitialTouchMode(false);

                // Construct Intent with INITIAL_URL data
                // removing the intent with tab:load
                // this will check both url sensitivity and  with intent action = null
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                intent.setData(Uri.parse(mixedCaseurl));
                intent.setComponent(new ComponentName(getInstrumentation().getTargetContext(),
                    BrowserActivity.class));

                setActivityIntent(intent);
                mActivity = getActivity();
                mTabManager = TabManager.getInstance();
                Thread.sleep(1000);
                assertEquals(1, webServer.getRequestCount(mixedCaseFilePath));
        } finally {
            if (webServer != null) {
                webServer.shutdown();
            }
        }
    }

    /**
    * test most frequent entry operations
    **/
    @Smoke
    @MediumTest
    @Feature({"SWE", "MostFrequentEntry"})
    public void testUserAgentString() throws Throwable {
        Header header;
        HttpRequest request;
        String mixedCaseurl;
        String mixedCaseFilePath = "/samplePage.html";
        TestWebServer webServer = null;
        try {
                loadUrlSync("about:blank");
                setUserAgent("mobile");
                webServer = new TestWebServer(false);
                mixedCaseurl = webServer.setResponse(mixedCaseFilePath, getHTML("mixedCaseUrl"), null);
                loadUrlSync(mixedCaseurl);
                request = webServer.getLastRequest(mixedCaseFilePath);
                assertNotNull("Request cannot be NULL", request);
                Header[] matchingHeaders = request.getHeaders("User-Agent");
                assertEquals(1, matchingHeaders.length);
                header = matchingHeaders[0];
                assertTrue(" 'Mobile' should be present in the user agent String " ,
                    (header.getValue()).contains("Mobile"));


                setUserAgent("desktop");
                loadUrlSync(mixedCaseurl);
                request = webServer.getLastRequest(mixedCaseFilePath);
                assertNotNull("Request cannot be NULL", request);
                matchingHeaders = request.getHeaders("User-Agent");
                assertEquals(1, matchingHeaders.length);
                header = matchingHeaders[0];
                Log.v(TAG, "Desk UA: "+header.getValue());
                assertFalse(" User agent string should not cotnain 'Mobile' ",(header.getValue()).contains("Mobile"));
        } finally {
            if (webServer != null) {
                webServer.shutdown();
            }
        }
    }
}