/*
 *  Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *      * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package org.codeaurora.swe.browser.test;

import java.util.concurrent.TimeUnit;

import org.chromium.content.browser.ContentViewCore;
import org.chromium.content.browser.test.util.TestCallbackHelperContainer.OnPageFinishedHelper;
import org.chromium.content.browser.test.util.TestWebContentsObserver;
import org.codeaurora.swe.browser.BrowserActivity;
import org.codeaurora.swe.browser.Intention;
import org.codeaurora.swe.browser.Tab;
import org.codeaurora.swe.browser.Intention.Type;
import org.codeaurora.swe.browser.TabManager;
import org.codeaurora.swe.browser.web.WebTab;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.test.ActivityInstrumentationTestCase2;

public class SWETest extends ActivityInstrumentationTestCase2<BrowserActivity> {
    private TestWebContentsObserver mTestWebContentsObserver;
    private OnPageFinishedHelper mOnPageFinishedHelper;

    private static final String INITIAL_URL = "www.codeaurora.org";

    protected BrowserActivity mActivity;
    protected TabManager mTabManager;
    protected Tab mTab;

    public SWETest() {
        super(BrowserActivity.class);
    } // end of SWETest constructor definition

    @Override
    public void setUp() throws Exception {
        super.setUp();
        //Initialize SWE Browser Activity

        // Explicitly set the Activity touch mode to false
        setActivityInitialTouchMode(false);

        // Construct Intent with INITIAL_URL data
        Intent intent = new Intent("tab:load");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setData(Uri.parse(INITIAL_URL));

        //  Inject Intent into SWE Browser activity
        intent.setComponent(new ComponentName(getInstrumentation().getTargetContext(),
              BrowserActivity.class));
        setActivityIntent(intent);
        mActivity = getActivity();
        mTabManager = TabManager.getInstance();
        mTab = mTabManager.getActiveTab();
    }

    @Override
    protected void tearDown() throws Exception {
        try {
            closeAllTabs();
            super.tearDown();
            Thread.sleep(1000);
        } catch (Throwable e) {
            throw new Exception(e);
        }
    }

    protected void killActivity(final BrowserActivity activity) throws Throwable {
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                getInstrumentation().callActivityOnDestroy(activity);
            }
        });
    }

    protected void recreateActivity(final BrowserActivity activity) throws Throwable {
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.recreate();
            }
        });
    }

    protected void closeAllTabs() throws Throwable {
        final TabManager tabManager = TabManager.getInstance();
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                while ( tabManager.getTabsCount() > 0 ) {
                    tabManager.closeTab(tabManager.getActiveTabData());
                }
            }
        });
    }

   /**
    * Requests a URL and returns the loaded URL.
    * @param inNewTab If inNewTab == true it loads the URL in a new Tab,
    *                 otherwise, it loads the URL in the current (active) Tab.
    * @param sleepTime Number of milliseconds to sleep after the page finished
    *                  loading.
    */
    protected String loadUrl(BrowserActivity activity, TabManager tabManager,
            String url, Boolean inNewTab, int sleepTime) throws Throwable {
        final TabManager mTabManager = tabManager;
        final String mUrl = url;
        WebTab mWebTab = (WebTab) tabManager.getActiveTab();

        ContentViewCore mContentViewCore = mWebTab.getContentView().getContentViewCore();
        int currentCallCount;

        mTestWebContentsObserver =
                new TestWebContentsObserver(mContentViewCore);
        mOnPageFinishedHelper =
                  mTestWebContentsObserver.getOnPageFinishedHelper();
        currentCallCount = mOnPageFinishedHelper.getCallCount();

        if (inNewTab == false) {
            runTestOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTabManager.setTabIntention(new Intention(Type.I_Consume, mUrl));
                }
            });
        } else {
            runTestOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTabManager.setTabIntention(new Intention(Type.I_OpenAndConsume, mUrl));
                }
            });
        }

        // Wait for the page to load
        mOnPageFinishedHelper.waitForCallback(currentCallCount, 1,
                40, TimeUnit.SECONDS);

        // Sleep if requested
        if (sleepTime > 0)
            Thread.sleep(sleepTime);

        // Return loaded URL
        return mOnPageFinishedHelper.getUrl();
    }

   /**
    * Delete current (active) Tab.
    */
    protected void closeCurrentTab(TabManager tabManager) throws Throwable {
        final TabManager mTabManager = tabManager;

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mTabManager.getTabsCount() > 0)
                    mTabManager.closeTab(mTabManager.getActiveTabData());
            }
        });
    }

    /**
     * Set User Agent.
     * @param agent Set to either "desktop" or "mobile".
     */
    protected void setUserAgent(TabManager tabManager, String agent) throws Throwable {
        final TabManager mTabManager = tabManager;

        if (agent.toLowerCase() == "desktop") {
            runTestOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTabManager.getActiveTab().setUseDesktopUserAgent(true);
                }
            });
        } else if (agent.toLowerCase() == "mobile") {
            runTestOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTabManager.getActiveTab().setUseDesktopUserAgent(false);
                }
            });
        } else {
            throw new Exception("Invalid agent parameter");
        }
    }
}
