/*
 *  Copyright (c) 2012-2013, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *      * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package org.codeaurora.swe.browser.test;

import android.test.suitebuilder.annotation.SmallTest;

import org.chromium.base.test.util.Feature;

import org.codeaurora.swe.browser.AbstractTabListener;
import org.codeaurora.swe.browser.Intention;
import org.codeaurora.swe.browser.Intention.Type;
import org.codeaurora.swe.browser.TabManager;
import org.codeaurora.swe.browser.web.WebTab;

public class LoadUrlTest extends SWETest {

    private static Object onPageFinishWaiter;

    // milliseconds to wait after loading URL
    // TODO: If set to 0, the tests fail. Need to find better way to assess a
    // page finished loading since WebContentsObserverAndroid.didStopLoading()
    // does not seem to do the job.
    protected static int WAIT_TIMEOUT_SECONDS = 10;
    private static final String[] DOMAIN_LIST = {
        "codeaurora.org",
        "qualcomm.com"
    };

    public void  loadUrlSync(final String url) throws Throwable {

        onPageFinishWaiter = new Object();
//        boolean timeoutOccured = false;

        //create a new tab
        runTestOnUiThread( new Runnable() {
            @Override
            public void run() {
                mTabManager.setTabIntention(
                    new Intention(Type.I_Consume, url));
                WebTab tab = (WebTab) mTabManager.getActiveTab();
                tab.addListener(
                    new AbstractTabListener(){
                        @Override
                        public void onLoadStopped(boolean isMainFrame) {
                            if (isMainFrame) {
                                synchronized (onPageFinishWaiter) {
                                    onPageFinishWaiter.notifyAll();
                                }
                            }
                        }
                    }// end of anonymous class
                );
            }
        });

        synchronized (onPageFinishWaiter) {
            try {
               onPageFinishWaiter.wait(WAIT_TIMEOUT_SECONDS*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public String getActiveUrl()
    {
        WebTab tab = (WebTab)mTabManager.getActiveTab();
        return tab.getUrl();
    }

    // Default User Agent should be mobile
    @SmallTest
    @Feature({"SWE", "Main"})
    public void testLoadUrl() throws Throwable {

        // Successively load URLs from DOMAIN_LIST in current tab and check
        // that loaded URL contains the requested domain string
        for (int i = 0; i < DOMAIN_LIST.length; i++ ) {
            loadUrlSync(DOMAIN_LIST[i]);
            assertTrue("Loaded URL should contain requested domain " + DOMAIN_LIST[i],(getActiveUrl()).contains(DOMAIN_LIST[i]));
        }
    }
}
