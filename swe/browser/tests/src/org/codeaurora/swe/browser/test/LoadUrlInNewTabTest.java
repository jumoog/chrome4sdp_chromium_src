/*
 *  Copyright (c) 2013, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *      * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package org.codeaurora.swe.browser.test;

import android.test.suitebuilder.annotation.SmallTest;
import android.test.suitebuilder.annotation.Smoke;
import android.util.Log;

import org.chromium.base.test.util.Feature;
import org.chromium.base.test.util.DisabledTest;

import org.codeaurora.swe.browser.AbstractTabListener;
import org.codeaurora.swe.browser.Intention;
import org.codeaurora.swe.browser.Intention.Type;
import org.codeaurora.swe.browser.web.WebTab;
import org.codeaurora.swe.browser.test.util.TestWebServer;


/**
 * A test suite for ContentView.getTitle().
 */
public class LoadUrlInNewTabTest extends SWETest {

    private static Object onPageFinishWaiter;

    private static final String TAG = "SWE_TEST";
    private static final String TITLE = "TITLE";
    protected static int WAIT_TIMEOUT_SECONDS = 15;
    private final String[] fileList = { "/get_title_test.html","/get_title_test_empty.html", "/get_title_test_no_title.html"};
    private final String [] titleList = {TITLE, "", null};

    private static final String getHTML(String title) {
        StringBuilder html = new StringBuilder();
        html.append("<html><head>");
        if (title != null) {
            html.append("<title>" + title + "</title>");
        }
        html.append("</head><body>BODY</body></html>");
        return html.toString();
    }

    public void openUrlInNewTabSync(final String url) throws Throwable {

        onPageFinishWaiter = new Object();

        //create a new tab
        runTestOnUiThread( new Runnable() {
            @Override
            public void run() {
                mTabManager.setTabIntention(
                    new Intention(Type.I_OpenAndConsume, url));
                WebTab tab = (WebTab) mTabManager.getActiveTab();
                tab.addListener(
                    new AbstractTabListener(){
                        @Override
                        public void onLoadStopped(boolean isMainFrame) {
                            if (isMainFrame) {
                                synchronized (onPageFinishWaiter) {
                                    onPageFinishWaiter.notifyAll();
                                }
                            }
                        }
                    }// end of anonymous class
                );
            }
        });

        synchronized (onPageFinishWaiter) {
            try {
               Log.v(TAG, "onPageFinishWaiter waiting...");
               onPageFinishWaiter.wait(WAIT_TIMEOUT_SECONDS*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
    * Loads listUrl in new tab
    **/
    @Smoke
    @SmallTest
    @Feature({"SWE", "Main"})
    public void testLoadurlInNewTab() throws Throwable {
        Log.v(TAG, "Load URL in new TAB");
        TestWebServer webServer = null;
        try {
            webServer = new TestWebServer(false);
            for ( int i = 0,n = fileList.length ; i < n; i++) {
                final String url = webServer.setResponse(fileList[i], getHTML(titleList[i]), null);
                openUrlInNewTabSync(url);
                //allowing rendering of page to complete
                Thread.sleep(100);
            }
        } finally {
            if (webServer != null) {
                webServer.shutdown();
            }
        }
    }
}
