/*
 *  Copyright (c) 2013, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *      * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package org.codeaurora.swe.browser.test;

import android.test.suitebuilder.annotation.SmallTest;
import android.util.Base64;

import org.codeaurora.swe.browser.Tab;
import org.codeaurora.swe.browser.TabManager;
import org.codeaurora.swe.browser.test.util.CommonResources;
import org.codeaurora.swe.browser.test.util.TestWebServer;
import org.codeaurora.swe.browser.utils.Logger;

import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.Callable;

/**
 * A test suite for persistent Tab state.
 */
public class SaveRestoreStateTest extends SweTestBase {

    private String mUrls[];
    private TestWebServer mWebServer;

    private static final int NUM_NAVIGATIONS = 5;
    private static final String TITLES[] = {
        "page 0 title foo0", "page 1 title foo1", "page 2 title foo2",
        "page 3 title foo3", "page 4 title foo4", "page 5 title foo5"
    };
    private static final String PATHS[] = {
        "/p0foo.html", "/p1foo.html", "/p2foo.html", "/p3foo.html",
        "/p4foo.html", "/p5foo.html"
    };

    private static final String REAL_URLS[] = {
        "http://www.quicinc.com/",
        "http://www.codeaurora.org/"
    };

    private class State {
        public int mNumTabs;
        public int mActiveTabIndex;
        public TabState[] tabs;

        public State(int numTabs) {
            this.mNumTabs = numTabs;
            this.tabs = new TabState [numTabs];
        }

        public boolean isSame(State state) {
            assertEquals("Number of tabs should be", state.mNumTabs, this.mNumTabs);
            assertEquals("Active Tab index should be", state.mActiveTabIndex,
                    this.mActiveTabIndex);

            for (int i=0; i < state.mNumTabs; i++) {
                assertEquals("Embodiment should be", state.tabs[i].mEmbodiment,
                        this.tabs[i].mEmbodiment);
                assertEquals("Title should be", state.tabs[i].mTitle,
                        this.tabs[i].mTitle);
                assertEquals("Tab state hash should be", state.tabs[i].mHash,
                        this.tabs[i].mHash);
                assertEquals("Tab parent index should be", state.tabs[i].parentIndex,
                        this.tabs[i].parentIndex);
            }
            return true;
        }
    }

    private class TabState {
        public Tab.Embodiment mEmbodiment;
        public String mTitle;
        public String mHash;
        public int parentIndex;

        public TabState(Tab.Embodiment embodiment, String title, byte[] state,
                int parentIndex) {
            this.mEmbodiment = embodiment;
            this.mTitle = title;
            this.mHash = (state != null) ? getHash(state) : "";
            this.parentIndex = parentIndex;
        }

        // returns the hash of the provided byte array
        private String getHash(byte[] byteArray) {
            try {
                if (byteArray != null) {
                    MessageDigest digester = MessageDigest.getInstance("MD5");
                    ByteArrayInputStream in = new ByteArrayInputStream(byteArray);
                    int byteCount;

                    while ((byteCount = in.read(byteArray, 0, byteArray.length)) > 0)
                        digester.update(byteArray, 0, byteCount);
                    return Base64.encodeToString(digester.digest(), Base64.DEFAULT);
                } else {
                    return null;
                }
            }
            catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    private byte[] getTabStateOnUiThread(final int i) {
        try {
            return runTestOnUiThreadAndGetResult(new Callable<byte[]>() {
                @Override
                public byte[] call() throws Exception {
                    return TabManager.getInstance().getTab(i).getState();
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }

    private State getState() {
        TabManager tabManager = TabManager.getInstance();
        int numTabs = tabManager.getTabsCount();
        Tab tab;

        if (numTabs == 0)
            return null;

        State state = new State(numTabs);
        for (int i=0; i < numTabs; i++) {
            tab = tabManager.getTab(i);

            state.tabs[i] = new TabState(
                    tab.getEmbodiment(),
                    tab.getTitle(),
                    getTabStateOnUiThread(i),
                    tabManager.getTabParentIndex(tabManager.getTabData(i)));
            if (tab == tabManager.getActiveTab()) {
                state.mActiveTabIndex = i;
            }
        }
        return state;
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();

        // Instantiate TestWebServer
        mWebServer = new TestWebServer(false);

        // Set TestWebServer response
        mUrls = new String[NUM_NAVIGATIONS];
        for (int i = 0; i < NUM_NAVIGATIONS; ++i) {
            String html = CommonResources.makeHtmlPageFrom(
                    "<title>" + TITLES[i] + "</title>",
                    "");
            mUrls[i] = mWebServer.setResponse(PATHS[i], html, null);
        }
    }

    @Override
    public void tearDown() throws Exception {
        if (mWebServer != null) {
            Logger.info("SRS_SaveRestoreStateTest.tearDown: Started server shutdown");
            mWebServer.shutdown();
            Logger.info("SRS_SaveRestoreStateTest.tearDown: Server shutdown complete");
        }
        super.tearDown();
    }

    @SmallTest
    public void testSaveRestoreState() throws Throwable {
        State state, restoredState;

        // Load fake and real URLs on same Tab
        for (int i = 1; i < mUrls.length; ++i) {
            loadUrlAsync(mUrls[i], false);
            // Sleep 1 second in between
            // TODO: Use listeners instead
            Thread.sleep(1000);
        }
        for (int i = 1; i < REAL_URLS.length; ++i) {
            loadUrlAsync(REAL_URLS[i], false);
            // Sleep 1 second in between
            // TODO: Use listeners instead
            Thread.sleep(1000);
        }

        // Navigate back
        goBack();
        goBack();
        goBack();

        // Save state -> close all Tabs -> restore State
        state = getState();
        persistState();
        closeAllTabs();
        restoreState();
        restoredState = getState();

        // Assert restored state is same as saved state
        assertTrue("Saved and restored state should be the same",
                state.isSame(restoredState));
    }
}
