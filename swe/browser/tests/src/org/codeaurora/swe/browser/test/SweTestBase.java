/*
 *  Copyright (c) 2013, The Linux Foundation. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *      * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package org.codeaurora.swe.browser.test;

import android.app.Instrumentation;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.test.ActivityInstrumentationTestCase2;

import org.codeaurora.swe.browser.BrowserActivity;
import org.codeaurora.swe.browser.Intention.Type;
import org.codeaurora.swe.browser.Intention;
import org.codeaurora.swe.browser.Tab;
import org.codeaurora.swe.browser.TabManager;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * A base class for SWE UI tests.
 */
public class SweTestBase
        extends ActivityInstrumentationTestCase2<BrowserActivity> {

    protected BrowserActivity mActivity;
    protected TabManager mTabManager;
    protected Tab mTab;

    private static final String INITIAL_URL = "www.codeaurora.org";
    private static final String DEFAULT_SWE_URL = "http://www.codeaurora.org";
    protected static int WAIT_TIMEOUT_SECONDS = 15;

    public SweTestBase() {
        super(BrowserActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        //Initialize SWE Browser Activity

        // Explicitly set the Activity touch mode to false
        setActivityInitialTouchMode(false);

        // Construct Intent with INITIAL_URL data
        Intent intent = new Intent("tab:load");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setData(Uri.parse(INITIAL_URL));

        //  Inject Intent into SWE Browser activity
        intent.setComponent(new ComponentName(getInstrumentation().getTargetContext(),
              BrowserActivity.class));
        setActivityIntent(intent);
        mActivity = getActivity();
        mTabManager = TabManager.getInstance();
    }

    @Override
    protected void tearDown() throws Exception {
        try {
            closeAllTabs();
            super.tearDown();
            Thread.sleep(1000);
        } catch (Throwable e) {
            throw new Exception(e);
        }
    }

    /**
     * Runs a {@link Callable} on the main thread, blocking until it is
     * complete, and returns the result. Calls
     * {@link Instrumentation#waitForIdleSync()} first to help avoid certain
     * race conditions.
     *
     * @param <R> Type of result to return
     */
    public <R> R runTestOnUiThreadAndGetResult(Callable<R> callable)
            throws Throwable {
        FutureTask<R> task = new FutureTask<R>(callable);
        getInstrumentation().waitForIdleSync();
        getInstrumentation().runOnMainSync(task);
        try {
            return task.get();
        } catch (ExecutionException e) {
            // Unwrap the cause of the exception and re-throw it.
            throw e.getCause();
        }
    }

    /**
     * Starts the SWE activity and loads the given URL.
     * The URL can be null, in which case will default to DEFAULT_SWE_URL.
     */
    protected BrowserActivity launchSweBrowserWithUrl(String url) {
        return launchSweBrowserWithUrlAndCommandLineArgs(url, null);
    }

    /**
     * Starts the SWE activity appending the provided command line arguments
     * and loads the given URL. The URL can be null, in which case it will
     * default to DEFAULT_SWE_URL.
     */
    protected BrowserActivity launchSweBrowserWithUrlAndCommandLineArgs(String url,
            String[] commandLineArgs) {

        // Explicitly set the Activity touch mode to false. If the touch mode is
        // set to true, then key events from test methods are ignored.
        setActivityInitialTouchMode(false);

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setAction("tab:new");
        intent.setData(Uri.parse(url != null ? url : DEFAULT_SWE_URL));
        intent.setComponent(new ComponentName(getInstrumentation().getTargetContext(),
              BrowserActivity.class));
        if (commandLineArgs != null) {
            intent.putExtra(BrowserActivity.COMMAND_LINE_ARGS_KEY, commandLineArgs);
        }
        setActivityIntent(intent);

        return getActivity();
    }

    /**
     * Loads url on the UI thread but does not block.
     * @param inNewTab If set to true it loads the URL in a new Tab,
     *                 otherwise, it loads the URL in the activeTab.
     */
    protected void loadUrlAsync(final String url,
                                final boolean inNewTab) throws Throwable {
        final Intention.Type intentionType =
                inNewTab ? Type.I_OpenAndConsume :  Type.I_Consume;
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                TabManager.getInstance().setTabIntention(new Intention(intentionType, url));
            }
        });
    }

    protected void goBack() throws Throwable {
        final TabManager tabManager = TabManager.getInstance();
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (tabManager.getActiveTab().canGoBack()) {
                    tabManager.getActiveTab().goBack();
                }
            }
        });
    }

    protected void closeAllTabs() throws Throwable {
        final TabManager tabManager = TabManager.getInstance();
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                while ( tabManager.getTabsCount() > 0) {
                    tabManager.closeTab(tabManager.getActiveTabData());
                }
            }
        });
    }

    protected void restoreState() throws Throwable {
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                TabManager.getInstance().restoreState();
            }
        });
    }

    protected void persistState() throws Throwable {
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                TabManager.getInstance().persistState();
            }
        });
    }

    protected void killActivity(final BrowserActivity activity) throws Throwable {
        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                getInstrumentation().callActivityOnDestroy(activity);
            }
        });
    }

}
