# Copyright (c) 2013, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#    * Neither the name of The Linux Foundation nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# The SWE Browser software versioning scheme closely resembles the scheme used
# by the chromium project: http://www.chromium.org/releases/version-numbers.

# The SWE Browser version consists of 5 parts:
# MAJOR.MINOR.BRANCH.BUILD_ID.COMMIT_ID

# The MAJOR and MINOR fields have the same semantics as those used by the
# chromium versioning scheme, see description at the above URL.

# The BRANCH and BUILD_ID fields have similar semantics to the chromium BUILD
# and PATCH fields. In particular, BRANCH is an integer that identifies a
# specific branch derived from master. When set to zero (0) it indicates that
# we are building from master. Other BRANCH values are chosen arbitrarily.
# BUILD_ID is an ever-increasing number representing a point in time of a given
# BRANCH. It is programmatically generated as the number of commits from the
# BASE_COMMIT_ID specified in this file. The BRANCH and BASE_COMMIT_ID fields
# are manually updated (by editing this file) when a new branch is created.

# COMMIT_ID is an additional field (not used by the chromium versioning scheme)
# that captures the latest commit id at the time of the build.

MAJOR=0
MINOR=1
BRANCH=0
BASE_COMMIT_ID=51ebe4d9e211cc2e013e1311a7ddbaeda005f157
