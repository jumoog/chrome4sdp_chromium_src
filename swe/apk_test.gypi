{
  'conditions': [
    ['v8_build == "shared"', {
      'variables' : {
        'native_libs_path': '<(SHARED_LIB_DIR)/libswev8.so',
      },
        'includes': [ '../build/apk_swe_test.gypi' ],
      }, {
        'includes': [ '../build/apk_test.gypi' ],
      },
    ],
  ],
}
