
 #  Copyright (c) 2013, The Linux Foundation. All rights reserved.
 #
 #  Redistribution and use in source and binary forms, with or without
 #  modification, are permitted provided that the following conditions are
 #  met:
 #      * Redistributions of source code must retain the above copyright
 #       notice, this list of conditions and the following disclaimer.
 #     * Redistributions in binary form must reproduce the above
 #       copyright notice, this list of conditions and the following
 #       disclaimer in the documentation and/or other materials provided
 #       with the distribution.
 #     * Neither the name of The Linux Foundation nor the names of its
 #       contributors may be used to endorse or promote products derived
 #       from this software without specific prior written permission.
 #
 #  THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 #  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 #  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 #  ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 #  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 #  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 #  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 #  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 #  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 #  OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 #  IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#!/bin/bash

if test $# -eq 1; then
  build=$1
  buildtype=""
  if [ "$build" == "Debug" ]; then
    buildtype="--debug"
  elif [ "$build" == "Release" ]; then
    buildtype="--release"
  else
    echo "Usage ./runSmokeTest.sh <Debug|Release> or just ./runSmokeTest.sh"
    exit 1
  fi
else
  build="Release"
  buildtype="--release"
fi


${CHROME_SRC:?"CHROME_SRC is not defined. Make sure you have run build/android/envsetup.sh"}
#go to the root directory
cd $CHROME_SRC

# building the test apk
ninja -C ./out/$build -v swe_browser_test_apk -j20
ninja -C ./out/$build -v device_forwarder -j20
ninja -C ./out/$build -v md5sum_bin -j20
ninja -C ./out/$build -v md5sum_bin_host -j20
ninja -C ./out/$build -v host_forwarder -j20

# installing the test apk and running smoke test for 2 iterations
adb install -r ./out/$build/apks/SWE_BrowserTest.apk
./build/android/run_instrumentation_tests.py -I --test-apk SWE_BrowserTest $buildtype  -f SmokeTest -n 2
