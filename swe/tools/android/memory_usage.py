#!/usr/bin/python
#
# Copyright (c) 2013, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#    * Neither the name of The Linux Foundation nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Update the SWE Browser, WebKit, and V8 versions in
# content/shell/android/res/values/swe_versions.xml file based on the
# latest configuration information at build time.
#


import time
from subprocess import call
import subprocess as sub
import sys
import getopt

default_loop_times = 1

# Usage: memory_usage.py count (integer value less than 10).

if(len(sys.argv)>1):
  if(sys.argv[1].isdigit()):
    if(int(sys.argv[1]) < 10):
      default_loop_times = int(sys.argv[1])

print default_loop_times

swe_browser = ["adb", "shell", "am", "start", "-n", "org.codeaurora.swe.browser/.BrowserActivity",  "-a", "tab:open", "-d"]

total_memory_usuage_command = 'adb shell procrank  | grep swe | awk \'{print $4+$5}\' |  awk \'{s+=$1} END {print s}\''

remove_tab_state = 'adb shell rm -r /data/data//org.codeaurora.swe.browser/files'

kill_swe_browser = 'adb shell kill `adb shell ps | grep "org.codeaurora.swe.browser" | grep -v "sandboxed" | awk \'{print $2}\'`'

kill_swe_r = 'adb shell kill `adb shell ps | grep "org.codeaurora.swe.browser:" | awk \'{print $2}\'`'

print_swe_process = 'adb shell procrank  | grep swe'

websites = list([['http://amazon.com'],
                ['http://cnn.com'],
                ['http://huffingtonpost.com'],
                ['http://pinterest.com'],
                ['http://engadget.com'],
                ['http://techcrunch.com'],
                ['http://facebook.com'],
                ['http://twitter.com'],
                ['http://yahoo.com'],
                ['http://ebay.com']])

def removetabstate():
  p = sub.Popen(remove_tab_state, shell=True, stdout=sub.PIPE, stderr=sub.PIPE)

def adbroot():
  p = sub.Popen("adb root", shell=True, stdout=sub.PIPE, stderr=sub.PIPE)


def killbrowser():
  adbroot()
  p = sub.Popen(kill_swe_browser, shell=True, stdout=sub.PIPE, stderr=sub.PIPE)
  output, errors = p.communicate()
  print errors

def killR():
  adbroot()
  p = sub.Popen(kill_swe_browser, shell=True, stdout=sub.PIPE, stderr=sub.PIPE)
  output, errors = p.communicate()
  print errors

def print_swe():
  p = sub.Popen(print_swe_process, shell=True, stdout=sub.PIPE, stderr=sub.PIPE)
  output, errors = p.communicate()
  print output


killbrowser()
removetabstate()

grandtotal = 0;

for x in xrange(0, default_loop_times):
  print "loop# ", x
  for item in websites:
    print item
    p = sub.Popen(swe_browser+item, stdout=sub.PIPE, stderr=sub.PIPE)
    time.sleep(5)
    p = sub.Popen(total_memory_usuage_command,shell=True, stdout=sub.PIPE, stderr=sub.PIPE)
    output, errors = p.communicate()
    print "total: "+output
    grandtotal = int(output)
    print errors
   # killR()
    print_swe()

print "Total Memory Usage(PSS+USS in MB): "+str(grandtotal)

