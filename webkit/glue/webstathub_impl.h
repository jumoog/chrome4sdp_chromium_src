/** ---------------------------------------------------------------------------
 Copyright (c) 2013 The Linux Foundation. All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above
       copyright notice, this list of conditions and the following
       disclaimer in the documentation and/or other materials provided
       with the distribution.
     * Neither the name of The Linux Foundation nor the names of its
       contributors may be used to endorse or promote products derived
       from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 -----------------------------------------------------------------------------**/
#ifndef WEBKIT_GLUE_WEBSTATHUB_IMPL_H_
#define WEBKIT_GLUE_WEBSTATHUB_IMPL_H_

#include "third_party/WebKit/Source/Platform/chromium/public/WebStatHub.h"
#include "webkit/glue/webkit_glue_export.h"

namespace WebKit {
class WebData;
class WebString;
class WebURL;
class WebURLResponse;
}

template <typename T> struct DefaultSingletonTraits;

namespace webkit_glue {

class WebStatHubImpl : public WebKit::WebStatHub {
 public:

  virtual ~WebStatHubImpl();

  static WebStatHubImpl* GetInstance();

  virtual WebKit::WebStatHubCmd cmdCreate(WebKit::WebStatHubCmdId cmdId);
  virtual void cmdAddParamAsString(WebKit::WebStatHubCmd cmd, const char* param);
  virtual void cmdAddParamAsBool(WebKit::WebStatHubCmd cmd, bool param);
  virtual void cmdAddParamAsUint32(WebKit::WebStatHubCmd cmd, unsigned int param);
  virtual void cmdAddParamAsPtr(WebKit::WebStatHubCmd cmd, void* param);
  virtual void cmdAddParamAsBuf(WebKit::WebStatHubCmd cmd, const void* param, unsigned int size);
  virtual bool cmdCommit(WebKit::WebStatHubCmd cmd);
  virtual bool cmdCommitDelayed(WebKit::WebStatHubCmd cmd, unsigned int delay);

  virtual unsigned long hash(const WebKit::WebURL& url);
  virtual unsigned int isPreloaded(const WebKit::WebURL& url);
  virtual bool getPreloaded(const WebKit::WebURL& url, unsigned int get_from,
      WebKit::WebURLResponse& response, WebKit::WebData& data);
  virtual bool releasePreloaded(const WebKit::WebURL& url);
  virtual bool isPreloaderEnabled();
  virtual bool isPerfEnabled();
  virtual bool isInspectorSpyEnabled();

 private:
  WebStatHubImpl();
  friend struct DefaultSingletonTraits<WebStatHubImpl>;

  DISALLOW_COPY_AND_ASSIGN(WebStatHubImpl);
};

} // namespace webkit_glue

#endif //WEBKIT_GLUE_WEBSTATHUB_IMPL_H_
