/** ---------------------------------------------------------------------------
 Copyright (c) 2013 The Linux Foundation. All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above
       copyright notice, this list of conditions and the following
       disclaimer in the documentation and/or other materials provided
       with the distribution.
     * Neither the name of The Linux Foundation nor the names of its
       contributors may be used to endorse or promote products derived
       from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 -----------------------------------------------------------------------------**/

#include "base/time.h"
#include "base/memory/singleton.h"
#include "webkit/glue/webstathub_impl.h"
#include "third_party/WebKit/Source/WebKit/chromium/public/platform/WebData.h"
#include "third_party/WebKit/Source/WebKit/chromium/public/platform/WebString.h"
#include "third_party/WebKit/Source/WebKit/chromium/public/platform/WebURL.h"
#include "third_party/WebKit/Source/WebKit/chromium/public/platform/WebURLResponse.h"

//Enable IPC API
#define STAT_HUB_API_BY_IPC

#include "net/stat_hub/stat_hub_api.h"
#include "net/stat_hub/stat_hub_cmd_api.h"
#include "net/http/http_response_headers.h"

//Disable IPC API
#undef STAT_HUB_API_BY_IPC

namespace webkit_glue {

WebStatHubImpl::WebStatHubImpl() {
}

WebStatHubImpl::~WebStatHubImpl() {
}

WebStatHubImpl* WebStatHubImpl::GetInstance() {
  return Singleton<WebStatHubImpl>::get();
}

WebKit::WebStatHubCmd WebStatHubImpl::cmdCreate(WebKit::WebStatHubCmdId cmdId) {
    StatHubCmdType cmd;
    StatHubActionType action;

    switch(cmdId) {
        case WebKit::WEBSH_CMD_MM_CACHE_CLEAR:
            cmd = SH_CMD_WK_MEMORY_CACHE;
            action = SH_ACTION_CLEAR;
            break;
        case WebKit::WEBSH_CMD_MM_CACHE_SET_STATUS:
            cmd = SH_CMD_WK_MEMORY_CACHE;
            action = SH_ACTION_STATUS;
            break;
        case WebKit::WEBSH_CMD_MAIN_URL_WILL_START:
            cmd = SH_CMD_WK_MAIN_URL;
            action = SH_ACTION_WILL_START;
            break;
        case WebKit::WEBSH_CMD_MAIN_URL_DID_FINISH:
            cmd = SH_CMD_WK_MAIN_URL;
            action = SH_ACTION_DID_FINISH;
            break;
        case WebKit::WEBSH_CMD_RESOURCE_WILL_START_LOAD:
            cmd = SH_CMD_WK_RESOURCE;
            action = SH_ACTION_WILL_START_LOAD;
            break;
        case WebKit::WEBSH_CMD_RESOURCE_DID_FINISH_LOAD:
            cmd = SH_CMD_WK_RESOURCE;
            action = SH_ACTION_DID_FINISH_LOAD;
            break;
        case WebKit::WEBSH_CMD_RESOURCE_JS_SEQ:
            cmd = SH_CMD_WK_RESOURCE;
            action = SH_ACTION_JS_SEQ;
            break;
        case WebKit::WEBSH_CMD_RESOURCE_WILL_SEND_REQUEST:
            cmd = SH_CMD_WK_RESOURCE;
            action = SH_ACTION_WILL_SEND_REQUEST;
            break;
        case WebKit::WEBSH_CMD_PAGE_WILL_START_LOAD:
            cmd = SH_CMD_WK_PAGE;
            action = SH_ACTION_WILL_START_LOAD;
            break;
        case WebKit::WEBSH_CMD_PAGE_DID_START_LOAD:
            cmd = SH_CMD_WK_PAGE;
            action = SH_ACTION_DID_START_LOAD;
            break;
        case WebKit::WEBSH_CMD_PAGE_DID_FINISH_LOAD:
            cmd = SH_CMD_WK_PAGE;
            action = SH_ACTION_DID_FINISH_LOAD;
            break;
        case WebKit::WEBSH_CMD_PAGE_ON_LOAD:
            cmd = SH_CMD_WK_PAGE;
            action = SH_ACTION_ON_LOAD;
            break;
        case WebKit::WEBSH_CMD_PAGE_FIRST_PIXEL:
            cmd = SH_CMD_WK_PAGE;
            action = SH_ACTION_FIRST_PIXEL;
            break;
        case WebKit::WEBSH_CMD_PAGE_PROGRESS_UPDATE:
            cmd = SH_CMD_WK_PAGE;
            action = SH_ACTION_PROGRESS_UPDATE;
            break;
        case WebKit::WEBSH_CMD_INSPECTOR_SPY_TL_MSG:
            cmd = SH_CMD_WK_INSPECTOR_RECORD;
            action = SH_ACTION_NONE;
            break;
        default:
            return NULL;
    }
    return (WebKit::WebStatHubCmd)STAT_HUB_API(CmdCreate)(cmd, action, 0);
}

void WebStatHubImpl::cmdAddParamAsString(WebKit::WebStatHubCmd cmd, const char* param) {
    if(cmd) {
        StatHubCmd* stat_hub_cmd = (StatHubCmd*)cmd;
        stat_hub_cmd->AddParamAsString(param);
    }
}

void WebStatHubImpl::cmdAddParamAsBool(WebKit::WebStatHubCmd cmd, bool param) {
    if(cmd) {
        StatHubCmd* stat_hub_cmd = (StatHubCmd*)cmd;
        stat_hub_cmd->AddParamAsBool(param);
    }
}

void WebStatHubImpl::cmdAddParamAsUint32(WebKit::WebStatHubCmd cmd, unsigned int param) {
    if(cmd) {
        StatHubCmd* stat_hub_cmd = (StatHubCmd*)cmd;
        stat_hub_cmd->AddParamAsUint32(param);
    }
}

void WebStatHubImpl::cmdAddParamAsPtr(WebKit::WebStatHubCmd cmd, void* param) {
    if(cmd) {
        StatHubCmd* stat_hub_cmd = (StatHubCmd*)cmd;
        stat_hub_cmd->AddParamAsPtr(param);
    }
}

void WebStatHubImpl::cmdAddParamAsBuf(WebKit::WebStatHubCmd cmd, const void* param, unsigned int size) {
    if(cmd) {
        StatHubCmd* stat_hub_cmd = (StatHubCmd*)cmd;
        stat_hub_cmd->AddParamAsBuf(param, size);
    }
}

bool WebStatHubImpl::cmdCommit(WebKit::WebStatHubCmd cmd) {
    return STAT_HUB_API(CmdCommit)((StatHubCmd*)cmd);
}

bool WebStatHubImpl::cmdCommitDelayed (WebKit::WebStatHubCmd cmd, unsigned int delay) {
    return STAT_HUB_API(CmdCommitDelayed)((StatHubCmd*)cmd, delay);
}

unsigned long WebStatHubImpl::hash(const WebKit::WebURL& url){
  return STAT_HUB_API(Hash)(url.spec().data());
}

unsigned int WebStatHubImpl::isPreloaded(const WebKit::WebURL& url) {
  return STAT_HUB_API(IsPreloaded)(url.spec().data());
}

bool WebStatHubImpl::getPreloaded(const WebKit::WebURL& url, unsigned int get_from,
    WebKit::WebURLResponse& response, WebKit::WebData& data) {
  std::string data_tmp;
  unsigned int size;
  std::string headers;

  if (STAT_HUB_API(GetPreloaded)(url.spec().data(), get_from, headers, data_tmp, size)) {
      scoped_refptr<net::HttpResponseHeaders> responseHeaders;
      responseHeaders = new net::HttpResponseHeaders(headers);

      std::string mime;
      std::string encoding;
      responseHeaders->GetMimeType(&mime);
      responseHeaders->GetCharset(&encoding);

      response.initialize();
      response.setURL(url);
      response.setMIMEType(WebKit::WebString::fromUTF8(mime));
      response.setTextEncodingName(WebKit::WebString::fromUTF8(encoding));
      response.setExpectedContentLength(size);

      WebKit::WebURLResponse::HTTPVersion version = WebKit::WebURLResponse::Unknown;
      if (responseHeaders->GetHttpVersion() == net::HttpVersion(0, 9))
        version = WebKit::WebURLResponse::HTTP_0_9;
      else if (responseHeaders->GetHttpVersion() == net::HttpVersion(1, 0))
        version = WebKit::WebURLResponse::HTTP_1_0;
      else if (responseHeaders->GetHttpVersion() == net::HttpVersion(1, 1))
        version = WebKit::WebURLResponse::HTTP_1_1;
      int httpStatusCode = responseHeaders->response_code();
      std::string httpStatusText = responseHeaders->GetStatusText();
      response.setHTTPVersion(version);
      response.setHTTPStatusCode(httpStatusCode);
      response.setHTTPStatusText(WebKit::WebString::fromUTF8(httpStatusText));

      base::Time time_val;
      if (responseHeaders->GetLastModifiedValue(&time_val)) {
        response.setLastModifiedDate(time_val.ToDoubleT());
      }

      // Build up the header map.
      void* iter = NULL;
      std::string value;
      std::string name;
      while (responseHeaders->EnumerateHeaderLines(&iter, &name, &value)) {
          response.addHTTPHeaderField(WebKit::WebString::fromUTF8(name),
              WebKit::WebString::fromUTF8(value));
      }

      data.assign(data_tmp.data(), size);
      return true;
  }
  return false;
}

bool WebStatHubImpl::releasePreloaded(const WebKit::WebURL& url) {
  return STAT_HUB_API(ReleasePreloaded)(url.spec().data());
}

bool WebStatHubImpl::isPreloaderEnabled() {
  return STAT_HUB_API(IsPreloaderEnabled)();
}

bool WebStatHubImpl::isPerfEnabled() {
  return STAT_HUB_API(IsPerfEnabled)();
}

bool WebStatHubImpl::isInspectorSpyEnabled() {
  unsigned int mask = STAT_HUB_API(GetCmdMask)();
  return (mask & (1<<SH_CMD_WK_INSPECTOR_RECORD));
}

}
