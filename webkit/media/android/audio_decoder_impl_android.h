// Copyright (c) 2013, The Linux Foundation. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//     * Neither the name of The Linux Foundation nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
// ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
// BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef WEBKIT_MEDIA_AUDIO_DECODER_IMPL_ANDROID_H_
#define WEBKIT_MEDIA_AUDIO_DECODER_IMPL_ANDROID_H_

#include "base/basictypes.h"
#include "base/message_loop.h"
#include "base/shared_memory.h"
#include "media/base/media_export.h"
#include "media/audio/android/audio_decoder_ipc.h"
#include "media/audio/scoped_loop_observer.h"
#include "base/synchronization/condition_variable.h"
#include "base/synchronization/lock.h"


namespace WebKit { class WebAudioBus; }
namespace media  { class AudioDecoderIPC; }


namespace webkit_media {

// Decode in-memory audio file data.
bool DecodeAudioFileData(WebKit::WebAudioBus** destination_bus, const char* data, size_t data_size, double sample_rate);

class AudioDecoderAndroid
         : public media::AudioDecoderIPCDelegate,
    NON_EXPORTED_BASE(public media::ScopedLoopObserver),
    public base::RefCountedThreadSafe<AudioDecoderAndroid>{
public:

    AudioDecoderAndroid(media::AudioDecoderIPC * ipc,
                        const scoped_refptr<base::MessageLoopProxy>& io_loop);

    virtual ~AudioDecoderAndroid();

    virtual void WillDestroyCurrentMessageLoop() OVERRIDE;

    virtual void OnDataDecoded(base::SharedMemoryHandle handle,
                               unsigned long length,
                               unsigned int channel_nb,
                               unsigned int sample_rate,
                               unsigned int byte_per_sample) OVERRIDE;

    virtual void OnIPCClosed() OVERRIDE;

     void Initialize();

     void DecodeAudioFileData( const char* data, uint32 size, double sample_rate, base::SharedMemoryHandle data_mem);

     void WaitForDecodeCompletion();

     void NotifyDecodeCompletion();

     double DecodedSampleRate() const { return decoded_data_sample_rate_; }

     unsigned int DecodedNbOfChannels() const { return decoded_data_nb_channels_; }

     unsigned int DecodedLength() const { return decoded_data_length_; }

     unsigned long DecodedFrames() const { return nb_decoded_frames_; }

     unsigned int DecodedBytesPerSample() const { return decoded_data_bytes_per_sample_; }

     int16* DecodedAudioData() const { return decoded_audio_data_; }

     base::SharedMemoryHandle DecodedSharedMemHandle() const { return mem_handle_; }

private:
     void InitializeOnIOThread();

     void DecodeBufferIOThread(const char* data, uint32 size, base::SharedMemoryHandle data_mem);

     void NotifyDecodeCompletionOnIOThread();

     media::AudioDecoderIPC* ipc_;

     int id_;
     bool is_initialized_;

     base::Lock lock_;
     base::ConditionVariable cv_;

     double decoded_data_sample_rate_;
     unsigned int decoded_data_nb_channels_;
     unsigned int decoded_data_length_;
     unsigned long nb_decoded_frames_;
     unsigned int decoded_data_bytes_per_sample_;
     int16* decoded_audio_data_;
     base::SharedMemoryHandle mem_handle_;
};

}  // namespace webkit_media

#endif  // WEBKIT_MEDIA_AUDIO_DECODER_IMPL_ANDROID_H_
