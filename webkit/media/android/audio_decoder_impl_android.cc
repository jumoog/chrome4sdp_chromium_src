// Copyright (c) 2013, The Linux Foundation. All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//     * Neither the name of The Linux Foundation nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
// ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
// BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "webkit/media/android/audio_decoder_impl_android.h"

#include "base/logging.h"
#include "base/android/jni_android.h"
#include "base/android/jni_string.h"
#include "base/basictypes.h"
#include "base/process_util.h"

#include "base/file_path.h"
#include "base/file_util.h"
#include "base/lazy_instance.h"

#include "media/audio/audio_util.h"
#include "media/audio/shared_memory_util.h"


#include "content/renderer/media/audio_message_filter.h"

#include "content/common/child_process.h"

#include "content/renderer/android/audio_decoder_message_filter.h"

#include "third_party/WebKit/Source/WebKit/chromium/public/platform/WebAudioBus.h"

#include "third_party/ashmem/ashmem.h"

#include <sys/mman.h>
#include "base/shared_memory.h"


using content::AudioDecoderMessageFilter;
using content::ChildProcess;

using media::AudioDecoderIPC;
using media::AudioDecoderIPCDelegate;

static bool decode_status = false;

base::LazyInstance<base::Lock,
                   base::internal::LeakyLazyInstanceTraits <base::Lock> > data_mutex;

namespace webkit_media {

AudioDecoderAndroid::AudioDecoderAndroid(
    AudioDecoderIPC* ipc,
    const scoped_refptr<base::MessageLoopProxy>& io_loop)
    : ScopedLoopObserver(io_loop),
      ipc_(ipc),
      id_(0),
      is_initialized_(false),
      cv_(&lock_){
    CHECK(ipc_);
}

AudioDecoderAndroid::~AudioDecoderAndroid() {}

void AudioDecoderAndroid::WillDestroyCurrentMessageLoop() {
    LOG(ERROR) << "IO loop going away before the audio device has been stopped";
}

void AudioDecoderAndroid::OnDataDecoded(base::SharedMemoryHandle handle,
                                        unsigned long length,
                                        unsigned int channel_nb,
                                        unsigned int sample_rate,
                                        unsigned int byte_per_sample) {
    base::AutoLock auto_lock(lock_);
    mem_handle_ = handle;
    decoded_data_nb_channels_ = channel_nb;
    decoded_data_sample_rate_ = sample_rate;
    nb_decoded_frames_ = length/(channel_nb*byte_per_sample);
    decoded_data_bytes_per_sample_ = byte_per_sample;
    decoded_data_length_ = length;
    decode_status = true;
    cv_.Signal();
}

void AudioDecoderAndroid::OnIPCClosed() {}

void AudioDecoderAndroid::Initialize() {
    message_loop()->PostTask(FROM_HERE,
                             base::Bind(&AudioDecoderAndroid::InitializeOnIOThread, this));
}

void AudioDecoderAndroid::DecodeAudioFileData( const char* data, uint32 size, double sample_rate, base::SharedMemoryHandle data_mem) {
    message_loop()->PostTask(FROM_HERE,
                             base::Bind(&AudioDecoderAndroid::DecodeBufferIOThread, this,data,size,data_mem));
}

void AudioDecoderAndroid::WaitForDecodeCompletion(){
    base::AutoLock auto_lock(lock_);
    while (decode_status != true) {
        cv_.Wait();
    }
}

void AudioDecoderAndroid::NotifyDecodeCompletion() {
    message_loop()->PostTask(FROM_HERE,
        base::Bind(&AudioDecoderAndroid::NotifyDecodeCompletionOnIOThread, this));
}


void AudioDecoderAndroid::InitializeOnIOThread() {
    DCHECK(message_loop()->BelongsToCurrentThread());
    id_ = ipc_->AddDelegate(this);
    ipc_->InitializeDecoder(id_);
    is_initialized_ = true;
}

void AudioDecoderAndroid::DecodeBufferIOThread(const char* data,uint32 size, base::SharedMemoryHandle data_mem) {
    DCHECK(message_loop()->BelongsToCurrentThread());
    if (id_ && is_initialized_)
        ipc_->DecodeBuffer(data,size,data_mem);
}

void AudioDecoderAndroid::NotifyDecodeCompletionOnIOThread() {
    DCHECK(message_loop()->BelongsToCurrentThread());
    if (id_ && is_initialized_)
        ipc_->NotifyDecodeCompletion();
    is_initialized_ = false;
    id_ = 0;
}

static bool deinterleaveAudioChannel(void* source,
                                     float* destination,
                                     int channels,
                                     int channel_index,
                                     size_t number_of_frames) {
    int16_t* source16 = static_cast<int16_t*>(source) + channel_index;
    const float kScale = 1.0f / 32768.0f;

    for (unsigned i = 0; i < number_of_frames; ++i) {
        destination[i] = kScale * *source16;
        source16 += channels;
    }
    return true;
}

bool DecodeAudioFileData(WebKit::WebAudioBus** destination_bus, const char* data, size_t data_size, double sample_rate) {
    base::SharedMemoryHandle memory_handle;
    base::SharedMemory shared_mem;

    if ((!destination_bus)||(!data)) {
        return false;
    }

    base::AutoLock auto_lock(data_mutex.Get());

    decode_status = false;

    scoped_refptr<AudioDecoderAndroid> audio_decoder = new AudioDecoderAndroid(
    AudioDecoderMessageFilter::Get(),
    ChildProcess::current()->io_message_loop()->message_loop_proxy());

    if (audio_decoder) {
        audio_decoder->Initialize();

        if (data_size) {
            shared_mem.CreateAndMapAnonymous(data_size);
            shared_mem.Map(data_size);
            int16 *decodedBuffer = reinterpret_cast<int16*>(shared_mem.memory());
            if (data_size<= shared_mem.created_size()) {
                memcpy(decodedBuffer, data, data_size);
            } else {
                delete audio_decoder;
                return false;
            }
            shared_mem.ShareToProcess(base::GetCurrentProcessHandle(),&memory_handle);
        }

        audio_decoder->DecodeAudioFileData(data,(uint32)data_size,sample_rate,memory_handle);
        audio_decoder->WaitForDecodeCompletion();

        if (decode_status) {
            shared_mem.Close();

            std::vector<float*> audio_data;
            audio_data.reserve(audio_decoder->DecodedNbOfChannels());

            base::SharedMemory shared_memory(audio_decoder->DecodedSharedMemHandle(),false);
            shared_memory.Map(media::TotalSharedMemorySizeInBytes(audio_decoder->DecodedLength()));
            int16 *decodedBuffer = reinterpret_cast<int16*>(shared_memory.memory());

            for (unsigned int i = 0; i< audio_decoder->DecodedNbOfChannels(); i++) {
                float* dst_buffer = new float[audio_decoder->DecodedFrames()];
                deinterleaveAudioChannel((void*)decodedBuffer,
                                         dst_buffer,
                                         audio_decoder->DecodedNbOfChannels(),
                                         i,
                                         audio_decoder->DecodedFrames());
                audio_data.push_back(dst_buffer);
            }

            WebKit::WebAudioBus* audio_bus = new WebKit::WebAudioBus();
            audio_bus->initialize (audio_decoder->DecodedNbOfChannels(),
                                  audio_decoder->DecodedFrames(),
                                  audio_decoder->DecodedSampleRate());

            if (audio_bus->length() == audio_decoder->DecodedFrames()){
                for (unsigned int i = 0; i< audio_decoder->DecodedNbOfChannels(); i++) {
                    float* dst = audio_bus->channelData(i);
                    memcpy(dst,audio_data[i],audio_decoder->DecodedFrames()*(sizeof(float)));
                }
            } else {
                shared_memory.Close();
                return false;
            }

            *destination_bus = audio_bus;
            shared_memory.Close();
            for (size_t i = 0; i < audio_data.size(); ++i)
                delete audio_data[i];
            audio_decoder->NotifyDecodeCompletion();
        } else {
            return false;
        }
    } else {
        LOG(ERROR) << "DecodeAudioFileData: AudioDecoderAndroid creation fails ";
    }

    return true;
}

}  // namespace webkit_media
