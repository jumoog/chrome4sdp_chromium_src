// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Copyright (c) 2013, The Linux Foundation. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "webkit/media/android/webmediaplayer_android.h"

#include "base/file_path.h"
#include "base/logging.h"
#include "media/base/android/media_player_bridge.h"
#include "media/filters/skcanvas_video_renderer.h"
#include "net/base/mime_util.h"
#include "third_party/WebKit/Source/WebKit/chromium/public/WebMediaPlayerClient.h"
#include "webkit/media/android/stream_texture_factory_android.h"
#include "webkit/media/android/webmediaplayer_manager_android.h"
#include "webkit/media/webmediaplayer_util.h"
#include "webkit/media/webvideoframe_impl.h"

static const uint32 kGLTextureExternalOES = 0x8D65;

using WebKit::WebMediaPlayer;
using WebKit::WebSize;
using WebKit::WebTimeRanges;
using WebKit::WebURL;
using WebKit::WebVideoFrame;
using media::MediaPlayerBridge;
using media::VideoFrame;

namespace webkit_media {

WebMediaPlayerAndroid::WebMediaPlayerAndroid(
    WebKit::WebMediaPlayerClient* client,
    WebMediaPlayerManagerAndroid* manager,
    StreamTextureFactory* factory)
    : client_(client),
      buffered_(1u),
      main_loop_(MessageLoop::current()),
      pending_seek_(0),
      seeking_(false),
      did_loading_progress_(false),
      manager_(manager),
      network_state_(WebMediaPlayer::NetworkStateEmpty),
      ready_state_(WebMediaPlayer::ReadyStateHaveNothing),
      is_playing_(false),
      needs_establish_peer_(true),
      has_size_info_(false),
      visible_(false),
      decoded_frame_count_(0),
      stream_texture_factory_(factory),
      volume_(1.0),
      pending_playback_(false) {
  main_loop_->AddDestructionObserver(this);
  if (manager_)
    player_id_ = manager_->RegisterMediaPlayer(this);

  if (stream_texture_factory_.get()) {
    stream_texture_proxy_.reset(stream_texture_factory_->CreateProxy());
    stream_id_ = stream_texture_factory_->CreateStreamTexture(&texture_id_);
    ReallocateVideoFrame();
  }
}

WebMediaPlayerAndroid::~WebMediaPlayerAndroid() {
  if (stream_id_)
    stream_texture_factory_->DestroyStreamTexture(texture_id_);

  if (manager_)
    manager_->UnregisterMediaPlayer(player_id_);

  if (main_loop_)
    main_loop_->RemoveDestructionObserver(this);
}

void WebMediaPlayerAndroid::load(const WebURL& url, CORSMode cors_mode) {
  if (cors_mode != CORSModeUnspecified)
    NOTIMPLEMENTED() << "No CORS support";

  url_ = url;

  InitializeMediaPlayer(url_);
}

void WebMediaPlayerAndroid::cancelLoad() {
  NOTIMPLEMENTED();
}

void WebMediaPlayerAndroid::play() {
  if (hasVideo() && needs_establish_peer_)
    EstablishSurfaceTexturePeer();

  SetVolumeInternal(volume_);
  PlayInternal();
  is_playing_ = true;
}

void WebMediaPlayerAndroid::pause() {
  PauseInternal();
  is_playing_ = false;
}

void WebMediaPlayerAndroid::seek(float seconds) {
  pending_seek_ = seconds;
  seeking_ = true;

  SeekInternal(ConvertSecondsToTimestamp(seconds));
}

bool WebMediaPlayerAndroid::supportsFullscreen() const {
  return true;
}

bool WebMediaPlayerAndroid::supportsSave() const {
  return false;
}

void WebMediaPlayerAndroid::setEndTime(float seconds) {
  // Deprecated.
  // TODO(qinmin): Remove this from WebKit::WebMediaPlayer as it is never used.
}

void WebMediaPlayerAndroid::setRate(float rate) {
  NOTIMPLEMENTED();
}

void WebMediaPlayerAndroid::setVolume(float volume) {
  if (volume_ != volume) {
    SetVolumeInternal(volume);
    volume_ = volume;
  }
}

void WebMediaPlayerAndroid::setVisible(bool visible) {
  visible_ = visible;
}

bool WebMediaPlayerAndroid::totalBytesKnown() {
  NOTIMPLEMENTED();
  return false;
}

bool WebMediaPlayerAndroid::hasVideo() const {
  // If we have obtained video size information before, use it.
  if (has_size_info_)
    return !natural_size_.isEmpty();

  // TODO(qinmin): need a better method to determine whether the current media
  // content contains video. Android does not provide any function to do
  // this.
  // We don't know whether the current media content has video unless
  // the player is prepared. If the player is not prepared, we fall back
  // to the mime-type. There may be no mime-type on a redirect URL.
  // In that case, we conservatively assume it contains video so that
  // enterfullscreen call will not fail.
  if (!url_.has_path())
    return false;
  std::string mime;
  if(!net::GetMimeTypeFromFile(FilePath(url_.path()), &mime))
    return true;
  return mime.find("audio/") == std::string::npos;
}

bool WebMediaPlayerAndroid::hasAudio() const {
  // TODO(hclam): Query status of audio and return the actual value.
  return true;
}

bool WebMediaPlayerAndroid::paused() const {
  return !is_playing_;
}

bool WebMediaPlayerAndroid::seeking() const {
  return seeking_;
}

float WebMediaPlayerAndroid::duration() const {
  return static_cast<float>(duration_.InSecondsF());
}

float WebMediaPlayerAndroid::currentTime() const {
  // If the player is pending for a seek, return the seek time.
  if (seeking())
    return pending_seek_;

  return GetCurrentTimeInternal();
}

int WebMediaPlayerAndroid::dataRate() const {
  // Deprecated.
  // TODO(qinmin): Remove this from WebKit::WebMediaPlayer as it is never used.
  return 0;
}

WebSize WebMediaPlayerAndroid::naturalSize() const {
  return natural_size_;
}

WebMediaPlayer::NetworkState WebMediaPlayerAndroid::networkState() const {
  return network_state_;
}

WebMediaPlayer::ReadyState WebMediaPlayerAndroid::readyState() const {
  return ready_state_;
}

const WebTimeRanges& WebMediaPlayerAndroid::buffered() {
  return buffered_;
}

float WebMediaPlayerAndroid::maxTimeSeekable() const {
  // TODO(hclam): If this stream is not seekable this should return 0.
  return duration();
}

bool WebMediaPlayerAndroid::didLoadingProgress() const {
  bool ret = did_loading_progress_;
  did_loading_progress_ = false;
  return ret;
}

unsigned long long WebMediaPlayerAndroid::totalBytes() const {
  // Deprecated.
  // TODO(qinmin): Remove this from WebKit::WebMediaPlayer as it is never used.
  return 0;
}

void WebMediaPlayerAndroid::setSize(const WebKit::WebSize& size) {
}

void WebMediaPlayerAndroid::paint(WebKit::WebCanvas* canvas,
                                  const WebKit::WebRect& rect,
                                  uint8_t alpha) {
  if (video_frame_.get()) {
    media::SkCanvasVideoRenderer renderer;
    media::VideoFrame* frame = WebVideoFrameImpl::toVideoFrame(video_frame_.get());
    if (frame)
      renderer.Paint(frame, canvas, (gfx::Rect)rect, alpha);
  }
}

bool WebMediaPlayerAndroid::hasSingleSecurityOrigin() const {
  if (!url_.has_path())
    return false;

  std::string mime;
  if (!net::GetMimeTypeFromFile(FilePath(url_.path()), &mime))
    return false;

  // Only playlist types may have multiple security origins (e.g.
  // HTTP Live Streaming). TODO: Ideally we'd want to check if all
  // referenced resources within a media file have the same origin
  // so that we don't have to blacklist all playlist files.
  return (!net::IsPlaylistMimeType(mime));
}

bool WebMediaPlayerAndroid::didPassCORSAccessCheck() const {
  return false;
}

WebMediaPlayer::MovieLoadType WebMediaPlayerAndroid::movieLoadType() const {
  // Deprecated.
  // TODO(qinmin): Remove this from WebKit::WebMediaPlayer as it is never used.
  return WebMediaPlayer::MovieLoadTypeUnknown;
}

float WebMediaPlayerAndroid::mediaTimeForTimeValue(float timeValue) const {
  return ConvertSecondsToTimestamp(timeValue).InSecondsF();
}

unsigned WebMediaPlayerAndroid::decodedFrameCount() const {
  return decoded_frame_count_;
}

unsigned WebMediaPlayerAndroid::droppedFrameCount() const {
  NOTIMPLEMENTED();
  return 0;
}

unsigned WebMediaPlayerAndroid::audioDecodedByteCount() const {
  NOTIMPLEMENTED();
  return 0;
}

unsigned WebMediaPlayerAndroid::videoDecodedByteCount() const {
  NOTIMPLEMENTED();
  return 0;
}

void WebMediaPlayerAndroid::OnMediaPrepared(base::TimeDelta duration) {
  if (url_.SchemeIs("file"))
    UpdateNetworkState(WebMediaPlayer::NetworkStateLoaded);

  if (ready_state_ >= WebMediaPlayer::ReadyStateHaveCurrentData) {
    // If the status is already set to >= ReadyStateHaveCurrentData, set it again
    // to make sure that Videolayerchromium will get created.
    UpdateReadyState(ready_state_);
  } else {
    UpdateReadyState(WebMediaPlayer::ReadyStateHaveMetadata);
  }

  // In we have skipped loading, we have to update webkit about the new
  // duration.
  if (duration_ != duration) {
    duration_ = duration;
    client_->durationChanged();
  }
}

void WebMediaPlayerAndroid::prepareForRendering() {
    // Hack for when video element has a poster.
    //
    // The video will not be displayed until the ready state is set to
    // >= HaveCurrentData, at which time it will be drawn instead of
    // the poster. See MediaPlayerPrivate::hasAvailableVideoFrame().
    //
    // However, the frame available listener is not set until the first
    // frame is drawn. See WebMediaPlayerAndroid::getCurrentFrame().
    //
    // In the case where the video doesn't have a poster, the video layer
    // gets drawn even before the ready state gets set to >= HaveCurrentData
    // so that the frame available listener is set before the first video
    // frame becomes available.  Hence we can advance the ready state to
    // HaveEnoughData when the first frame actually becomes available.
    //
    // However in the case where video has a poster, it is a chicken and
    // egg situation with the frame available listener getting set and the
    // ready state getting set to >= HaveCurrentData.
    //
    // Hence, we set the ready state to HaveEnoughData here so that the video
    // layer will get drawn, which then triggers the video frame listener to
    // get set up.  We do this only if media has been prepared (i.e.
    // readystate == HaveMetaData) so we don't prematurely draw videos
    // in the case where there is no video poster available.
    if (ready_state_ == WebMediaPlayerAndroid::ReadyStateHaveMetadata)
      UpdateReadyState(WebMediaPlayerAndroid::ReadyStateHaveEnoughData);
}

void WebMediaPlayerAndroid::OnPlaybackComplete() {
  // When playback is about to finish, android media player often stops
  // at a time which is smaller than the duration. This makes webkit never
  // know that the playback has finished. To solve this, we set the
  // current time to media duration when OnPlaybackComplete() get called.
  OnTimeUpdate(duration_);
  client_->timeChanged();

  // if the loop attribute is set, timeChanged() will update the current time
  // to 0. It will perform a seek to 0. As the requests to the renderer
  // process are sequential, the OnSeekCompelete() will only occur
  // once OnPlaybackComplete() is done. As the playback can only be executed
  // upon completion of OnSeekComplete(), the request needs to be saved.
  is_playing_ = false;
  if ((seeking_) && (pending_seek_ == 0))
    pending_playback_ = true;
}

void WebMediaPlayerAndroid::OnBufferingUpdate(int percentage) {
  buffered_[0].end = duration() * percentage / 100;
  did_loading_progress_ = true;
}

void WebMediaPlayerAndroid::OnSeekComplete(base::TimeDelta current_time) {
  seeking_ = false;

  OnTimeUpdate(current_time);
  client_->timeChanged();

  if (pending_playback_) {
    play();
    pending_playback_ = false;
  }
}

void WebMediaPlayerAndroid::OnMediaError(int error_type) {
  switch (error_type) {
    case MediaPlayerBridge::MEDIA_ERROR_FORMAT:
      UpdateNetworkState(WebMediaPlayer::NetworkStateFormatError);
      break;
    case MediaPlayerBridge::MEDIA_ERROR_DECODE:
      UpdateNetworkState(WebMediaPlayer::NetworkStateDecodeError);
      break;
    case MediaPlayerBridge::MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
      UpdateNetworkState(WebMediaPlayer::NetworkStateFormatError);
      break;
    case MediaPlayerBridge::MEDIA_ERROR_INVALID_CODE:
      break;
  }
  client_->repaint();
}

void WebMediaPlayerAndroid::OnVideoSizeChanged(int width, int height) {
  has_size_info_ = true;
  if (natural_size_.width == width && natural_size_.height == height)
    return;

  natural_size_.width = width;
  natural_size_.height = height;
  ReallocateVideoFrame();

  if (hasVideo() && visible_) {
    // If video is visible, wait for first video frame to be available
    // to update ready state to ReadyStateHaveEnoughData
    UpdateReadyState(WebMediaPlayer::ReadyStateHaveMetadata);
  } else
    UpdateReadyState(WebMediaPlayer::ReadyStateHaveEnoughData);
  SetStreamTextureFrameSize();
}

void WebMediaPlayerAndroid::UpdateNetworkState(
    WebMediaPlayer::NetworkState state) {
  network_state_ = state;
  client_->networkStateChanged();
}

void WebMediaPlayerAndroid::UpdateReadyState(
    WebMediaPlayer::ReadyState state) {
  ready_state_ = state;
  client_->readyStateChanged();
}

void WebMediaPlayerAndroid::OnPlayerReleased() {
  needs_establish_peer_ = true;
}

void WebMediaPlayerAndroid::ReleaseMediaResources() {
  // Pause the media player first.
  pause();
  client_->playbackStateChanged();

  ReleaseResourcesInternal();
  OnPlayerReleased();
}

void WebMediaPlayerAndroid::WillDestroyCurrentMessageLoop() {
  Destroy();

  if (stream_id_) {
    stream_texture_factory_->DestroyStreamTexture(texture_id_);
    stream_id_ = 0;
  }

  video_frame_.reset();

  if (manager_)
    manager_->UnregisterMediaPlayer(player_id_);

  manager_ = NULL;
  main_loop_ = NULL;
}

/* This is called on the stream texture client thread */
void WebMediaPlayerAndroid::didReceiveFrame() {
  ++decoded_frame_count_;
}

void WebMediaPlayerAndroid::ReallocateVideoFrame() {
  if (texture_id_) {
    video_frame_.reset(new WebVideoFrameImpl(VideoFrame::WrapNativeTexture(
        texture_id_, kGLTextureExternalOES, natural_size_,
        gfx::Rect(natural_size_), natural_size_, base::TimeDelta(),
        base::Bind(&StreamTextureFactory::ReadBackStreamTexture,
            base::Unretained(stream_texture_factory_.get()),
            texture_id_, natural_size_.width, natural_size_.height),
        base::Closure())));
  }
}

WebVideoFrame* WebMediaPlayerAndroid::getCurrentFrame() {
  WebKit::WebSize textureSize = video_frame_->textureSize();
  if (stream_texture_proxy_.get() && stream_id_ && !stream_texture_proxy_->IsInitialized()) {
    stream_texture_proxy_->Initialize(
        stream_id_, textureSize.width, textureSize.height);
  }

  return video_frame_.get();
}

void WebMediaPlayerAndroid::putCurrentFrame(
    WebVideoFrame* web_video_frame) {
  // If video frame has updated and ready state is still not ReadyStateHaveEnoughData,
  // post task to set ready state to ReadyStateHaveEnoughData
  if (main_loop_ && decoded_frame_count_ > 0 &&
          ready_state_ != WebMediaPlayer::ReadyStateHaveEnoughData) {
    main_loop_->PostTask(FROM_HERE, base::Bind(
                &WebMediaPlayerAndroid::UpdateReadyState,
                base::Unretained(this),
                WebMediaPlayer::ReadyStateHaveEnoughData));
  }

}

void WebMediaPlayerAndroid::setStreamTextureClient(
    WebKit::WebStreamTextureClient* client) {
  if (stream_texture_proxy_.get())
    stream_texture_proxy_->SetClient(client);
}

void WebMediaPlayerAndroid::EstablishSurfaceTexturePeer() {
  if (stream_texture_factory_.get() && stream_id_)
    stream_texture_factory_->EstablishPeer(stream_id_, player_id_);
  needs_establish_peer_ = false;
}

void WebMediaPlayerAndroid::SetNeedsEstablishPeer(bool needs_establish_peer) {
  needs_establish_peer_ = needs_establish_peer;
}

void WebMediaPlayerAndroid::SetStreamTextureFrameSize() {
  if (stream_texture_factory_.get() && stream_id_) {
    stream_texture_factory_->SetFrameSize(stream_id_, natural_size_.width, natural_size_.height);
  }
}

void WebMediaPlayerAndroid::UpdatePlayingState(bool is_playing) {
  is_playing_ = is_playing;
}

}  // namespace webkit_media
