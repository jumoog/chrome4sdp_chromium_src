// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Copyright (c) 2013, The Linux Foundation. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "webkit/media/audio_decoder.h"

#include "webkit/media/android/audio_decoder_impl_android.h"

#include "base/logging.h"
#include "base/android/jni_android.h"
#include "base/android/jni_string.h"
#include "base/basictypes.h"
#include "third_party/WebKit/Source/WebKit/chromium/public/platform/WebAudioBus.h"

namespace webkit_media {

bool DecodeAudioFileData(WebKit::WebAudioBus* destination_bus, const char* data,
                         size_t data_size, double sample_rate) {

    bool res = false;

    if ((!destination_bus)||(!data)) {
        return false;
    }

    WebKit::WebAudioBus* webaudio_bus;

    res = DecodeAudioFileData(&webaudio_bus,data,data_size,sample_rate);

    if (res) {
        destination_bus->initialize (webaudio_bus->numberOfChannels(),
                                     webaudio_bus->length(),
                                     webaudio_bus->sampleRate());

        if (webaudio_bus->length() == destination_bus->length()) {
            for (unsigned int i = 0; i< webaudio_bus->numberOfChannels(); i++) {
                memcpy(destination_bus->channelData(i),webaudio_bus->channelData(i),webaudio_bus->length()*(sizeof(float)));
            }
        } else {
            res = false;
        }
    }

    return res;
}

}  // namespace webkit_media
