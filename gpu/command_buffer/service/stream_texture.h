// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Copyright (c) 2013, The Linux Foundation. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef GPU_COMMAND_BUFFER_SERVICE_STREAM_TEXTURE_H_
#define GPU_COMMAND_BUFFER_SERVICE_STREAM_TEXTURE_H_

#include "base/basictypes.h"

namespace gfx {
class GLContext;
}

namespace gpu {

class StreamTexture {
 public:
  StreamTexture() {
  }

  virtual ~StreamTexture() {
  }

  virtual void Update() = 0;
  // Validates the update context
  // Returns true if the context can be used for updating
  // the stream texture
  // Param context should be a pointer to the current GL context
  virtual bool ValidateUpdateContext(gfx::GLContext* context) { return true; }
  virtual int FrameWidth() const = 0;
  virtual int FrameHeight() const = 0;

 private:
  DISALLOW_COPY_AND_ASSIGN(StreamTexture);
};

} // namespace gpu

#endif  // GPU_COMMAND_BUFFER_SERVICE_STREAM_TEXTURE_H_
