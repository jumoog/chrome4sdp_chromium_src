/*
Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef UI_GL_GL_SURFACE_ANDROID_H_
#define UI_GL_GL_SURFACE_ANDROID_H_

#include "base/memory/ref_counted.h"
#include "ui/base/ui_export.h"
#include "ui/gl/gl_surface_egl.h"

namespace gfx {

// A view surface. This can be created in the GPU process (default case), or
// browser process (in-process-gpu), or render process (in-process-webgl). When
// it is initialized, it always uses a pbuffer EGL surface until the native view
// is set. The native view is in charge of sharing the GL texture with UI thread
// in the browser process through SurfaceTexture.
class GL_EXPORT AndroidViewSurface : public NativeViewGLSurfaceEGL {
 public:
  AndroidViewSurface();

  // Implement GLSurface.
  virtual bool Initialize() OVERRIDE;
  virtual void Destroy() OVERRIDE;
  virtual bool Resize(const gfx::Size& size) OVERRIDE;
  virtual bool IsOffscreen() OVERRIDE;
  virtual bool SwapBuffers() OVERRIDE;
  virtual gfx::Size GetSize() OVERRIDE;
  virtual EGLSurface GetHandle() OVERRIDE;
  virtual void SetNativeWindow(AndroidNativeWindow* window) OVERRIDE;

 private:
  virtual ~AndroidViewSurface();

  bool CreateWindowSurface(AndroidNativeWindow* window);

  scoped_refptr<PbufferGLSurfaceEGL>  pbuffer_surface_;
  AndroidNativeWindow* window_;

  ANativeWindow* native_window_;
  DISALLOW_COPY_AND_ASSIGN(AndroidViewSurface);
};

}  // namespace gfx

#endif  // UI_GL_GL_SURFACE_ANDROID_H_
