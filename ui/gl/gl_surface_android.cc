/*
Copyright (c) 2013, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ui/gl/gl_surface_android.h"

#include <EGL/egl.h>

#include "base/logging.h"
#include "base/memory/ref_counted.h"
#include "ui/gl/android_native_window.h"
#include "ui/gl/egl_util.h"
#include "ui/gl/gl_bindings.h"
#include "ui/gl/gl_context.h"
#include "ui/gl/gl_implementation.h"
#include "ui/gl/gl_surface_egl.h"
#include "ui/gl/gl_surface_stub.h"

using ui::GetLastEGLErrorString;

namespace gfx {

bool GLSurface::InitializeOneOffInternal() {
  static bool initialized = false;
  if (initialized)
    return true;

  switch (GetGLImplementation()) {
    case kGLImplementationEGLGLES2:
      if (!GLSurfaceEGL::InitializeOneOff()) {
        LOG(ERROR) << "GLSurfaceEGL::InitializeOneOff failed.";
        return false;
      }
      break;
    default:
      NOTREACHED();
      break;
  }

  initialized = true;
  return true;
}
// static
scoped_refptr<GLSurface>
GLSurface::CreateViewGLSurface(bool software, gfx::AcceleratedWidget window) {
  if (software)
    return NULL;

  switch (GetGLImplementation()) {
    case kGLImplementationEGLGLES2: {
      scoped_refptr<GLSurface> surface;
      if (window)
        surface = new NativeViewGLSurfaceEGL(false, window);
      else
        surface = new GLSurfaceStub();
      if (!surface->Initialize())
        return NULL;
      return surface;
    }
    default:
      NOTREACHED();
      return NULL;
  }
}

// static
scoped_refptr<GLSurface>
GLSurface::CreateOffscreenGLSurface(bool software, const gfx::Size& size) {
  if (software)
    return NULL;

  switch (GetGLImplementation()) {
    case kGLImplementationEGLGLES2: {
      scoped_refptr<PbufferGLSurfaceEGL> surface(
          new PbufferGLSurfaceEGL(false, size));
      if (!surface->Initialize())
        return NULL;
      return surface;
    }
    default:
      NOTREACHED();
      return NULL;
  }
}

AndroidViewSurface::AndroidViewSurface()
  : NativeViewGLSurfaceEGL(false, 0),
    pbuffer_surface_(new PbufferGLSurfaceEGL(false, Size(1, 1))),
    window_(NULL),
    native_window_(NULL) {
}

AndroidViewSurface::~AndroidViewSurface() {
}

bool AndroidViewSurface::Initialize() {
  DCHECK(pbuffer_surface_.get());
  return pbuffer_surface_->Initialize();
}

void AndroidViewSurface::Destroy() {
  if (pbuffer_surface_.get()) {
    pbuffer_surface_->Destroy();
  } else {
    window_ = NULL;
    native_window_ = NULL;
  }
  NativeViewGLSurfaceEGL::Destroy();
}

bool AndroidViewSurface::IsOffscreen() {
  return false;
}

bool AndroidViewSurface::SwapBuffers() {
  if (!pbuffer_surface_.get())
    return NativeViewGLSurfaceEGL::SwapBuffers();
  return true;
}

gfx::Size AndroidViewSurface::GetSize() {
  if (pbuffer_surface_.get())
    return pbuffer_surface_->GetSize();
  else
    return NativeViewGLSurfaceEGL::GetSize();
}

EGLSurface AndroidViewSurface::GetHandle() {
  if (pbuffer_surface_.get())
    return pbuffer_surface_->GetHandle();
  else
    return NativeViewGLSurfaceEGL::GetHandle();
}

bool AndroidViewSurface::Resize(const gfx::Size& size) {
  if (pbuffer_surface_.get())
    return pbuffer_surface_->Resize(size);
  else if (GetHandle()) {
    if (size == GetSize())
       return true;
    DCHECK(window_ && window_->GetNativeWindow());
    // Deactivate and restore any currently active context.
    EGLContext context = eglGetCurrentContext();
    if (context != EGL_NO_CONTEXT) {
      eglMakeCurrent(GetDisplay(), EGL_NO_SURFACE, EGL_NO_SURFACE,
          EGL_NO_CONTEXT);
    }
    NativeViewGLSurfaceEGL::Destroy();
    if (CreateWindowSurface(window_)) {
      if (context != EGL_NO_CONTEXT)
        eglMakeCurrent(GetDisplay(), GetHandle(), GetHandle(), context);
    }
  }
  return true;
}

bool AndroidViewSurface::CreateWindowSurface(AndroidNativeWindow* window) {
  DCHECK(window->GetNativeWindow());
  window_ = window;
  native_window_ = window->GetNativeWindow();
  EGLSurface surface = eglCreateWindowSurface(GetDisplay(),
                                              GetConfig(),
                                              window->GetNativeWindow(),
                                              NULL);
  if (surface == EGL_NO_SURFACE) {
    LOG(ERROR) << "eglCreateWindowSurface failed with error "
               << GetLastEGLErrorString();
    Destroy();
    return false;
  }

  SetHandle(surface);
  return true;
}

void AndroidViewSurface::SetNativeWindow(AndroidNativeWindow* window) {
  if (window && window->GetNativeWindow()) {
    if(pbuffer_surface_.get()){
      pbuffer_surface_->Destroy();
      pbuffer_surface_ = NULL;
    }
    if (native_window_ != window->GetNativeWindow()) {
      EGLContext context = eglGetCurrentContext();
      if (context != EGL_NO_CONTEXT) {
        eglMakeCurrent(GetDisplay(), EGL_NO_SURFACE, EGL_NO_SURFACE,
        EGL_NO_CONTEXT);
      }
      NativeViewGLSurfaceEGL::Destroy();
      window_ = NULL;
      native_window_ = NULL;
      if (CreateWindowSurface(window)) {
        if (context != EGL_NO_CONTEXT)
            eglMakeCurrent(GetDisplay(), GetHandle(), GetHandle(), context);
      }
    }
  } else {
    DCHECK(GetHandle());
    EGLContext context = eglGetCurrentContext();
    if (context != EGL_NO_CONTEXT) {
        eglMakeCurrent(GetDisplay(), EGL_NO_SURFACE, EGL_NO_SURFACE,
        EGL_NO_CONTEXT);
    }
    NativeViewGLSurfaceEGL::Destroy();
    window_ = NULL;

    native_window_ = NULL;
    pbuffer_surface_ = new PbufferGLSurfaceEGL(false, Size(1,1));
    pbuffer_surface_->Initialize();
  }
}

}  // namespace gfx
